# Emoji How To

You can use custom (user-defined) emoji (small emotion pictures in post text), like this:

![](res/emoji_in_text.png)


To do this, you have to:
- create folder named `emoji` in your feed's folder;
- copy your emoji picture into that folder:
    - must be `.png` image;
    - should be 48x48px in size;
    - should have short and memorizable name, preferrably in Latin;
- now you can use your emoji in posts:
    - if you named it, for example, as `emoji.png`, type `:emoji:`;
    - or you can just pick it from editor's right sidebar;

Limitations:
- only `.png` file format supported;
- only post body can have custom emojis;

