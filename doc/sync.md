# Sync

Sync setup is a bit clumsy and limited for now.


## How to set things up

On server:
1. Get a sync server: any Linux machine with `ssh` set up and accessible from your PC would suffice:
2. Place `fourteen-cli` binary, make binary globally executable (`ssh yer-server` and then `fourteen-cli` has to work fine);
3. Install `rsync`;

On PC:
1. Set up ssh: you should be able to perform `ssh yer-server` w/o login;
2. Install `rsync` (from your distro's repo) and `fourteen-cli` (build it yourself and put in PATH);

Check if it works: `ssh yer-server fourteen-cli --version` executed on your PC should print version info;


## How to add sync server

1. Launch `fourteen-qt`;
2. Go to `☰ -> Sync` and add new server;
3. Then edit each feed you want to sync, assigning pull/push mode:
    - Pull means "grab all new data from remote server";
    - Push means "upload all new data to remote server";


## How to sync feeds

If you want to sync some existing feed to server, its really easy: just go to sync screen, hover mouse pointer on sync server and press `⟲` button to start sync.

If you want add some feed from remote:
- find out feed uuid (you can get it from feed edit menu);
- use `fourteen-cli new-stub <UUID>` command to create empty feed w/ given uuid;
- perform sync from sync menu.
