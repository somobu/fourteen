# Notes on cryptography / encryption


## Q&A

How do I create encrypted feed?
- As of June 2024 it is possible to create new encrypted feed using `fourteen-cli new-encrypted` command:

Is it possible to encrypt/decrypt existing feed?
- As of June 2024 it is not possible to encrypt (decrypt) feed with pictures;
- It is possible to encrypt (decrypt) textual data using `fourteen-cli export` command and then import it back using `fourteen-cli import`;
- There's no command (yet) to recrypt media in the same way;

How do I store encryption key?
- First option: keep it somewhere safe and far away from app data and enter key each time;
- Second option: set `--priv-keys PATH` cmdline argument pointing to json dict in `feed id -> priv key` format.


## Review status

As of mar 2024, no security audit / code review has been performed.


## Implementation details

As of mar 2024, posts and media can be stored encrypted using ChaCha20-Poly1305.
