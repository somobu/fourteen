-- List all active posts with certain tag
-- Arguments: tag_id, feed_id, feed_id
SELECT
    p.uuid,
    n.feed_id,
    n.created_at,
    p.edited_at,
    n.status,
    n.crypted
FROM
    (
        SELECT
            post_id
        FROM
            post_tags
        WHERE
            tag_id = ?
    ) as t
    INNER JOIN (
        SELECT
            uuid,
            MAX(edited_at) as edited_at
        FROM
            post AS p
        WHERE
            p.feed_id = ?
        GROUP BY
            uuid
    ) AS p ON t.post_id = p.uuid
    INNER JOIN (
        SELECT
            uuid,
            feed_id,
            created_at,
            edited_at,
            status,
            crypted
        FROM
            post
        WHERE
            feed_id = ?
    ) AS n ON n.uuid = p.uuid
    AND n.edited_at = p.edited_at
WHERE
    n.status = 0
ORDER BY
    n.created_at