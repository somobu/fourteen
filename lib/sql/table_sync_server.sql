CREATE TABLE sync_server(
    uuid TEXT NOT NULL,
    server_name TEXT NOT NULL,
    server_type TEXT NOT NULL,
    server_data TEXT NOT NULL,
    mode TEXT NOT NULL,
    last_sync INTEGER NOT NULL,
    UNIQUE(uuid)
);