SELECT
    uuid,
    server_name,
    server_type,
    server_data,
    mode,
    last_sync
FROM
    sync_server
ORDER BY
    server_name ASC