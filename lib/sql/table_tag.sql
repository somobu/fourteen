CREATE TABLE tag(
    uuid TEXT NOT NULL,
    feed_id TEXT NOT NULL,
    edited_at INTEGER NOT NULL,
    active INTEGER NOT NULL,
    crypted TEXT NOT NULL,
    UNIQUE(edited_at),
    UNIQUE(uuid, edited_at)
);