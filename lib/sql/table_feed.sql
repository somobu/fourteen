CREATE TABLE feed(
    uuid TEXT NOT NULL,
    edited_at INTEGER NOT NULL,
    private INTEGER NOT NULL DEFAULT 0,
    status INTEGER DEFAULT 0,
    crypted TEXT NOT NULL,
    UNIQUE(edited_at),
    UNIQUE(uuid, edited_at)
);