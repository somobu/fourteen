CREATE TABLE sync_feed(
    feed_id TEXT NOT NULL,
    pull_mode TEXT NOT NULL,
    pull_servers TEXT NOT NULL,
    push_mode TEXT NOT NULL,
    push_servers TEXT NOT NULL,
    UNIQUE(feed_id)
);