-- List all active posts within specified time range
-- Arguments: feed_id, feed_id, millis start, millis end
SELECT
    p.uuid,
    n.feed_id,
    n.created_at,
    p.edited_at,
    n.status,
    n.crypted
FROM
    (
        SELECT
            uuid,
            MAX(edited_at) as edited_at
        FROM
            post AS p
        WHERE
            p.feed_id = ?
        GROUP BY
            uuid
    ) AS p
    INNER JOIN (
        SELECT
            uuid,
            feed_id,
            created_at,
            edited_at,
            status,
            crypted
        FROM
            post
        WHERE
            feed_id = ?
    ) AS n ON n.uuid = p.uuid
    AND n.edited_at = p.edited_at
WHERE
    n.status = 0
    AND n.created_at >= ?
    AND n.created_at < ?
ORDER BY
    n.created_at