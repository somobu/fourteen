-- Returs all active feeds
SELECT
    p.uuid,
    n.edited_at,
    n.private,
    n.status,
    n.crypted
FROM
    (
        SELECT
            uuid,
            MAX(edited_at) as edited_at
        FROM
            feed AS p
        GROUP BY
            uuid
    ) AS p
    INNER JOIN (
        SELECT
            uuid,
            edited_at,
            private,
            status,
            crypted
        FROM
            feed
    ) AS n ON n.uuid = p.uuid
    AND n.edited_at = p.edited_at
WHERE
    n.status = 0;