SELECT
    uuid,
    feed_id,
    edited_at,
    crypted
FROM
    user
WHERE
    feed_id = ?
    AND uuid = ?
ORDER BY
    edited_at DESC;