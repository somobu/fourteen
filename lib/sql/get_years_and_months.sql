-- Enumerates dates (in YYYY-MM-DD and millis) of months where are some posts
-- Args: feed_id
SELECT
    DISTINCT strftime('%Y-%m-01', datetime(created_at / 1000, 'unixepoch')) as fc,
    unixepoch(strftime('%Y-%m-01', datetime(created_at / 1000, 'unixepoch'))) * 1000 as t
FROM
    post
    INNER JOIN (
        SELECT
            uuid as mid,
            MAX(edited_at) as medit
        FROM
            post
        GROUP BY
            uuid
    ) mx ON post.uuid = mx.mid
    AND post.edited_at = mx.medit
WHERE
    feed_id = ?
    AND status = 0
ORDER BY
    t ASC