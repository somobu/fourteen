-- Get most recent post creation date
-- Arguments: feed_id
SELECT
    created_at
FROM
    post
    INNER JOIN (
        SELECT
            uuid as mid,
            MAX(edited_at) as medit
        FROM
            post
        GROUP BY
            uuid
    ) mx ON post.uuid = mx.mid
    AND post.edited_at = mx.medit
WHERE
    feed_id = ?
    AND status = 0
ORDER BY
    created_at DESC;