SELECT
    feed_id,
    pull_mode,
    pull_servers,
    push_mode,
    push_servers
FROM
    sync_feed
