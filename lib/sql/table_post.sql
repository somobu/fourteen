CREATE TABLE post(
    uuid TEXT NOT NULL,
    feed_id TEXT NOT NULL,
    created_at INTEGER NOT NULL,
    edited_at INTEGER NOT NULL,
    status INTEGER DEFAULT 0,
    crypted TEXT NOT NULL,
    UNIQUE(edited_at),
    UNIQUE(uuid, edited_at)
);