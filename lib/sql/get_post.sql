-- Returns latest revision of post
-- Arguments: post_id
SELECT
    uuid,
    feed_id,
    created_at,
    edited_at,
    status,
    crypted
FROM
    post
WHERE
    uuid = ?
ORDER BY
    edited_at DESC
LIMIT
    1;