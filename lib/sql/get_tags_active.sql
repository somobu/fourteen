-- List all tags marked as 'active'
-- Arguments: feed_id, feed_id
SELECT DISTINCT
    p.uuid,
    n.feed_id,
    p.edited_at,
    n.active,
    n.crypted
FROM
    (
        SELECT
            uuid,
            MAX(edited_at) as edited_at
        FROM
            tag AS p
        WHERE
            p.feed_id = ?
        GROUP BY
            uuid
    ) AS p
    INNER JOIN (
        SELECT
            uuid,
            feed_id,
            edited_at,
            active,
            crypted
        FROM
            tag
        WHERE
            feed_id = ?
    ) AS n ON n.uuid = p.uuid
    AND n.edited_at = p.edited_at
WHERE
    n.active = 1