//! "Raw" structures, as it stored in database

use std::{
    fmt::{self},
    num::TryFromIntError,
    str::FromStr,
};

use serde::{de::DeserializeOwned, Deserialize, Serialize};

use super::{
    crypto,
    error::{self, OptionToError},
};

/// Time in millis since Unix epoch
pub type Date = u64;

pub type UserId = String;
pub type FeedId = String;
pub type PostId = String;
pub type TagId = String;
pub type SyncServerId = String;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Inner<T> {
    Decrypted(T),
    Encrypted(InnerEncryptedData),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InnerEncryptedData {
    pub crypted: String,
}

#[derive(Debug, PartialEq, Eq)]
pub enum InnerError {
    IsEncrypted,
    IsDecrypted,
}

impl std::fmt::Display for InnerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fmt::Debug::fmt(&self, f)
    }
}

impl std::error::Error for InnerError {}

impl<T> Inner<T> {
    pub fn is_encrypted(&self) -> bool {
        match self {
            Inner::Decrypted(_) => false,
            Inner::Encrypted(_) => true,
        }
    }

    /// Return decrypted value or error
    pub fn de(&self) -> Result<&T, InnerError> {
        match self {
            Inner::Decrypted(v) => Ok(v),
            Inner::Encrypted(_) => Err(InnerError::IsEncrypted),
        }
    }

    /// Return decrypted value or error
    pub fn de_mut(&mut self) -> Result<&mut T, InnerError> {
        match self {
            Inner::Decrypted(v) => Ok(v),
            Inner::Encrypted(_) => Err(InnerError::IsEncrypted),
        }
    }

    /// Returns encrypted value or error
    pub fn en(&self) -> Result<&InnerEncryptedData, InnerError> {
        match self {
            Inner::Decrypted(_) => Err(InnerError::IsDecrypted),
            Inner::Encrypted(v) => Ok(v),
        }
    }
}

impl<T: Serialize + DeserializeOwned> Inner<T> {
    pub fn encrypt(self, key: &str) -> error::Result<Self> {
        assert!(key.len() > 0);

        match self {
            Inner::Decrypted(v) => {
                let data = serde_json::to_string(&v)?;
                let encrypted_str = crypto::encrypt_b64(key, &data)?;
                Ok(Self::Encrypted(InnerEncryptedData {
                    crypted: encrypted_str,
                }))
            }
            Inner::Encrypted(_) => Err(InnerError::IsEncrypted.into()),
        }
    }

    pub fn decrypt(self, key: &str) -> error::Result<Self> {
        assert!(key.len() > 0);

        match self {
            Inner::Decrypted(_) => Err(InnerError::IsDecrypted.into()),
            Inner::Encrypted(v) => {
                let decrypted_str = crypto::decrypt_b64(key, &v.crypted)?;
                let data = serde_json::from_str::<T>(&decrypted_str)?;
                Ok(Self::Decrypted(data))
            }
        }
    }
}

impl<T: Default> Default for Inner<T> {
    fn default() -> Self {
        Inner::Decrypted(T::default())
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Feed {
    pub uuid: String,
    pub edited_at: Date,
    pub private: bool,

    #[serde(default)]
    #[serde(skip_serializing_if = "FeedStatus::is_default")]
    pub status: FeedStatus,

    #[serde(flatten)]
    pub inner: Inner<FeedData>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct FeedData {
    pub owned_by: UserId,
    pub edited_by: UserId,

    #[serde(default)]
    #[serde(skip_serializing_if = "String::is_empty")]
    pub title: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "String::is_empty")]
    pub descr: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub header_imgs: Vec<String>,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
#[repr(u8)]
pub enum FeedStatus {
    #[default]
    Visible = 0,
    Deleted = 1,
}

impl FeedStatus {
    
    pub fn is_default(&self) -> bool {
        match self {
            FeedStatus::Visible => true,
            FeedStatus::Deleted => false,
        }
    }

    pub fn to_db(&self) -> String {
        (*self as u8).to_string()
    }

}

impl TryFrom<u8> for FeedStatus {
    type Error = TryFromIntError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(FeedStatus::Visible),
            1 => Ok(FeedStatus::Deleted),
            _ => Ok(FeedStatus::Visible),
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct User {
    pub uuid: UserId,
    pub feed_id: FeedId,
    pub edited_at: Date,

    #[serde(flatten)]
    pub inner: Inner<UserData>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct UserData {
    pub created_by: UserId,
    pub created_at: Date,
    pub edited_by: UserId,
    pub name: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "String::is_empty")]
    pub icon: String,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Post {
    pub uuid: String,
    pub feed_id: FeedId,
    pub created_at: Date,
    pub edited_at: Date,
    pub status: PostStatus,

    #[serde(flatten)]
    pub inner: Inner<PostData>,
}

impl Post {
    pub fn decrypted(mut self, key: Option<&String>) -> error::Result<Self> {
        if self.inner.is_encrypted() {
            if key.is_none() {
                return Err(key.to_result().unwrap_err());
            } else {
                self.inner = self.inner.decrypt(key.unwrap())?;
            }
        }

        Ok(self)
    }

    pub fn encrypted(mut self, key: Option<&String>) -> error::Result<Self> {
        if !self.inner.is_encrypted() {
            if key.is_none() {
                return Err(key.to_result().unwrap_err());
            } else {
                self.inner = self.inner.encrypt(key.unwrap())?;
            }
        }

        Ok(self)
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct PostData {
    pub created_by: UserId,
    pub edited_by: UserId,

    pub format: PostFormat,

    #[serde(default)]
    #[serde(skip_serializing_if = "String::is_empty")]
    pub title: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "String::is_empty")]
    pub text: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "String::is_empty")]
    pub f_img: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub tags: Vec<TagId>,
}

#[derive(Debug, Default, Clone, PartialEq, Serialize, Deserialize)]
pub enum PostFormat {
    #[default]
    Default,
    Featured,
}

/// Do not forget to update TryFrom as well
#[derive(Debug, Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
#[repr(u8)]
pub enum PostStatus {
    #[default]
    Visible = 0,
    Deleted = 1,
    Drafted = 2,
}

impl PostStatus {
    pub fn to_db(&self) -> String {
        (*self as u8).to_string()
    }
}

impl TryFrom<u8> for PostStatus {
    type Error = TryFromIntError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(PostStatus::Visible),
            1 => Ok(PostStatus::Deleted),
            2 => Ok(PostStatus::Drafted),
            _ => Ok(PostStatus::Visible),
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Tag {
    pub uuid: TagId,
    pub feed_id: FeedId,
    pub edited_at: Date,
    pub active: bool,

    #[serde(flatten)]
    pub inner: Inner<TagData>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct TagData {
    pub name: String,
    pub edited_by: UserId,
    pub tag_type: String,

    /// RegEx
    pub trigger: String,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum TagType {
    #[default]
    Regular = 0,

    /// To be implemented in the future
    Location = 1,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct SyncServer {
    pub uuid: SyncServerId,

    /// User-defined name of the server
    pub server_name: String,

    /// Other variants will be added in future
    pub server_type: SyncServerType,

    /// Data required to connect to server
    ///
    /// Content on `server_type`.
    ///
    /// Now just a name of ssh server.
    pub server_data: String,

    pub mode: SyncMode,
    pub last_sync: u64,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub enum SyncServerType {
    #[default]
    Ssh,
}

impl ToString for SyncServerType {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

impl FromStr for SyncServerType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Ssh" => Ok(Self::Ssh),
            _ => Err(()),
        }
    }
}

impl From<String> for SyncServerType {
    fn from(value: String) -> Self {
        Self::from_str(&value).unwrap()
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum SyncMode {
    #[default]
    None,
    Pull,
    Push,
    Both,
}

impl ToString for SyncMode {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

impl FromStr for SyncMode {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "None" => Ok(Self::None),
            "Pull" => Ok(Self::Pull),
            "Push" => Ok(Self::Push),
            "Both" => Ok(Self::Both),
            _ => Err(()),
        }
    }
}

impl From<String> for SyncMode {
    fn from(value: String) -> Self {
        Self::from_str(&value).unwrap()
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct SyncFeed {
    pub feed_id: FeedId,

    pub pull_mode: SyncServersFilter,
    pub pull_servers: Vec<SyncServerId>,

    pub push_mode: SyncServersFilter,
    pub push_servers: Vec<SyncServerId>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum SyncServersFilter {
    /// Allow servers specified in servers list
    #[default]
    WhiteList,

    /// Nothing, allow no servers to be synced
    None,

    /// Everything, allow all servers to be synced
    All,
}

impl ToString for SyncServersFilter {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

impl FromStr for SyncServersFilter {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "None" => Ok(Self::None),
            "WhiteList" => Ok(Self::WhiteList),
            "All" => Ok(Self::All),
            _ => Err(()),
        }
    }
}

impl From<String> for SyncServersFilter {
    fn from(value: String) -> Self {
        Self::from_str(&value).unwrap()
    }
}
