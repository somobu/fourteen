//! Simple header-only container format
//!
//! Basically it is just a prefix to store some metadata like encryption algo and nonce

use std::slice;

/// Our magic bytes to validate header
pub const MAGIC: &[u8; 2] = b"FT";

/// Actual header version
pub const VERSION: u8 = 1;

/// Header itself
#[repr(C)]
pub struct Header {
    pub name: [u8; 2],                 // 0x46 54
    pub version: u8,                   // 0x01
    pub content_format: ContentFormat, // 0x02

    /// Can be interpreted as LE u128
    pub nonce: [u8; 16],
}

#[derive(PartialEq, Eq)]
#[repr(u8)]
pub enum ContentFormat {
    Raw = 1,
    ChaChaPoly1305 = 2,
}

impl Header {
    pub fn new(format: ContentFormat, nonce: u128) -> Self {
        Header {
            name: *MAGIC,
            version: VERSION,
            content_format: format,
            nonce: nonce.to_le_bytes(),
        }
    }

    /// Interprets first n of provided bytes as Header and performs header validation
    ///
    /// Returns None if validation failed
    pub fn from_bytes(bytes: &[u8]) -> Option<&Header> {
        let l = unsafe { (bytes.as_ptr() as *const Header).as_ref() };
        l?;

        let l = l.unwrap();
        if !l.is_valid() {
            return None;
        }

        Some(l)
    }

    pub fn get_bytes(&self) -> Vec<u8> {
        let raw: *const u8 = self as *const Header as *const u8;
        let slice = unsafe { slice::from_raw_parts(raw, std::mem::size_of::<Header>()) };
        slice.to_vec()
    }

    /// Basic sanity check
    pub fn is_valid(&self) -> bool {
        self.name == *MAGIC && self.version == VERSION
    }

    /// Returns actual size of header
    ///
    /// Currently is just a convenience method, but may be useful in future
    /// because header size will change between different versions.
    pub const fn get_size(&self) -> usize {
        std::mem::size_of::<Header>()
    }
}

impl Default for Header {
    fn default() -> Self {
        Self {
            name: *MAGIC,
            version: VERSION,
            content_format: ContentFormat::Raw,
            nonce: u128::max_value().to_le_bytes(),
        }
    }
}
