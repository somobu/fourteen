//! Main and only export format

use rusqlite::ErrorCode;
use serde::{Deserialize, Serialize};

use crate::{database::Database, error};

use super::raw;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Delta {
    pub feeds: Vec<raw::Feed>,
    pub users: Vec<raw::User>,
    pub posts: Vec<raw::Post>,
    pub tags: Vec<raw::Tag>,
}

impl Delta {
    pub fn from_db(
        backend: &Database,
        feed_id: &String,
        since: u64,
        decrypt_with: Option<String>,
        encrypt_with: Option<String>,
    ) -> Delta {
        let since = since.to_string();

        let mut delta = Delta {
            feeds: backend.get_feed_delta(feed_id, &since).unwrap(),
            posts: backend.get_posts_delta(feed_id, &since).unwrap(),
            users: backend.get_users_delta(feed_id, &since).unwrap(),
            tags: backend.get_tags_delta(feed_id, &since).unwrap(),
        };

        if decrypt_with.is_some() {
            let key = decrypt_with.unwrap();
            delta.feeds.iter_mut().for_each(|f| {
                f.private = false;
                f.inner = f.inner.clone().decrypt(&key).unwrap();
            });
            delta.posts.iter_mut().for_each(|f| {
                f.inner = f.inner.clone().decrypt(&key).unwrap();
            });
            delta.users.iter_mut().for_each(|f| {
                f.inner = f.inner.clone().decrypt(&key).unwrap();
            });
            delta.tags.iter_mut().for_each(|f| {
                f.inner = f.inner.clone().decrypt(&key).unwrap();
            });
        }

        if encrypt_with.is_some() {
            let key = encrypt_with.unwrap();
            delta.feeds.iter_mut().for_each(|f| {
                f.private = true;
                f.inner = f.inner.clone().encrypt(&key).unwrap();
            });
            delta.posts.iter_mut().for_each(|f| {
                f.inner = f.inner.clone().encrypt(&key).unwrap();
            });
            delta.users.iter_mut().for_each(|f| {
                f.inner = f.inner.clone().encrypt(&key).unwrap();
            });
            delta.tags.iter_mut().for_each(|f| {
                f.inner = f.inner.clone().encrypt(&key).unwrap();
            });
        }

        delta
    }

    /// Tiny obscure function that handles given result and writes provided [DeltaSummaryEntry] to specified [DeltaSummary]
    fn handle_error(
        rz: &mut DeltaSummary,
        l: error::Result<()>,
        fmt: impl Fn() -> DeltaSummaryEntry,
    ) {
        if l.is_err() {
            let e = l.unwrap_err();
            match e.kind {
                error::ErrorKind::Sqlite(e) => {
                    let code = e.sqlite_error_code().unwrap_or(ErrorCode::Unknown);
                    if code == ErrorCode::ConstraintViolation {
                        rz.existing_entries.push(fmt())
                    }
                }
                error::ErrorKind::NotFound(e) => {
                    rz.other_errors.push((format!("Not found: {e}"), fmt()));
                }
                _ => unimplemented!("Error handling for {e:#?}"),
            }
        }
    }

    pub fn into_db(&self, backend: &Database) -> error::Result<DeltaSummary> {
        let mut rz = DeltaSummary::default();

        for feed in &self.feeds {
            let l = backend.insert_feed(feed);

            if l.is_ok() {
                rz.successfully_added.feeds += 1;
            }

            Delta::handle_error(&mut rz, l, || DeltaSummaryEntry {
                table: "feed".to_string(),
                uuid: feed.uuid.clone(),
                edited_at: feed.edited_at,
            });
        }

        for tag in &self.tags {
            let l = backend.insert_tag(tag);

            if l.is_ok() {
                rz.successfully_added.tags += 1;
            }

            Delta::handle_error(&mut rz, l, || DeltaSummaryEntry {
                table: "tag".to_string(),
                uuid: tag.uuid.clone(),
                edited_at: tag.edited_at,
            });
        }

        for post in &self.posts {
            let l = backend.insert_post(post);

            if l.is_ok() {
                if !post.inner.is_encrypted() {
                    let tags = post.inner.de().unwrap().tags.clone();
                    backend.insert_post_tags(&post.uuid, &tags)?;
                }

                rz.successfully_added.posts += 1;
            } else {
                Delta::handle_error(&mut rz, l, || DeltaSummaryEntry {
                    table: "post".to_string(),
                    uuid: post.uuid.clone(),
                    edited_at: post.edited_at,
                });
            }
        }

        for user in &self.users {
            let l = backend.insert_user(user);

            if l.is_ok() {
                rz.successfully_added.users += 1;
            }

            Delta::handle_error(&mut rz, l, || DeltaSummaryEntry {
                table: "user".to_string(),
                uuid: user.uuid.clone(),
                edited_at: user.edited_at,
            });
        }

        Ok(rz)
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct DeltaSummary {
    pub successfully_added: DeltaSummarySuccess,
    pub existing_entries: Vec<DeltaSummaryEntry>,
    pub other_errors: Vec<(String, DeltaSummaryEntry)>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct DeltaSummarySuccess {
    pub feeds: u32,
    pub users: u32,
    pub posts: u32,
    pub tags: u32,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DeltaSummaryEntry {
    pub table: String,
    pub uuid: String,
    pub edited_at: u64,
}
