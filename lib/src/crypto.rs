//! Encryption/decryption of `Vec<u8>` or base64 string

use base64::{engine::general_purpose::STANDARD, Engine};
use chacha20poly1305::{
    aead::{generic_array::GenericArray, Aead},
    ChaCha20Poly1305, KeyInit,
};

use super::{
    container::{self, ContentFormat, Header},
    error::{self, ErrorKind},
    util,
};

/// Turns plain bytes into [Header] + encrypted bytes
pub fn encrypt(key: &str, data: Vec<u8>) -> error::Result<Vec<u8>> {
    let key = GenericArray::from(util::decode_hex::<32>(key)?);
    let cipher = ChaCha20Poly1305::new(&key);
    let nonce: u128 = rand::random();

    let sliced_nonce = &nonce.to_le_bytes()[0..12];
    let crypto_nonce = GenericArray::from_slice(sliced_nonce);

    let mut ciphertext = cipher.encrypt(crypto_nonce, data.as_slice()).unwrap();

    let mut bytes = Header::new(container::ContentFormat::ChaChaPoly1305, nonce).get_bytes();
    bytes.append(&mut ciphertext);

    Ok(bytes)
}

/// Turns [Header] + encrypted bytes into plaintext
pub fn decrypt(key: &str, ciphertext: &Vec<u8>) -> error::Result<Vec<u8>> {
    let header = container::Header::from_bytes(ciphertext).unwrap();
    assert!(header.content_format == ContentFormat::ChaChaPoly1305);

    let key = GenericArray::from(util::decode_hex::<32>(key)?);
    let cipher = ChaCha20Poly1305::new(&key);

    let sliced_nonce = &header.nonce[0..12];
    let nonce = GenericArray::from_slice(sliced_nonce);

    let data_slice = &ciphertext.as_slice()[header.get_size()..];

    let plaintext = cipher
        .decrypt(nonce, data_slice)
        .map_err(|e| error::new(ErrorKind::BadString(e.to_string())))?;

    Ok(plaintext)
}

/// Encrypts UTF-8 string to bytes and converts these bytes into Base64-encoded string
pub fn encrypt_b64(key: &str, data: &str) -> error::Result<String> {
    let bytes = encrypt(key, data.as_bytes().to_vec())?;
    Ok(STANDARD.encode(bytes))
}

/// Converts Base64-encoded string, decodes these bytes and constructs UTF-8 string from them
pub fn decrypt_b64(key: &str, ciphertext: &str) -> error::Result<String> {
    let bytes_cipher = STANDARD.decode(ciphertext).unwrap();
    let plaintext = decrypt(key, &bytes_cipher)?;
    Ok(String::from_utf8(plaintext.clone())?)
}
