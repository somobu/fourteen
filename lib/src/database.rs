//! Interface to a database
//!
//! Doesnt care about encryption and other things

use std::collections::HashMap;

use gregorian::Date;
use rusqlite::{Connection, Row};

use crate::raw::{FeedStatus, InnerEncryptedData, PostId, TagId};

use super::{
    error::{self},
    raw::{self, FeedId, PostStatus, UserId},
    util::{self},
};

pub struct Database {
    conn: Connection,
}

impl Database {
    pub fn new(connection: Connection) -> error::Result<Database> {
        let backend = Database { conn: connection };

        if !backend.has_data() {
            backend.create_tables()?;
        }

        Ok(backend)
    }

    pub fn open_file(path: &str) -> error::Result<Database> {
        Database::new(Connection::open(path)?)
    }

    pub fn open_in_memory() -> error::Result<Database> {
        Database::new(Connection::open_in_memory()?)
    }

    pub fn has_data(&self) -> bool {
        self.conn
            .query_row(
                "SELECT name FROM sqlite_master WHERE type='table' AND name='feed';",
                [],
                |_| Ok(()),
            )
            .is_ok()
    }

    pub fn close(self) {
        self.conn.close().unwrap();
    }

    fn create_tables(&self) -> error::Result<()> {
        eprintln!("Creating all tables");

        self.conn
            .execute(include_str!("../sql/table_feed.sql"), ())?;
        self.conn
            .execute(include_str!("../sql/table_user.sql"), ())?;
        self.conn
            .execute(include_str!("../sql/table_post.sql"), ())?;
        self.conn
            .execute(include_str!("../sql/table_tag.sql"), ())?;

        // Internal tables
        self.conn
            .execute(include_str!("../sql/table_current_user.sql"), ())?;
        self.conn
            .execute(include_str!("../sql/table_post_tags.sql"), ())?;
        self.conn
            .execute(include_str!("../sql/table_sync_feed.sql"), ())?;
        self.conn
            .execute(include_str!("../sql/table_sync_server.sql"), ())?;

        Ok(())
    }

    pub fn insert_user(&self, user: &raw::User) -> error::Result<()> {
        let feed = self.get_feed(&user.feed_id)?;

        let crypted: String;

        if feed.private {
            if user.inner.is_encrypted() {
                crypted = user.inner.en()?.clone().crypted;
            } else {
                return Err(raw::InnerError::IsDecrypted.into());
            }
        } else {
            crypted = serde_json::to_string(&user.inner.de()?)?;
        }

        self.conn.execute(
            "INSERT INTO user (uuid, feed_id, edited_at, crypted) VALUES (?, ?, ?, ?);",
            (
                user.uuid.clone(),
                user.feed_id.clone(),
                user.edited_at,
                crypted,
            ),
        )?;

        Ok(())
    }

    pub fn set_current_user(&self, feed_id: &FeedId, user_id: &UserId) -> error::Result<()> {
        self.conn.execute(
            "DELETE FROM current_user WHERE feed_id = ? AND user_id != ?;",
            (feed_id, user_id),
        )?;

        self.conn.execute(
            "INSERT INTO current_user (feed_id, user_id) VALUES (?, ?);",
            (feed_id, user_id),
        )?;

        Ok(())
    }

    pub fn insert_feed(&self, feed: &raw::Feed) -> error::Result<()> {
        let crypted: String;

        if feed.private {
            if feed.inner.is_encrypted() {
                crypted = feed.inner.en()?.clone().crypted;
            } else {
                return Err(raw::InnerError::IsDecrypted.into());
            }
        } else {
            crypted = serde_json::to_string(&feed.inner.de()?)?;
        }

        self.conn.execute(
            "INSERT INTO feed (uuid, edited_at, private, status, crypted) VALUES (?, ?, ?, ?, ?);",
            (
                feed.uuid.clone(),
                feed.edited_at,
                feed.private,
                feed.status.to_db(),
                crypted,
            ),
        )?;

        Ok(())
    }

    pub fn insert_post(&self, post: &raw::Post) -> error::Result<()> {
        let feed = self.get_feed(&post.feed_id)?;

        let crypted: String;

        if feed.private {
            if post.inner.is_encrypted() {
                crypted = post.inner.en()?.clone().crypted;
            } else {
                return Err(raw::InnerError::IsDecrypted.into());
            }
        } else {
            let inner = post.inner.de()?;
            crypted = serde_json::to_string(inner)?;
        }

        self.conn.execute(
            "INSERT INTO post (uuid, feed_id, created_at, edited_at, status, crypted) VALUES (?, ?, ?, ?, ?, ?);",
            (post.uuid.clone(), post.feed_id.clone(), post.created_at, post.edited_at, post.status.to_db(), crypted),
        )?;

        Ok(())
    }

    pub fn insert_tag(&self, tag: &raw::Tag) -> error::Result<()> {
        let feed = self.get_feed(&tag.feed_id)?;

        let crypted: String;

        if feed.private {
            if tag.inner.is_encrypted() {
                crypted = tag.inner.en()?.clone().crypted;
            } else {
                return Err(raw::InnerError::IsDecrypted.into());
            }
        } else {
            let inner = tag.inner.de()?;
            crypted = serde_json::to_string(inner)?;
        }

        self.conn.execute(
            "INSERT INTO tag (uuid, feed_id, edited_at, active, crypted) VALUES (?, ?, ?, ?, ?);",
            (
                tag.uuid.clone(),
                tag.feed_id.clone(),
                tag.edited_at,
                tag.active,
                crypted,
            ),
        )?;

        Ok(())
    }

    pub fn insert_post_tags(&self, post_id: &PostId, tag_ids: &Vec<String>) -> error::Result<()> {
        self.conn.execute(
            "DELETE FROM post_tags WHERE post_id = ?",
            (post_id.clone(),),
        )?;

        for tag in tag_ids {
            self.conn.execute(
                "INSERT INTO post_tags (post_id, tag_id) VALUES (?,?)",
                (post_id.clone(), tag),
            )?;
        }

        Ok(())
    }

    pub fn insert_sync_server(&self, server: &raw::SyncServer) -> error::Result<()> {
        self.conn.execute(
            "DELETE FROM sync_server WHERE uuid = ?",
            (server.uuid.clone(),),
        )?;

        self.conn.execute(
            "INSERT INTO sync_server (uuid, server_name, server_type, server_data, mode, last_sync)
             VALUES (?,?,?,?,?,?)",
            (
                server.uuid.clone(),
                server.server_name.clone(),
                server.server_type.clone().to_string(),
                server.server_data.clone(),
                server.mode.clone().to_string(),
                server.last_sync.clone(),
            ),
        )?;

        Ok(())
    }

    pub fn insert_sync_feed(&self, feed: &raw::SyncFeed) -> error::Result<()> {
        self.conn.execute(
            "DELETE FROM sync_feed WHERE feed_id = ?",
            (feed.feed_id.clone(),),
        )?;

        self.conn.execute(
            "INSERT INTO sync_feed (feed_id, pull_mode, pull_servers, push_mode, push_servers)
             VALUES (?,?,?,?,?)",
            (
                feed.feed_id.clone(),
                feed.pull_mode.clone().to_string(),
                serde_json::to_string(&feed.pull_servers)?,
                feed.push_mode.clone().to_string(),
                serde_json::to_string(&feed.push_servers)?,
            ),
        )?;

        Ok(())
    }

    pub fn get_user(&self, feed_id: &FeedId, user_id: &UserId) -> error::Result<raw::User> {
        self.conn.query_row_and_then(
            include_str!("../sql/get_user.sql"),
            [feed_id, user_id],
            |row| row.to_struct(),
        )
    }

    pub fn list_feeds(&self) -> error::Result<Vec<raw::Feed>> {
        self.conn
            .prepare(include_str!("../sql/get_feeds.sql"))?
            .query_and_then([], |row| row.to_struct())?
            .collect()
    }

    pub fn get_feed(&self, feed_id: &FeedId) -> error::Result<raw::Feed> {
        self.conn
            .query_row_and_then(include_str!("../sql/get_feed.sql"), [feed_id], |row| {
                row.to_struct()
            })
    }

    pub fn get_all_posts(&self, feed_id: &FeedId) -> error::Result<Vec<raw::Post>> {
        self.conn.prepare("SELECT uuid, feed_id, created_at, edited_at, status, crypted FROM post WHERE feed_id = ?;")?
            .query_and_then([feed_id], |row| row.to_struct())?
            .collect()
    }

    pub fn get_current_user(&self, feed_id: &FeedId) -> error::Result<String> {
        self.conn.query_row_and_then(
            "SELECT user_id FROM current_user WHERE feed_id = ?;",
            [feed_id],
            |row| Ok(row.get("user_id")?),
        )
    }

    pub fn get_posts_by_month(
        &self,
        feed_id: &FeedId,
        year: i16,
        month: u8,
    ) -> error::Result<Vec<raw::Post>> {
        let date_since = Date::new(year, month, 1)?;
        let date_until = date_since.add_months(1)?;
        let since = date_since.to_unix_timestamp() * 1000;
        let until = date_until.to_unix_timestamp() * 1000;

        self.conn
            .prepare(include_str!("../sql/get_posts_timed.sql"))?
            .query_and_then(
                [
                    feed_id.clone(),
                    feed_id.clone(),
                    since.to_string(),
                    until.to_string(),
                ],
                |row| row.to_struct(),
            )?
            .collect()
    }

    pub fn get_posts_by_tag(
        &self,
        feed_id: &FeedId,
        tag_id: &TagId,
    ) -> error::Result<Vec<raw::Post>> {
        self.conn
            .prepare(include_str!("../sql/get_posts_by_tag.sql"))?
            .query_and_then([tag_id.clone(), feed_id.clone(), feed_id.clone()], |row| {
                row.to_struct()
            })?
            .collect()
    }

    pub fn get_post(&self, post_id: &PostId) -> error::Result<raw::Post> {
        self.conn
            .query_row_and_then(include_str!("../sql/get_post.sql"), [post_id], |row| {
                row.to_struct()
            })
    }

    pub fn get_tags(&self, tag_ids: &Vec<TagId>) -> error::Result<Vec<raw::Tag>> {
        let mut rz = vec![];

        for tag_id in tag_ids {
            let tag: raw::Tag = self.conn.query_row_and_then(
                "SELECT uuid, feed_id, edited_at, active, crypted 
                        FROM tag WHERE uuid = ? ORDER BY edited_at DESC LIMIT 1;",
                [tag_id],
                |row| row.to_struct(),
            )?;

            rz.push(tag);
        }

        Ok(rz)
    }

    pub fn get_active_tags(&self, feed_id: &FeedId) -> error::Result<Vec<raw::Tag>> {
        self.conn
            .prepare(include_str!("../sql/get_tags_active.sql"))?
            .query_and_then([feed_id.clone(), feed_id.clone()], |row| row.to_struct())?
            .collect()
    }

    pub fn get_last_post_time(&self, feed_id: &FeedId) -> error::Result<u64> {
        Ok(self
            .conn
            .query_row(
                include_str!("../sql/get_newest_post_date.sql"),
                [feed_id],
                |row| Ok(row.get::<usize, u64>(0)),
            )
            .unwrap_or(Ok(0))?)
    }

    pub fn list_sync_servers(&self) -> error::Result<Vec<raw::SyncServer>> {
        self.conn
            .prepare(include_str!("../sql/get_sync_servers.sql"))?
            .query_and_then([], |row| row.to_struct())?
            .collect()
    }

    pub fn list_sync_feeds(&self) -> error::Result<Vec<raw::SyncFeed>> {
        self.conn
            .prepare(include_str!("../sql/get_sync_feeds.sql"))?
            .query_and_then([], |row| row.to_struct())?
            .collect()
    }

    pub fn delete_post(&mut self, post_id: &PostId) -> error::Result<()> {
        let existing_post = self.get_post(post_id);

        if existing_post.is_ok() {
            let mut post = existing_post?;
            post.status = PostStatus::Deleted;
            post.edited_at = util::millis();
            self.insert_post(&post)?;
        }

        Ok(())
    }

    pub fn delete_sync_server(&self, server: &String) -> error::Result<()> {
        self.conn
            .execute("DELETE FROM sync_server WHERE uuid = ?", (server.clone(),))?;

        Ok(())
    }

    pub fn years_and_months(&mut self, feed_id: &FeedId) -> error::Result<Vec<(i16, Vec<u8>)>> {
        let mut stmt = self
            .conn
            .prepare(include_str!("../sql/get_years_and_months.sql"))?;

        let mut rz: HashMap<i16, Vec<u8>> = HashMap::new();

        stmt.query_map([feed_id], |row| row.get::<usize, u64>(1))?
            .for_each(|e| {
                let date = Date::from_unix_timestamp((e.unwrap() / 1000) as i64);
                let year = date.year().to_number();
                let month = date.month().to_number();

                rz.entry(year).or_default();

                let months = rz.get_mut(&year).unwrap();
                if !months.contains(&month) {
                    months.push(month);
                    months.sort();
                }
            });

        let mut l: Vec<(i16, Vec<u8>)> = rz.iter().map(|e| (*e.0, e.1.clone())).collect();
        l.sort_by_key(|e| e.0);
        Ok(l)
    }

    pub fn get_feed_delta(
        &self,
        feed_id: &FeedId,
        since: &String,
    ) -> error::Result<Vec<raw::Feed>> {
        self.conn
            .prepare(
                "SELECT uuid, edited_at, private, status, crypted FROM feed
                WHERE uuid = ? AND edited_at >= ?;",
            )?
            .query_and_then([feed_id, since], |row| row.to_struct())?
            .collect()
    }

    pub fn get_posts_delta(
        &self,
        feed_id: &FeedId,
        since: &String,
    ) -> error::Result<Vec<raw::Post>> {
        self.conn
            .prepare(
                "SELECT uuid, feed_id, created_at, edited_at, status, crypted FROM post 
                WHERE feed_id = ? AND edited_at >= ?;",
            )?
            .query_and_then([feed_id, since], |row| row.to_struct())?
            .collect()
    }

    pub fn get_users_delta(
        &self,
        feed_id: &FeedId,
        since: &String,
    ) -> error::Result<Vec<raw::User>> {
        self.conn
            .prepare(
                "SELECT uuid, feed_id, edited_at, crypted FROM user  
                WHERE feed_id = ? AND edited_at >= ?;",
            )?
            .query_and_then([feed_id, since], |row| row.to_struct())?
            .collect()
    }

    pub fn get_tags_delta(&self, feed_id: &FeedId, since: &String) -> error::Result<Vec<raw::Tag>> {
        self.conn
            .prepare(
                "SELECT uuid, feed_id, edited_at, active, crypted FROM tag
                WHERE feed_id = ? AND edited_at >= ?;",
            )?
            .query_and_then([feed_id, since], |row| row.to_struct())?
            .collect()
    }
}

pub trait ToStruct<T>: Sized {
    fn to_struct(&self) -> error::Result<T>;
}

impl ToStruct<raw::Feed> for Row<'_> {
    fn to_struct(&self) -> error::Result<raw::Feed> {
        let uuid: String = self.get("uuid")?;
        let edited_at: u64 = self.get("edited_at")?;
        let private: bool = self.get("private")?;
        let crypted: String = self.get("crypted")?;
        let status: u8 = self.get("status")?;
        let inner = serde_json::from_str::<raw::FeedData>(&crypted)
            .map(raw::Inner::Decrypted)
            .unwrap_or_else(|_| raw::Inner::Encrypted(InnerEncryptedData { crypted }));

        let status = FeedStatus::try_from(status)?;

        Ok(raw::Feed {
            uuid,
            private,
            edited_at,
            status,
            inner,
        })
    }
}

impl ToStruct<raw::Post> for Row<'_> {
    fn to_struct(&self) -> error::Result<raw::Post> {
        let uuid: String = self.get("uuid")?;
        let feed_id: String = self.get("feed_id")?;
        let created_at: u64 = self.get("created_at")?;
        let edited_at: u64 = self.get("edited_at")?;
        let status: u8 = self.get("status")?;
        let crypted: String = self.get("crypted")?;
        let inner = serde_json::from_str::<raw::PostData>(&crypted)
            .map(raw::Inner::Decrypted)
            .unwrap_or_else(|_| raw::Inner::Encrypted(InnerEncryptedData { crypted }));

        let status = PostStatus::try_from(status)?;

        Ok(raw::Post {
            uuid,
            feed_id,
            status,
            created_at,
            edited_at,
            inner,
        })
    }
}

impl ToStruct<raw::User> for Row<'_> {
    fn to_struct(&self) -> error::Result<raw::User> {
        let uuid = self.get::<usize, String>(0)?;
        let feed_id = self.get::<usize, String>(1)?;
        let edited_at = self.get::<usize, u64>(2)?;
        let crypted = self.get::<usize, String>(3)?;

        let inner = serde_json::from_str::<raw::UserData>(&crypted)
            .map(raw::Inner::Decrypted)
            .unwrap_or_else(|_| raw::Inner::Encrypted(InnerEncryptedData { crypted }));

        Ok(raw::User {
            uuid,
            feed_id,
            edited_at,
            inner,
        })
    }
}

impl ToStruct<raw::Tag> for Row<'_> {
    fn to_struct(&self) -> error::Result<raw::Tag> {
        let uuid: String = self.get("uuid")?;
        let feed_id: String = self.get("feed_id")?;
        let edited_at: u64 = self.get("edited_at")?;
        let active: bool = self.get("active")?;
        let crypted: String = self.get("crypted")?;

        let inner = serde_json::from_str::<raw::TagData>(&crypted)
            .map(raw::Inner::Decrypted)
            .unwrap_or_else(|_| raw::Inner::Encrypted(InnerEncryptedData { crypted }));

        Ok(raw::Tag {
            uuid,
            feed_id,
            edited_at,
            active,
            inner,
        })
    }
}

impl ToStruct<raw::SyncServer> for Row<'_> {
    fn to_struct(&self) -> error::Result<raw::SyncServer> {
        let uuid: String = self.get("uuid")?;
        let server_name: String = self.get("server_name")?;
        let server_type: String = self.get("server_type")?;
        let server_data: String = self.get("server_data")?;
        let mode: String = self.get("mode")?;
        let last_sync: u64 = self.get("last_sync")?;

        Ok(raw::SyncServer {
            uuid,
            server_name,
            server_type: server_type.into(),
            server_data,
            mode: mode.into(),
            last_sync,
        })
    }
}

impl ToStruct<raw::SyncFeed> for Row<'_> {
    fn to_struct(&self) -> error::Result<raw::SyncFeed> {
        let feed_id: String = self.get("feed_id")?;
        let pull_mode: String = self.get("pull_mode")?;
        let pull_servers: String = self.get("pull_servers")?;
        let push_mode: String = self.get("push_mode")?;
        let push_servers: String = self.get("push_servers")?;

        Ok(raw::SyncFeed {
            feed_id,
            pull_mode: pull_mode.into(),
            pull_servers: serde_json::from_str(&pull_servers).unwrap(),
            push_mode: push_mode.into(),
            push_servers: serde_json::from_str(&push_servers).unwrap(),
        })
    }
}
