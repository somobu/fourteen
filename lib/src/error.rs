//! Some kind of universal error

use std::{
    backtrace::Backtrace,
    num::{ParseIntError, TryFromIntError},
    string::FromUtf8Error,
};

use super::raw;

static mut COLORED_BACKTRACE: bool = false;

fn use_colors() -> bool {
    unsafe { COLORED_BACKTRACE }
}

pub fn set_colored_backtrace(use_colors: bool) {
    unsafe { COLORED_BACKTRACE = use_colors }
}

pub type Result<T> = core::result::Result<T, Error>;

pub struct Error {
    pub kind: ErrorKind,
    pub bt: Backtrace,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        let bt = Backtrace::force_capture();
        Self { kind, bt }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{:?}", self.kind).unwrap();

        let capture = format!("{:#?}", self.bt);
        write!(f, "{capture}")
    }
}

const TERM_BOLD: &str = "\x1b[1;1m";
const TERM_RESET: &str = "\x1b[1;0m";

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{:?}", self.kind).unwrap();

        let term_bold = if use_colors() { TERM_BOLD } else { "" };

        let term_reset = if use_colors() { TERM_RESET } else { "" };

        let capture = format!("{:#?}", self.bt)
            .replace("    { fn: \"", format!("  {term_bold}").as_str())
            .replace("\", file: \"", format!("{term_reset} \tat ").as_str())
            .replace("\", line: ", " :")
            .replace(" },", term_reset);
        write!(f, "{capture}")
    }
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind
    }
}

pub fn new(kind: ErrorKind) -> Error {
    Error::new(kind)
}

#[derive(Debug, PartialEq)]
pub enum ErrorKind {
    NotFound(String),
    BadString(String),
    ConversionError(String),

    Sqlite(rusqlite::Error),
    CryptoState(raw::InnerError),
    Gregorian(gregorian::InvalidDate),
}

impl std::error::Error for Error {}

impl From<rusqlite::Error> for Error {
    fn from(value: rusqlite::Error) -> Self {
        match value {
            rusqlite::Error::QueryReturnedNoRows => {
                Self::new(ErrorKind::NotFound(value.to_string()))
            }
            _ => Self::new(ErrorKind::Sqlite(value)),
        }
    }
}

impl From<raw::InnerError> for Error {
    fn from(value: raw::InnerError) -> Self {
        Self::new(ErrorKind::CryptoState(value))
    }
}

impl From<serde_json::Error> for Error {
    fn from(value: serde_json::Error) -> Self {
        Self::new(ErrorKind::ConversionError(value.to_string()))
    }
}

impl From<gregorian::InvalidDate> for Error {
    fn from(value: gregorian::InvalidDate) -> Self {
        Self::new(ErrorKind::Gregorian(value))
    }
}

impl From<gregorian::InvalidDayOfMonth> for Error {
    fn from(value: gregorian::InvalidDayOfMonth) -> Self {
        Self::new(ErrorKind::Gregorian(
            gregorian::InvalidDate::InvalidDayOfMonth(value),
        ))
    }
}

impl From<TryFromIntError> for Error {
    fn from(value: TryFromIntError) -> Self {
        Self::new(ErrorKind::ConversionError(value.to_string()))
    }
}

impl From<ParseIntError> for Error {
    fn from(value: ParseIntError) -> Self {
        Self::new(ErrorKind::ConversionError(value.to_string()))
    }
}

impl From<std::fmt::Error> for Error {
    fn from(value: std::fmt::Error) -> Self {
        Self::new(ErrorKind::ConversionError(value.to_string()))
    }
}

impl From<FromUtf8Error> for Error {
    fn from(value: FromUtf8Error) -> Self {
        Self::new(ErrorKind::ConversionError(value.to_string()))
    }
}

impl From<core::convert::Infallible> for Error {
    fn from(_: core::convert::Infallible) -> Self {
        Self::new(ErrorKind::BadString(
            "Infallible error occurred!".to_string(),
        ))
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::new(ErrorKind::ConversionError(value.to_string()))
    }
}

pub trait OptionToError<T> {
    fn to_result(self) -> Result<T>;
}

impl<T: std::fmt::Debug> OptionToError<T> for Option<T> {
    fn to_result(self) -> Result<T> {
        if self.is_some() {
            Ok(self.unwrap())
        } else {
            Err(new(ErrorKind::NotFound(format!(
                "Option {:?} is empty",
                self
            ))))
        }
    }
}
