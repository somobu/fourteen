//! All [Fourteen](https://gitlab.com/somobu/fourteen) code that is not related to user interface and/or platform.

pub mod container;
pub mod crypto;
pub mod database;
pub mod delta;
pub mod error;
pub mod raw;
pub mod util;
