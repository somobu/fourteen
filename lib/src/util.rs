//! A few utility functions

use std::time::{SystemTime, UNIX_EPOCH};

use crate::error;

pub fn millis() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64
}

pub fn micros() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_micros() as u64
}

pub fn decode_hex<const T: usize>(s: &str) -> error::Result<[u8; T]> {
    let mut rz = [0u8; T];

    let e = (0..s.len())
        .step_by(2)
        .map(|i| u8::from_str_radix(&s[i..i + 2], 16));

    for (i, v) in e.enumerate() {
        if v.is_ok() {
            rz[i] = v?;
        } else {
            return Err(v.unwrap_err().into());
        }
    }

    Ok(rz)
}

#[allow(dead_code)]
pub fn hiding_place() {
    println!(
        "
           │Entrance hidden by     
           │Bricks and rubble      
       ▂▃▂▅▇▅▅▇▄▃                  
    ┳  ║       ║▔▔▔▔▔▔▔            
    │  ╚╗     ╔╝ Saddam Hussein
    │   ║     ║   │                
   6ft  ╚╗   ╔╝   │                
    │====o   ╚════│═════╗          
    │   │║@    ▇▅▆▇▆▅▅█ ║          
    ┷   │╚│═════════════╝          
Air vent│ │Fan
    "
    )
}
