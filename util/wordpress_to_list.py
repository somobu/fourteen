#!/usr/bin/python3

# Small stupid script to convert wordpress XML export to some kind of list of entries
# Needs to be adjusted before use

import time

from xml.etree import ElementTree

from datetime import datetime

tree = ElementTree.parse("localhost.wordpress.2015-07-15.xml")

items = []

for child in tree.getroot()[0]:
    if child.tag == 'item':
        date_text = child.find("pubDate").text
        date_obj = datetime.strptime(date_text, "%a, %d %b %Y %H:%M:%S %z")

        title = child.find("title").text
        
        content = child.find("{http://purl.org/rss/1.0/modules/content/}encoded").text
        if content is None:
            content = ""
        content = content.replace("<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->", "")
        content = content.replace("<!-- wp:paragraph -->\n", "")
        content = content.replace("\n<!-- /wp:paragraph -->", "")
        content = content.replace("<strong>", "**").replace("</strong>", "**")


        items.append({
            "date": date_obj,
            "date_text": date_text,
            "date_millis": int(time.mktime(date_obj.timetuple())*1e3 + date_obj.microsecond/1e3),
            "title": title,
            "content": content
        })

items = sorted(items, key=lambda e: e["date_millis"])

for item in items:
    print("#", item["date_text"], "|", item["title"])
    print(item["date"].strftime("%d.%m.%Y %H:%M"))
    print()
    print(item["content"])
    print()
    print()