#!/usr/bin/python3

import argparse
import datetime
import time
import json

parser = argparse.ArgumentParser(
    prog='Md extractor',
    description='Converts md notes into json posts'
)

exported = ["0000-fourteen", "2014-fourteen", "2015-fourteen", "2016-fourteen", "2019-fourteen", "2020-fourteen", "2021-fourteen", ]
handwritten = ["2011-oldbook","2012-oldbook","2021-journal","2022-journal","2023-journal"]

feed_id = "fourteen"
author_id = "somobu"

filename = "nofile"
id_counter = 0


class Post:
    def __init__(self):
        global id_counter
        id_counter += 1
        self.id = filename + "-" + str(id_counter)
        self.feed_id = feed_id
        self.author_id = author_id
        self.date = 0
        self.title = ""
        self.text = ""
        self.format = "Default"  # May be: Default, Featured
        self.featured_image = ""


class PostEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Post):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)


def prettify_text(text):
    text = text.strip()

    while text.find("\n\n\n") != -1:
        text = text.replace("\n\n\n", "\n\n")

    return text


def parse_as_fourteen_export(file):
    posts = []
    post = None
    has_time = False

    for line in file.readlines():
        trimmed = line.strip()

        if trimmed.startswith('# '):
            if post is not None: 
                post.text = prettify_text(post.text)
                posts.append(post)

            post = Post()

            header = trimmed[1:].strip()
            has_time = False
            post.title = header
            print()
            print("Header:    ", header)

        elif trimmed.startswith("![Post image]"):
            post.format = "Featured"
            post.featured_image = trimmed[14:-1].replace("photos", "")
            print("   img:    ", post.featured_image)

        else:
            if trimmed and not has_time:    # Non-empty string, no time yet
                raw_time = trimmed[1:11]
                parsed_time = datetime.datetime.strptime(raw_time, '%d.%m.%Y')
                parsed_time += datetime.timedelta(hours=int(12))
                post.date = int(time.mktime(parsed_time.timetuple())) * 1000

                raw_text = trimmed[12:]
                post.text = raw_text

                print(raw_time, raw_text[:50])
                has_time = True
            else:
                post.text += "\n" + trimmed
                print("         : ", trimmed[:50])

    return posts


def parse_as_handwritten(file):
    posts = []
    post = None

    for line in file.readlines():
        trimmed = line.strip()

        if trimmed.startswith("**"):
            if post is not None:
                post.text = prettify_text(post.text)
                posts.append(post)
            post = Post()

            raw_time = trimmed[2:12]
            parsed_time = datetime.datetime.strptime(raw_time, '%d.%m.%Y')
            parsed_time += datetime.timedelta(hours=int(12))

            post.title = raw_time
            post.date = int(time.mktime(parsed_time.timetuple())) * 1000
            post.text = trimmed[14:].strip()

            print()
            print(raw_time, "", post.text[:50])
        else:
            post.text += "\n" + trimmed
            print("         : ", trimmed[:50])

    return posts


all_posts = []

for n in exported:
    print()
    print()
    print("### PROCESSING - EXPORTED", n)
    print()

    filename = n
    file = open("userdata/src/" + filename + ".md", 'r')
    posts = parse_as_fourteen_export(file)
    file.close()

    with open("userdata/out/" + filename + '.json', 'w', encoding='utf8') as json_file:
        json.dump(posts, json_file, indent=2, ensure_ascii=False, cls=PostEncoder)

    all_posts += posts


for n in handwritten:
    print()
    print()
    print("### PROCESSING - HANDWRITTEN", n)
    print()

    filename = n
    file = open("userdata/src/" + filename + ".md", 'r')
    posts = parse_as_handwritten(file)
    file.close()

    with open("userdata/out/" + filename + '.json', 'w', encoding='utf8') as json_file:
        json.dump(posts, json_file, indent=2, ensure_ascii=False, cls=PostEncoder)

    all_posts += posts


all_posts.sort(key=lambda x: x.date)
with open('userdata/posts.json', 'w', encoding='utf8') as json_file:
    json.dump(all_posts, json_file, indent=2, ensure_ascii=False, cls=PostEncoder)