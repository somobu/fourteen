#!/usr/bin/python3

import json
import uuid

from datetime import datetime

print("Use this tool to generate posts.json w/ defined meta-data and stub content")
print()
print("First, you are to provide some basic data")

feed = str(input(" 1. Feed id [fourteen]: "))
if not feed.strip(): feed = "fourteen"

author = str(input(" 2. Author id [somobu]: "))
if not author.strip(): author = "somobu"

print()
print("Now, we can start filling posts")
print()

posts = []

while True:
  print("Creating next post. Leave title empty to finish")
  title = str(input(" Title: "))
  if not title.strip(): break
  
  id = str(uuid.uuid4())
  while True:
    date_str = str(input(" Date: "))
    try:
      date = datetime.strptime(date_str, '%Y-%m-%d')
      date = int(date.timestamp() * 1000)
      date += 12 * 60 * 60 * 1000
      break;
    except:
      print(" Failed to parse date in format %Y-%m-%d. Lets try again")

  featured = str(input(" Featured img []: ")).strip()
  format = "Default"
  if featured:
    format = "Featured"

  text = "TEXT STUB"

  posts.append({
    "id": id,
    "feed_id": feed,
    "author_id": author,
    "date": date,
    "title": title,
    "text": text,
    "format": format,
    "featured_image": featured,
  })

  print()

print(json.dumps(posts))



