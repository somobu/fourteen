#!/bin/bash

# Move all *.png in this folder to match fourteen's upload structure
# and rename files by its modification date.

set -euxo pipefail

for f in *.png
do
  d=$(date -r "$f" +%Y/%m/)
  mkdir -p $d

  t=$(date -r "$f" +%Y/%m/%Y-%m-%dT%H:%M:%S)
  mv -n -- "$f" "${t}.png"
done



