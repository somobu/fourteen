#!/bin/bash
set -euxo pipefail

# Remove when #12625 is stabilized
# #1265: https://github.com/rust-lang/cargo/pull/12625
REMAP="\
    --remap-path-prefix $(pwd)=/build/\
    --remap-path-prefix $HOME/.cargo/registry=/registry/\
    --remap-path-prefix $HOME/.cargo/git=/git/\
    --remap-path-prefix $HOME/.local/lib/cargo=cargo\
    --remap-path-prefix $HOME/.local/lib/rustup=rustup\
"

RUSTFLAGS=$REMAP cargo build --bin fourteen-cli --release
RUSTFLAGS=$REMAP cargo build --bin fourteen-qt --release --no-default-features --features gettext
