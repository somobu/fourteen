# Fourteen

Desktop-first blog-like diary/journal application

![](doc/res/screenshot_greeter.png)


## Features

- Posts feed, post editor, media library;
- Tags and regex-based tag suggestion system;
- User-defined emoji ([details](doc/emoji.md));
- Encryption ([details](doc/crypto.md));
- Synchronization ([details](doc/sync.md));
- Localization (via gettext) -- translations welcome;


## What to expect in the future

- Windows support;
- Post editor overhaul;
- Full-text search;
- "Places & Events" feature;


## How to build & run

Option 1 (linux only): grab AppImage from [the latest release](https://gitlab.com/somobu/fourteen/-/releases);

Option 2 (linux and windows): follow the build instructions of [fourteen desktop readme](https://gitlab.com/somobu/fourteen/-/blob/master/desktop/README.md);


## Download

See [releases](https://gitlab.com/somobu/fourteen/-/releases) page.


## License

[GNU GPL v3](LICENSE.md)

Components used:
- [Inter Font](https://github.com/rsms/inter/) by Rasmus Andersson under [OFL](https://github.com/rsms/inter/blob/master/LICENSE.txt)
- [Material Icons](https://pictogrammers.com/) by Pictogrammers under [Apache 2.0](https://pictogrammers.com/docs/general/license/)
