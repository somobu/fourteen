#!/bin/bash
set -euxo pipefail

docker run -p 80:8080 \
    -v ./src:/var/www/html \
    -v ./../lib/sql:/var/www/sql \
    -v ./../testdata:/var/www/data \
    --rm -it $(docker build -q .)
