<?php
$feed_id = $path_segments[1];
$parsedown->feed_id = $feed_id;


// Query feed info
$statement = $db->prepare(file_get_contents('../sql/get_feed.sql'));
$statement->bindValue(1, $feed_id);
$result = $statement->execute();

$feed = (object) $result->fetchArray(SQLITE3_ASSOC);
$feed_data = json_decode($feed->crypted);

if ($feed_data == null) {
    // Also happens when feed is malformed / encrypted
    include("render/feed/not_found.php");
    return;
}

// Try to get feed date and/or query params
$display_error = "";
$query_tag = "";
$feed_date = new DateTime();


if (count($path_segments) > 2) {
    if (preg_match('/^[0-9]{4}-[0-9]{2}$/', $path_segments[2])) {
        $feed_date->setDate(
            substr($path_segments[2], 0, 4),
            substr($path_segments[2], 5, 2),
            1
        );
    } else if ($path_segments[2] == "tag") {
        if (count($path_segments) > 3) {
            $query_tag = $path_segments[3];
        } else {
            $display_error = 'No tag present';
        }
    } else {
        $display_error =  'Malformed feed subsegment';
    }
} else {
    $statement = $db->prepare(file_get_contents('../sql/get_newest_post_date.sql'));
    $statement->bindValue(1, $feed_id);
    $result = $statement->execute();

    $last_post_date = $result->fetchArray(SQLITE3_ASSOC)["created_at"];

    $feed_date->setTimestamp($last_post_date / 1000);

    $feed_date->setDate(
        $feed_date->format("Y"),
        $feed_date->format("n"),
        1
    );
}

$feed_date->setTime(0, 0, 0, 0);



include("render/feed/header.php");

echo '<div id="feed_body">';
echo '<div id="feed_posts">';



// Query months
$statement = $db->prepare(file_get_contents('../sql/get_years_and_months.sql'));
$statement->bindValue(1, $feed_id);
$result = $statement->execute();

$toc_months = [];

for (;;) {
    $month = $result->fetchArray(SQLITE3_ASSOC);
    if ($month == null) {
        break;
    }

    $dt = new DateTime();
    $dt->setTimestamp($month["t"] / 1000);

    array_push($toc_months, $dt);
}


if ($display_error != "") {
    include("render/feed/nonfatal_error.php");
} else {
    if ($query_tag != "") {
        $statement = $db->prepare(file_get_contents('../sql/get_posts_by_tag.sql'));
        $statement->bindValue(1, $query_tag);
        $statement->bindValue(2, $feed_id);
        $statement->bindValue(3, $feed_id);

        $result = $statement->execute();

        $month_title_value = "Tag: " . $query_tag;
    } else {
        $statement = $db->prepare(file_get_contents('../sql/get_posts_timed.sql'));
        $statement->bindValue(1, $feed_id);
        $statement->bindValue(2, $feed_id);
        $statement->bindValue(3, $feed_date->getTimestamp() * 1000);

        $date_end = DateTime::createFromInterface($feed_date);
        $date_end->add(DateInterval::createFromDateString('1 month'));
        $statement->bindValue(4, $date_end->getTimestamp() * 1000);

        $result = $statement->execute();

        $month_title_value = $feed_date->format('M Y');
    }

    include("render/feed/month_title.php");

    $toc_posts = [];

    for (;;) {
        $post = $result->fetchArray(SQLITE3_ASSOC);
        if ($post == null) {
            break;
        }

        $post = (object) $post;
        $post_data = json_decode($post->crypted);

        array_push($toc_posts, array(
            'id' => $post->uuid,
            'title' => $post_data->title,
        ));

        include("render/post/post.php");
    }

    if ($query_tag == "") {
        include("render/feed/bottom_buttons.php");
    }
}

echo '</div>'; // Closes #feed_posts

include("render/feed/toc.php");

echo '</div>'; // Closes #feed_body
