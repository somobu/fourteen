<?php

class ParsedownExtended extends Parsedown
{

    private $baseImagePath = '/data/';
    public $feed_id = "";

    protected function inlineImage($excerpt)
    {
        $image = parent::inlineImage($excerpt);

        if (!isset($image)) {
            return null;
        }

        $image['element']['attributes']['src'] = $this->baseImagePath . $this->feed_id . "/" . $image['element']['attributes']['src'];

        return $image;
    }
}
