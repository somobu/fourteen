<?php
$full_path = parse_url($_SERVER['REQUEST_URI'])['path'];
$path_segments = explode('/', $full_path);

$cfg = include('config.php');
$db = new SQLite3($cfg->userdir . '/database.sqlite', SQLITE3_OPEN_READONLY);

include("lib/Parsedown.php");
include("lib/ParsedownExtended.php");
$parsedown = new ParsedownExtended();

include("render/page_start.php");

switch ($path_segments[1]) {
    case "":
        include('greeter.php');
        break;
    default:
        include('feed.php');
};

include("render/page_end.php");
