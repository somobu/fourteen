<?php
$human_created = date("d.m.Y", $post->created_at / 1000);
?>

<div class="post" id="<?php echo $post->uuid ?>">
    <div class="post_info_wrapper">
        <div class="post_info">
            <?php
            $block_title = $post_data->title;
            include(__DIR__ . "/../block_header.php");
            ?>
            <div class="post_info_short"><?php echo $human_created . " | " . $post_data->created_by ?></div>
            <div class="post_info_long">
                <?php
                $entry_img = "/img/ic_map.png";
                $entry_title = "Unknown";
                include("info_entry.php");
                ?>
                <?php
                $entry_img = "/img/ic_calendar.png";
                $entry_title = $human_created;
                include("info_entry.php");
                ?>
                <?php
                $entry_img = "/img/ic_user.png";
                $entry_title = $post_data->created_by;
                include("info_entry.php");
                ?>
            </div>
        </div>
    </div>
    <div class="post_body">
        <?php

        if ($post_data->format == "Featured") {
            include('f_img.php');
        }

        $out = trim($post_data->text);

        foreach ($post_data->tags as $key => $value) {
            $out = $out . "\n\n[" . ($key + 1) . "]: /" . $feed_id . "/tag/" . $value;
        } ?>

        <div class="post_content">
            <?php echo $parsedown->text($out) ?>
        </div>

        <!-- <pre><?php var_dump($post) ?></pre> -->
        <!-- <pre><?php var_dump($post_data) ?></pre> -->
    </div>

</div>

<?php
unset($human_created);
?>