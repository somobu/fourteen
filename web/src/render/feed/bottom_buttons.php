<div id="feed_bottom_btns">

    <?php
    $page_idx = array_search($feed_date, $toc_months);

    if ($page_idx != 0) {
        $url = '/' . $feed_id . '/' . $toc_months[$page_idx - 1]->format('Y-m');
        echo '<a href="' . $url . '">‹&nbsp;&nbsp;Prev</a>';
    }
    ?>

    <a href="#feed_header">Up</a>

    <?php
    if ($page_idx < count($toc_months) - 1) {
        $url = '/' . $feed_id . '/' . $toc_months[$page_idx + 1]->format('Y-m');
        echo '<a href="' . $url . '">Next&nbsp;&nbsp;›</a>';
    }
    ?>
</div>