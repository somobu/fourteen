<div id="feed_header">

    <?php if (count($feed_data->header_imgs) > 0) {
        $rand_header = $feed_data->header_imgs[rand(0, count($feed_data->header_imgs) - 1)];
    ?>
        <a class="fh_i" href="/<?php echo $feed_id ?>">
            <img src="<?php echo "/data/" . $feed->uuid . "/" . $rand_header ?>" />
        </a>
    <?php } ?>

    <a class="fh_a" href="/<?php echo $feed_id ?>"><?php echo $feed_data->title ?></a>
    <p><?php echo $feed_data->descr ?></p>
</div>