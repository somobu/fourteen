<div id="feed_toc">
    <div>
        <?php
        $block_title = "TOC";
        include(__DIR__ . "/../block_header.php");
        ?>

        <div id="toc_content">
            <?php
            $feed_year = $feed_date->format("Y");
            $feed_month = $feed_date->format("M");

            $last_year = '';
            $last_month = '';

            foreach ($toc_months as $month_date) {
                $cur_year = $month_date->format("Y");
                $cur_month = $month_date->format("M");

                $url = '/' . $feed_id . '/' . $month_date->format("Y-m");

                if ($cur_year != $last_year) {
                    echo '<a class="toc_year" href="' . $url . '">' . $month_date->format("Y") . '</a>';
                    $last_year = $cur_year;
                }

                if ($cur_year == $feed_year) {
                    echo '<a class="toc_month" href="'. $url . '">' . $month_date->format("F") . '</a>';

                    if ($cur_month == $feed_month) {
                        foreach ($toc_posts as $postinfo) {
                            echo '<a class="toc_post" href="#' . $postinfo['id'] . '">' . $postinfo['title'] . '</a>';
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
</div>