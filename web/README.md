# Fourteen

## Build and run

Generate test database by `cd desktop ; cargo run -- -d ../testdata`

- Install `docker`
- Start server: `./run.sh`
- Visit `http://localhost/test`

## Mount points and urls

| Container path    | Repo path     | Url   | Usage             |
| ---               | ---           | ---   | ---               |
| /var/www/html/    | ./web/src/    | /     | Website itself    |
| /var/www/sql/     | ./lib/sql/    | N/A   | SQL queries       |
| /var/www/data/    | ./testdata/   | /data | User data         |


## TODO

- feed page (html, css)
- greeter page (html, css)
