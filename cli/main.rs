use std::{
    io::{stdin, BufRead},
    path::Path,
};

use clap::{Parser, Subcommand};

use fourteen_lib::{
    database::Database,
    delta::Delta,
    error,
    raw::{self},
    util::millis,
};
use uuid::Uuid;

const DB_NAME: &str = "/database.sqlite";

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct CliCmds {
    /// Path to 'userdata' dir, defaults to current folder in release
    #[arg(short, long)]
    dir: Option<String>,

    #[arg(short, long)]
    colored: bool,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Grab data from database
    Export {
        /// Feed name
        #[arg(short, long)]
        feed: String,

        /// Time, millis since unix epoch
        ///
        /// Defaults to 0
        #[arg(short, long)]
        since: Option<String>,

        /// Decrypts inner data with provided key
        ///
        /// Will crash if data is not encrypted
        ///
        /// Decryption occurs before `encrypt_with`
        #[arg(short, long)]
        decrypt_with: Option<String>,

        /// Encrypts inner data with provided key
        ///
        /// Will crash if data is already encrypted
        ///
        /// Encryption occurs after `decrypt_with`
        #[arg(short, long)]
        encrypt_with: Option<String>,
    },

    /// Write data to database
    Import,

    /// Create new encrypted feed
    NewEncrypted {
        /// Feed name
        #[arg(short, long)]
        name: String,

        /// Encryption key, 32 hex symbols
        #[arg(short, long)]
        key: String,
    },

    /// Create stub feed w/ given uuid
    NewStub { uuid: String },

    /// Debug use only
    ManualMigrate,

    /// Debug use only
    ManualAction,
}

fn main() {
    let cli = CliCmds::parse();

    if cli.colored {
        error::set_colored_backtrace(true);
    }

    let workdir = cli.dir.clone();

    let backend = if workdir.is_some() {
        let file = workdir.clone().unwrap() + DB_NAME;
        Database::open_file(&file)
    } else if cfg!(debug_assertions) {
        Database::open_in_memory()
    } else {
        let file = ".".to_string() + DB_NAME;
        let p = Path::new(&file).parent().unwrap();
        std::fs::create_dir_all(p).unwrap();

        Database::open_file(&file)
    }
    .expect("unable to setup backend");

    match cli.command {
        Commands::Export {
            feed,
            since,
            encrypt_with,
            decrypt_with,
        } => {
            let since: u64 = since.unwrap_or("0".to_string()).parse().unwrap();
            let delta = Delta::from_db(&backend, &feed, since, encrypt_with, decrypt_with);
            let delta_json = serde_json::to_string(&delta).unwrap();
            println!("{delta_json}");
        }
        Commands::Import => {
            let line = stdin().lock().lines().next().unwrap();
            let line = line.expect("Unable to read line from stdio");

            let delta: Delta = serde_json::from_str(&line).unwrap();
            let rz = delta.into_db(&backend).unwrap();
            let rz_json = serde_json::to_string(&rz).unwrap();
            println!("{rz_json}");
        }
        Commands::NewEncrypted { name, key } => {
            let feed_id = Uuid::new_v4().to_string();
            let user_id = Uuid::new_v4().to_string();

            let feed = raw::Feed {
                uuid: feed_id.clone(),
                edited_at: millis(),
                private: true,
                status: raw::FeedStatus::Visible,
                inner: raw::Inner::Decrypted(raw::FeedData {
                    owned_by: user_id.clone(),
                    edited_by: user_id.clone(),
                    title: name.clone(),
                    descr: "".to_string(),
                    header_imgs: vec![],
                })
                .encrypt(&key)
                .unwrap(),
            };
            backend.insert_feed(&feed).unwrap();

            let user = raw::User {
                uuid: user_id.clone(),
                feed_id: feed_id.clone(),
                edited_at: millis() + 1,
                inner: raw::Inner::Decrypted(raw::UserData {
                    created_by: user_id.clone(),
                    created_at: millis(),
                    edited_by: user_id.clone(),
                    name: "Author".to_string(),
                    icon: "".to_string(),
                })
                .encrypt(&key)
                .unwrap(),
            };
            backend.insert_user(&user).unwrap();

            backend.set_current_user(&feed_id, &user_id).unwrap();

            println!("Successfully added new feed {name} (id {feed_id})");
        }
        Commands::NewStub { uuid } => {
            let feed = raw::Feed {
                uuid,
                edited_at: 0,
                private: false,
                status: raw::FeedStatus::Visible,
                inner: raw::Inner::Decrypted(raw::FeedData {
                    owned_by: "".to_string(),
                    edited_by: "".to_string(),
                    title: "Stub".to_string(),
                    descr: "".to_string(),
                    header_imgs: vec![],
                }),
            };
            backend.insert_feed(&feed).unwrap();
        }
        Commands::ManualMigrate => {
            // hexdump -vn16 -e'4/4 "%08X" 1 "\n"' /dev/urandom
            let key = "00000000000000000000000000000000".to_string();
            unimplemented!("Manual migrate w/ {key}")
        }
        Commands::ManualAction => {}
    }

    backend.close();
}
