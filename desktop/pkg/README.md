# AppImage packaging

1. Build project in release mode: `cargo build --release`;
2. Get [appimage-builder](https://appimage-builder.readthedocs.io/en/latest/intro/install.html);
3. Build AppImage `cd desktop/pkg ; appimage-builder --recipe recipe.yml`;
