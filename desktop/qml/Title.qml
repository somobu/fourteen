import QtQuick 2.15

Item {  // Header
    property string text: ""
    property string icon: ""
    id: header
    width: dp(240)
    height: dp(32)

    Rectangle {
        width: parent.width
        height: dp(4)
        color: theme.gray_bold
    }

    Rectangle {
        width: dp(32)
        height: dp(32)
        color: theme.gray_bold
    }

    Image {
        width: dp(16)
        height: dp(16)
        x: dp(8)
        y: dp(8)
        source: header.icon
    }

    Text {
        x: dp(40)
        y: dp(4)
        width: parent.width - x
        height: dp(28)
        text: header.text
        font.family: theme.inter_bold_extra.name
        font.pixelSize: dp(18)
        lineHeight: dp(22)
        lineHeightMode: Text.FixedHeight
        verticalAlignment: Text.AlignVCenter
        color: theme.gray_bold
        elide: Text.ElideRight
    }
}