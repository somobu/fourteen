import QtQuick 2.15

import ".."
import "../reusable"

Item {
    property string title: ""
    property string feed_uuid: ""
    property string uuid: ""
    property bool pull_enabled: true
    property bool push_enabled: true
    property bool is_pull: true
    property bool is_push: false

    id: section_root
    width: parent.width
    height: dp(40)


    HoverHandler {
        id: section_hover
        acceptedButtons: Qt.NoButton
    }


    Text {
        width: dp(240)
        height: parent.height
        text: section_root.title
        font.family: theme.inter_bold.name
        font.pixelSize: dp(18)
        color: (is_pull || is_push) ? theme.gray_bold : theme.gray_light
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    BtnIcon {
        id: pull_btn
        x: parent.width - width - push_btn.width - dp(8)
        height: dp(32)
        y: (parent.height - height) / 2
        label: gettext("Pull")
        cursor: pull_enabled ? Qt.PointingHandCursor : Qt.ForbiddenCursor
        backgroundColorNormal: pull_enabled ? (is_pull ? theme.accent : theme.gray_bold) : theme.gray_light
        backgroundColorHover: is_pull ? theme.accent_hover : theme.gray_light_hover
        onClick: { 
            if (pull_enabled) {
                is_pull = !is_pull
                changeFeedPull(section_root.feed_uuid, section_root.uuid, is_pull)
            } 
        }
    }

    BtnIcon {
        id: push_btn
        x: parent.width - width
        height: dp(32)
        y: (parent.height - height) / 2
        label: gettext("Push")
        cursor: push_enabled ? Qt.PointingHandCursor : Qt.ForbiddenCursor
        backgroundColorNormal: push_enabled ? (is_push ? theme.accent : theme.gray_bold) : theme.gray_light
        backgroundColorHover: is_push ? theme.accent_hover : theme.gray_light_hover
        onClick: { 
            if (push_enabled) {
                is_push = !is_push
                changeFeedPush(section_root.feed_uuid, section_root.uuid, is_push)
            }
        }
    }


}