import QtQuick 2.15

import ".."
import "../reusable"

Item {
    property string title: ""
    property bool show_btn: false
    signal addClick

    id: section_root
    width: parent.width
    height: dp(32)

    HoverHandler {
        id: section_hover
        acceptedButtons: Qt.NoButton
    }

    Text {
        height: parent.height
        width: parent.width
        text: section_root.title
        font.family: theme.inter_bold.name
        font.pixelSize: dp(22)
        color: theme.gray_bold
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    BtnIconOnly {
        visible: section_root.show_btn && section_hover.hovered
        width: dp(32)
        height: dp(32)
        x: parent.width - 2*dp(16)
        icon: "imgs/plus_sand32.png"
        onClick: () => section_root.addClick()
    }

}