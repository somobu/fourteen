import QtQuick 2.15

import ".."
import "../reusable"

Item {
    property string title: ""
    property string mode: ""
    property var lastSyncTime: 0

    signal syncClick
    signal editClick

    id: section_root
    width: parent.width
    height: dp(40)

    property var modes: {
            "Both": gettext("pull/push"),
            "Pull": gettext("pull only"),
            "Push": gettext("push only"),
            "None": gettext("dont sync")
        }

    function getDisplayMode() {
        return modes[mode] || "???"
    }

    function getDisplaySyncDelta() {
        let then = section_root.lastSyncTime;

        if (then < 1000) {
            return gettext("never")
        }

        let delta = (Date.now() - then) / 1000;

        // Check that we cannot pick bigger time unit
        if (delta / 60 < 1) {
            return gettext("just now")
        }

        // To minutes
        delta /= 60;
        if (delta / 60 < 1) {
            return Math.floor(delta) + " " + gettext("minutes ago")
        }

        // To hours
        delta /= 60;
        if (delta / 24 < 1) {
            return Math.floor(delta) + " " + gettext("hours ago")
        }

        // To days
        delta /= 24;
        return Math.floor(delta) + " " + gettext("days ago")
    }

    HoverHandler {
        id: section_hover
        acceptedButtons: Qt.NoButton
    }

    Row {
        width: parent.width
        height: parent.height

        Text {
            width: dp(240)
            height: parent.height
            text: section_root.title
            font.family: theme.inter_bold.name
            font.pixelSize: dp(18)
            color: theme.gray_bold
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }

        Item {
            width: dp(32)
            height: parent.height
        }

        Text {
            width: dp(176)
            height: parent.height
            text: section_root.getDisplayMode()
            font.family: theme.inter_bold.name
            font.pixelSize: dp(18)
            color: theme.gray_light
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            height: parent.height
            text: gettext("last sync: ") + section_root.getDisplaySyncDelta()
            font.family: theme.inter_bold.name
            font.pixelSize: dp(16)
            color: theme.gray_light
            verticalAlignment: Text.AlignVCenter
        }
    }

    BtnIconOnly {
        visible: section_hover.hovered
        width: dp(32)
        height: dp(32)
        x: parent.width - 4.5*dp(16)
        y: dp(4)
        icon: "imgs/sync_sand32.png"
        onClick: () => section_root.syncClick()
    }

    BtnIconOnly {
        visible: section_hover.hovered
        width: dp(32)
        height: dp(32)
        x: parent.width - 2*dp(16)
        y: dp(4)
        icon: "imgs/edit_sand32.png"
        onClick: () => section_root.editClick()
    }

}