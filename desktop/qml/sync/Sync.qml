import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import ".."
import "../reusable"

Item {
    id: sync_root

    function serverById(sid) {
        for (let server of fourteen.pagedata.servers) {
            if (server.uuid == sid) return server;
        }

        return null;
    }

    function feedById(sid) {
        for (let feed of fourteen.pagedata.raw_feeds) {
            if (feed.uuid == sid) return feed;
        }

        return null;
    }

    function syncFeedById(sid) {
        for (let feed of fourteen.pagedata.sync_feeds) {
            if (feed.feed_id == sid) return feed;
        }

        return null;
    }

    function isPullServer(server_id) {
        var server = serverById(server_id);
        if (server.mode == "None") return false;
        if (server.mode == "Push") return false;
        return true;
    }

    function isPushServer(server_id) {
        var server = serverById(server_id);
        if (server.mode == "None") return false;
        if (server.mode == "Pull") return false;
        return true;
    }

    function isPullServerForFeed(server_id, feed_id) {
        var server = serverById(server_id);
        if (server.mode == "None") return false;
        if (server.mode == "Push") return false;

        var feed = syncFeedById(feed_id);
        if (feed == null) return false;

        return feed.pull_servers.includes(""+server_id);
    }

    function isPushServerForFeed(server_id, feed_id) {
        var server = serverById(server_id);
        if (server.mode == "None") return false;
        if (server.mode == "Pull") return false;

        var feed = syncFeedById(feed_id);
        if (feed == null) return false;

        return feed.push_servers.includes(""+server_id);
    }

    function changeFeedPush(feed_id, server_id, is_push) {
        for (let feed of fourteen.pagedata.sync_feeds) {
            if (feed.feed_id == feed_id) {
                // Always remove element
                const index = feed.push_servers.indexOf(server_id);
                if (index > -1) feed.push_servers.splice(index, 1);

                if (is_push) {
                    feed.push_servers.push(server_id);
                }

                return;
            }
        }

        // Nothing is found and we want to add push server
        if (is_push) {
            fourteen.pagedata.sync_feeds.push({
                "feed_id": feed_id,
                "pull_mode": "WhiteList",
                "pull_servers": [],
                "push_mode": "WhiteList",
                "push_servers": [ server_id ]
            });
        }
    }

    function changeFeedPull(feed_id, server_id, is_pull) {
        for (let feed of fourteen.pagedata.sync_feeds) {
            if (feed.feed_id == feed_id) {
                // Always remove element
                const index = feed.pull_servers.indexOf(server_id);
                if (index > -1) feed.pull_servers.splice(index, 1);

                if (is_pull) {
                    feed.pull_servers.push(server_id);
                }

                return;
            }
        }

        // Nothing is found and we want to add pull server
        if (is_pull) {
            fourteen.pagedata.sync_feeds.push({
                "feed_id": feed_id,
                "pull_mode": "WhiteList",
                "pull_servers": [ server_id ],
                "push_mode": "WhiteList",
                "push_servers": []
            });
        }
    }

    Item {
        width: dp(832)
        height: content.height + 2*dp(32)
        anchors.centerIn: parent

        Rectangle {
            x: dp(4) 
            y: dp(4)
            width: parent.width
            height: parent.height
            color: theme.gray_light
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        Column {
            id: content
            x: dp(32)
            y: dp(32)
            width: parent.width - 2*x
           
            Section {
                title: gettext("Servers")
                show_btn: true
                onAddClick: {
                    server_popup.show(null)
                }
            }

            Item {
                width: parent.width
                height: dp(16) - dp(4)
            }

            Text {
                visible: fourteen.pagedata.servers.length == 0
                height: dp(48)
                text: gettext("No sync servers present\nYou can add one by clicking + button while hovering the title above")
                font.family: theme.inter_bold.name
                font.pixelSize: dp(16)
                color: theme.gray_light
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            Repeater {
                model: fourteen.pagedata.servers

                ServerEntry {
                    required property var modelData
                    required property int index

                    title: modelData.server_name
                    mode: modelData.mode
                    lastSyncTime: modelData.last_sync

                    onSyncClick: { fourteen.trigger_sync(modelData.uuid) }
                    onEditClick: { server_popup.show(modelData.uuid) }
                }

            }

            Item {
                width: parent.width
                height: dp(40) - dp(4)
            }

            Section {
                title: gettext("Feeds")
            }

            Item {
                width: parent.width
                height: dp(16) - dp(4)
            }

            Text {
                visible: fourteen.pagedata.raw_feeds.length == 0
                height: dp(48)
                text: gettext("No feeds present\nYou can add one from start screen")
                font.family: theme.inter_bold.name
                font.pixelSize: dp(16)
                color: theme.gray_light
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            Repeater {
                model: fourteen.pagedata.raw_feeds

                FeedEntry {
                    required property int index

                    title: data().name
                    pull_mode: data().pull_mode
                    push_mode: data().push_mode
                    pull_list: data().pull
                    push_list: data().push

                    onEditClick: {
                        feed_popup.show(fourteen.pagedata.raw_feeds[index].uuid)
                    }

                    function data() {
                        var raw = fourteen.pagedata.raw_feeds[index]
                        var name = (raw.private ? "🔒 " : "") + (raw.title || raw.uuid.substring(0,8))

                        var sync = null
                        for (var feed of fourteen.pagedata.sync_feeds) {
                            if (feed.feed_id === raw.uuid) {
                                sync = feed
                                break;
                            }
                        }

                        if (sync == null) {
                            return {
                                "name": name,
                                "pull_mode": "None",
                                "push_mode": "None",
                                "pull": [],
                                "push": [],
                            }
                        } else {
                            var pull_servers = []
                            for (var server of sync.pull_servers) {
                                if (isPullServer(server)) {
                                    pull_servers.push(server)
                                }
                            }

                            var push_servers = []
                            for (var server of sync.push_servers) {
                                if (isPushServer(server)) {
                                    push_servers.push(server)
                                }
                            }

                            var e = {
                                "name": name,
                                "pull_mode": sync.pull_mode,
                                "push_mode": sync.push_mode,
                                "pull": pull_servers,
                                "push": push_servers,
                            }

                            return e;
                        }

                    }
                }

            }

        }
    }

    Item { // Server setup popup
        width: parent.width
        height: parent.height
        id: server_popup
        visible: false

        // Null when adding a new server
        property var target_server_id: null

        property var modes_indices: {
            "Both": 0,
            "Pull": 1,
            "Push": 2,
            "None": 3
        }

        function mode_from_idx(idx) {
            for (var key in modes_indices) {
                if (modes_indices[key] == idx) {
                    return key;
                }
            }

            return 3; // None
        }

        function show(uuid) {
            var server = sync_root.serverById(uuid);

            if (server != null) {
                popup_server_name.text = server.server_name;
                popup_server_address.text = server.server_data;
                popup_server_mode_dropdown.currentIndex = modes_indices[server.mode];
                server_popup_btn_delete.visible = true;
            } else {
                popup_server_name.text = "Awesome sync server";
                popup_server_address.text = "127.0.0.1";
                popup_server_mode_dropdown.currentIndex = 0;
                server_popup_btn_delete.visible = false;
            }

            server_popup.target_server_id = uuid;
            server_popup.visible = true;
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onClicked: { 
                server_popup.visible = false;
            }
        }

        Background {
            with_solid_color: false
        }


        Item {
            width: dp(420)
            height: server_popup_content.height + 2*dp(32)
            anchors.centerIn: parent

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.ArrowCursor
                onClicked: { }
            }

            Rectangle {
                x: dp(4) 
                y: dp(4)
                width: parent.width
                height: parent.height
                color: theme.gray_light
            }

            Rectangle {
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Column {
                id: server_popup_content
                x: dp(32)
                y: dp(32)
                width: parent.width - 2*x


                Section {
                    title: gettext("Server settings")
                }

                Item {
                    width: parent.width
                    height: dp(16)
                }

                Text {
                    width: parent.width
                    height: dp(32)
                    text: gettext("Server label")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(18)
                    color: theme.gray_bold
                    verticalAlignment: Text.AlignVCenter
                }

                CustomTextField {
                    id: popup_server_name
                }

                Item {
                    width: parent.width
                    height: dp(16)
                }

                Text {
                    width: parent.width
                    height: dp(32)
                    text: gettext("Server address")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(18)
                    color: theme.gray_bold
                    verticalAlignment: Text.AlignVCenter
                }

                CustomTextField {
                    id: popup_server_address
                }

                Item {
                    width: parent.width
                    height: dp(16)
                }

                Text {
                    width: parent.width
                    height: dp(32)
                    text: gettext("Sync mode")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(18)
                    color: theme.gray_bold
                    verticalAlignment: Text.AlignVCenter
                }

                ComboBox { // Post type selector
                    id: popup_server_mode_dropdown
                    height: dp(32)
                    width: parent.width

                    model: [ gettext("pull/push"), gettext("pull only"), gettext("push only"), gettext("dont sync") ]
                }

                Item {
                    width: parent.width
                    height: dp(32)
                }

                Item { // Bottom buttons
                    width: parent.width
                    height: dp(32)
                    
                    BtnIcon {
                        id: server_popup_btn_delete
                        label: gettext("Delete")
                        backgroundColorNormal: theme.sand_light
                        backgroundColorHover: theme.sand_light
                        textColorNormal: theme.accent
                        textColorHover: theme.accent_hover
                        onClick: {
                            fourteen.delete_sync_server(server_popup.target_server_id);
                            server_popup.visible = false;
                        }
                    }

                    BtnIcon {
                        label: gettext("Confirm")
                        height: parent.height
                        x: parent.width - width
                        onClick: {
                            var orig_server = sync_root.serverById(server_popup.target_server_id || "");
                            var server = {
                                "uuid": server_popup.target_server_id || "",
                                "server_name": popup_server_name.text || "",
                                "server_type": "Ssh",
                                "server_data": popup_server_address.text || "",
                                "mode": server_popup.mode_from_idx(popup_server_mode_dropdown.currentIndex),
                                "last_sync": orig_server == null ? 0 : orig_server.last_sync
                            };
                            fourteen.update_sync_server(JSON.stringify(server));
                            server_popup.visible = false;
                        }
                    }
                }


            }

        }

    }

    Item { // Feed setup popup
        width: parent.width
        height: parent.height
        id: feed_popup
        visible: false

        property var target_feed_id: null

        function show(uuid) {
            feed_popup_title.title = gettext("Feed settings: ") + uuid
            feed_popup.target_feed_id = uuid;
            
            feed_popup_rep.model = fourteen.pagedata.servers
            feed_popup_rep.modelChanged()

            feed_popup.visible = true;
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onClicked: { 
                feed_popup_rep.model = [];
                feed_popup_rep.modelChanged();

                feed_popup.visible = false;
            }
        }

        Background {
            with_solid_color: false
        }


        Item {
            width: dp(570)
            height: feed_popup_content.height + 2*dp(32)
            anchors.centerIn: parent

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.ArrowCursor
                onClicked: { }
            }

            Rectangle {
                x: dp(4) 
                y: dp(4)
                width: parent.width
                height: parent.height
                color: theme.gray_light
            }

            Rectangle {
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Column {
                id: feed_popup_content
                x: dp(32)
                y: dp(32)
                width: parent.width - 2*x

                Section {
                    id: feed_popup_title
                    title: gettext("Feed settings [stub]")
                }

                Item {
                    width: parent.width
                    height: dp(16)
                }

                Text {
                    visible: fourteen.pagedata.servers.length == 0
                    height: dp(48)
                    text: gettext("No sync servers present")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(16)
                    color: theme.gray_light
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }

                Repeater {
                    id: feed_popup_rep
                    model: []
                    visible: fourteen.pagedata.servers.length != 0

                    ServerEntryInPopup {
                        required property var modelData
                        required property int index

                        title: modelData.server_name
                        uuid: modelData.uuid
                        feed_uuid: feed_popup.target_feed_id
                        pull_enabled: isPullServer(modelData.uuid)
                        push_enabled: isPushServer(modelData.uuid)
                        is_pull: isPullServerForFeed(modelData.uuid, feed_popup.target_feed_id)
                        is_push: isPushServerForFeed(modelData.uuid, feed_popup.target_feed_id)
                    }

                }

                Item {
                    width: parent.width
                    height: dp(32)
                }

                Item { // Bottom buttons
                    width: parent.width
                    height: dp(32)
                    
                    BtnIcon {
                        label: gettext("Confirm")
                        height: parent.height
                        x: parent.width - width
                        onClick: {
                            fourteen.update_sync_feeds(JSON.stringify(fourteen.pagedata.sync_feeds));
                            feed_popup.visible = false;
                        }
                    }
                }

            }
        }

    }
}