import QtQuick 2.15

import ".."
import "../reusable"

Item {
    property string title: ""

    property string pull_mode: ""
    property string push_mode: ""
    property var pull_list: []
    property var push_list: []

    signal editClick

    id: section_root
    width: parent.width
    height: dp(40)

    function serverName(sid) {
        let s = serverById(sid);
        if (s != undefined) {
            return s.server_name
        } else {
            return null
        }
    }

    function getDisplaySync() {

        let pull = gettext("pull: ");
        if (pull_mode == "All") {
            pull += gettext("all")
        } else if (pull_mode == "None") {
            pull += gettext("none")
        } else if (pull_list.length == 0){
            pull += gettext("none")
        } else if (pull_list.length == fourteen.pagedata.servers.length){
            pull += gettext("all")
        } else {
            let has_servers = false;

            for (var i = 0; i < pull_list.length - 1; i++) {
                let n = serverName(pull_list[i]);
                if (n != null) {
                    pull += n + "; ";
                    has_servers = true;
                }
            }

            let n = serverName(pull_list[pull_list.length - 1]);
            if (n != null) {
                pull += n;
                has_servers = true;
            }

            if (!has_servers) {
                pull += gettext("none");
            }
        }

        let push = gettext("push: ");
        if (push_mode == "All") {
            push += gettext("All")
        } else if (push_mode == "None") {
            push += gettext("none")
        } else if (push_list.length == 0){
            push += gettext("none")
        } else if (push_list.length == fourteen.pagedata.servers.length){
            push += gettext("all")
        } else {
            let has_servers = false;
            
            for (var i = 0; i < push_list.length - 1; i++) {
                let n = serverName(push_list[i]);
                if (n != null) {
                    push += n + "; "
                    has_servers = true;
                }
            }

            let n = serverName(push_list[push_list.length - 1]);
            if (n != null) {
                push += n;
                has_servers = true;
            }

            if (!has_servers) {
                push += gettext("none");
            }
        }

        return pull + " | " + push;
    }

    HoverHandler {
        id: section_hover
        acceptedButtons: Qt.NoButton
    }

    Row {
        width: parent.width
        height: parent.height

        Text {
            width: dp(240)
            height: parent.height
            text: section_root.title
            font.family: theme.inter_bold.name
            font.pixelSize: dp(18)
            color: theme.gray_bold
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }

        Item {
            width: dp(32)
            height: parent.height
        }
        
        Text {
            height: parent.height
            text: section_root.getDisplaySync()
            font.family: theme.inter_bold.name
            font.pixelSize: dp(16)
            color: theme.gray_light
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
    }

    BtnIconOnly {
        visible: section_hover.hovered
        width: dp(32)
        height: dp(32)
        x: parent.width - 2*dp(16)
        y: dp(4)
        icon: "imgs/edit_sand32.png"
        onClick: () => section_root.editClick()
    }

}