import QtQuick 2.15

QtObject {

    function getMonthName(month_index) {
        return [
            gettext("January"),     gettext("February"),    gettext("March"),       gettext("April"), 
            gettext("May"),         gettext("June"),        gettext("July"),        gettext("August"), 
            gettext("September"),   gettext("October"),     gettext("November"),    gettext("December")
        ][month_index]
    }

    function getMonthNameOf(month_index) {
        return [
            gettext("of January"),     gettext("of February"),    gettext("of March"),       gettext("of April"), 
            gettext("of May"),         gettext("of June"),        gettext("of July"),        gettext("of August"), 
            gettext("of September"),   gettext("of October"),     gettext("of November"),    gettext("of December")
        ][month_index]
    }

    function getDayShort(day_index) {
        return [ 
            gettext("Sun"), gettext("Mon"), gettext("Tue"), gettext("Wed"), gettext("Thu"), gettext("Fri"), gettext("Sat") 
        ][day_index]
    }

    // Supposedly, QML JS's Date() is buggy
    function getFormattedMonth(date_int) {
        var date = new Date(date_int);
        var month = getMonthName(date.getMonth())
        return month + " " + (1900 + date.getYear())
    }

    // Output: 13.02.2000 14:55
    function getFormattedDate(date_int) {
        var d = new Date(date_int);
        return ("0" + d.getDate()).slice(-2) + "." + ("0"+(d.getMonth()+1)).slice(-2) + "." + d.getFullYear()
                + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
    }

    // Output: 13 of February, Mon
    function getFormattedShortDate(date_int) {
        var d = new Date(date_int);
        return d.getDate() + " " + getMonthNameOf(d.getMonth()) + ", " + getDayShort(d.getDay())
    }

    function extraTranslations() {
        gettext("Editor");  // In header nav
        gettext("Search");  // In header nav
    }
}