import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import QtGraphicalEffects 1.15

import Fourteen 1.0

import "header"
import "sync_status"

ApplicationWindow {
    id: window
    visible: true
    title: "Fourteen"
    width: dp(1470)
    height: dp(900)
    minimumWidth: dp(970)
    minimumHeight: dp(650)
    color: "#00000000"
    flags: Qt.WA_TranslucentBackground | Qt.TransparentMode | (fourteen.is_csd() ? Qt.FramelessWindowHint : 0)
    Component.onCompleted: { fourteen.proceed_to_page({"page": "greeter"}) }

    property var dpScale: 1.0

    function hot_reload_pre() {
        pageLoader.source = "";
        pageLoader.active = false;

        popupLoader.source = "";
        popupLoader.active = false;
        popup.visible = false;
    }

    function hot_reload_post() {
        pageLoader.active = false;
        fourteen.proceed_to_page(fourteen.lastpage);
        pageLoader.active = true;

        popupLoader.active = true;
    }

    function sync_status(data) {
        sync_status_popup.handleSyncUpdate(JSON.parse(""+data))
    }

    function gettext(phrase) {
        return fourteen.gettext(phrase)
    }

    function dp(value) {
        return Math.ceil(dpScale * (+(value)));
    }

    Bugs {
        id: bugs
    }

    Fourteen {
        id: fourteen

        // Map "template" - "page qml file"
        property var pages: {
            "404":      "PgNotfound.qml",
            "about":    "about/About.qml",
            "editor":   "editor/Editor.qml",
            "error":    "PgError.qml",
            "feed":     "feed/Feed.qml",
            "greeter":  "greeter/Greeter.qml",
            "key_rq":   "key_rq/KeyRq.qml",
            "map":      "map/Map.qml",
            "loading":  "PgLoading.qml",
            "search":   "search/Search.qml",
            "sync":     "sync/Sync.qml",
            "tag_query":    "tags_query/Page.qml"
        }

        property var lastpage
        property var headdata
        property var pagedata

        onPageSwitch: (data) => {
            pageLoader.source = "";

            // Note: typeof(data) == "object" cuz it is QString, so lets cast it to plain js string:
            var data = ""+data

            data = JSON.parse(data)
            
            var page = data["page"]
            headdata = data["head"]
            pagedata = data["data"]

            if (page in pages) pageLoader.source = pages[page];
            
            header.recalc_nav();
        }

        function proceed_to_page(page) {
            if (page == null) return;
            fourteen.lastpage = page;
            fourteen.proceed_to(JSON.stringify(page));
        }

        function need_csd_borders() {
            return fourteen.is_csd() && window.visibility == Window.Windowed;
        }
    }

    Theme {
        id: theme
    }

    Rectangle { // opacity mask to make rounded corners
        property var pad: fourteen.need_csd_borders() ? dp(8) : 0
        id: mask
        x: pad
        y: pad
        width: window.width - 2*pad
        height: window.height - 2*pad
        radius: fourteen.need_csd_borders() ? dp(8) : 0
        color: "white"
    }

    BorderImage {
        width: window.width
        height: window.height
        border { left: dp(30); top: dp(30); right: dp(30); bottom: dp(30) }
        horizontalTileMode: BorderImage.Stretch
        verticalTileMode: BorderImage.Stretch
        source: "imgs/shadow.9.png"
    }

    Rectangle {
        id: root
        x: mask.pad
        y: mask.pad
        width: window.width - 2*mask.pad
        height: window.height - 2*mask.pad
        layer.enabled: fourteen.need_csd_borders()
        layer.effect: OpacityMask {
            maskSource: mask
        }

        Background {
            id: app_background
            with_solid_color: true
        }

        Loader {
            id: pageLoader
            anchors.fill: parent
        }

        Header {
            id: header
        }

        SyncStatus {
            id: sync_status_popup
            visible: false
            x: parent.width - width - dp(12)
            y: parent.height - height - dp(12)
        }

        Item {
            id: popup
            width: parent.width
            height: parent.height
            visible: false

            property var onMediaPicked  // signature: fn (paths);

            function show_media(onMediaPicked) {
                popup.onMediaPicked = onMediaPicked;
                popup.visible = true;
                
                popupLoader.active = true;
                popupLoader.source = "medialib/MediaPopup.qml";
            }

            function show_tags() {
                popup.visible = true;
                
                popupLoader.active = true;
                popupLoader.source = "tags_list/TagsList.qml";
            }

            function hide() {
                popup.visible = false;
                popupLoader.source = "";
                popupLoader.active = false;
            }

            Background {
                with_solid_color: false
                bg_offset: app_background.bg_offset
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: { 
                    popup.hide(); 

                    if (popup.onMediaPicked != null) {
                        popup.onMediaPicked([]);
                        popup.onMediaPicked = null;
                    }
                }
            }

            Loader {
                id: popupLoader
                anchors.fill: parent
            }
        }

    }

    Item {  // CSD resize indicator -- left
        visible: fourteen.need_csd_borders()
        height: parent.height
        width: parent.width * resizeHandler.gapPercent
        x: 0
        y: 0

        HoverHandler {
            acceptedButtons: Qt.NoButton
            cursorShape: Qt.SizeHorCursor
        }
    }

    Item {  // CSD resize indicator -- right
        visible: fourteen.need_csd_borders()
        height: parent.height
        width: parent.width * resizeHandler.gapPercent
        x: parent.width - width
        y: 0

        HoverHandler {
            acceptedButtons: Qt.NoButton
            cursorShape: Qt.SizeHorCursor
        }
    }

    Item {  // CSD resize indicator -- top
        visible: fourteen.need_csd_borders()
        height: parent.height * resizeHandler.gapPercent
        width: parent.width
        x: 0
        y: 0

        HoverHandler {
            acceptedButtons: Qt.NoButton
            cursorShape: Qt.SizeVerCursor
        }
    }

    Item {  // CSD resize indicator -- bottom
        visible: fourteen.need_csd_borders()
        height: parent.height * resizeHandler.gapPercent
        width: parent.width
        x: 0
        y: parent.height - height

        HoverHandler {
            acceptedButtons: Qt.NoButton
            cursorShape: Qt.SizeVerCursor
        }
    }

    DragHandler {
        id: resizeHandler
        property var gapPercent: 0.01
        enabled: fourteen.need_csd_borders()
        grabPermissions: TapHandler.TakeOverForbidden
        target: null
        onActiveChanged: if (active) {
            const p = resizeHandler.centroid.position;
            let e = 0;
            
            if ((p.x / width) <= gapPercent) { e |= Qt.LeftEdge }
            if ((p.x / width) >= (1-2*gapPercent)) { e |= Qt.RightEdge }
            if ((p.y*1.0 / height) <= gapPercent) { e |= Qt.TopEdge }
            if ((p.y*1.0 / height) >= (1-2*gapPercent)) { e |= Qt.BottomEdge }
            
            if (e != 0) {
                window.startSystemResize(e);
            }
        }
    }
}
