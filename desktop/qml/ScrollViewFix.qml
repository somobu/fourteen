import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ScrollView {
    ScrollBar.vertical.policy: ScrollBar.AlwaysOn
    ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

    property double targetScrollY: 0
    
    onTargetScrollYChanged: {
        contentItem.contentY = targetScrollY;
    }

    Behavior on targetScrollY { PropertyAnimation {} }

    function onWheel(wheel) {
        root_mouse_area.onWheel(wheel);
    }

    MouseArea {
        id: root_mouse_area
        anchors.fill: parent

        onWheel: {
            var praded = root_mouse_area.parent.parent.parent;
            var ci = praded.contentItem;
            ci.contentY -= wheel.angleDelta.y;

            if (ci.contentY + ci.height > ci.contentHeight) {
                ci.contentY = ci.contentHeight - ci.height;
            }

            // Higher priority
            if (ci.contentY < 0) {
                ci.contentY = 0;
            }
        }
        onClicked: mouse.accepted = false;
        onPressed: mouse.accepted = false;
        onReleased: mouse.accepted = false;
        onDoubleClicked: mouse.accepted = false;
        onPositionChanged: mouse.accepted = false;
        onPressAndHold: mouse.accepted = false;
    }

}