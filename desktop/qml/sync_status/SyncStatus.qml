import QtQuick 2.15

import ".."
import "../reusable"

Item {
    id: popup_root

    width: dp(320)
    height: dp(60)

    property string message_action: "Dismiss"

    function handleSyncUpdate(data) {
        popup_root.visible = true

        var t = data.text

        if (t == "Working") {
            title.text = gettext("Sync in progress")
            subtitle.text = gettext("Click to dismiss")
        } else if (t == "Success") {
            title.text = gettext("Sync done")
            subtitle.text = gettext("Click to dismiss")
        } else if (t == "Failed") {
            title.text = gettext("Sync failed!")
            subtitle.text = gettext("Click to view logs")
        }

        message_action = data.action

        if (message_action == "None") {
            mouse_area.cursorShape = Qt.ArrowCursor
        } else {
            mouse_area.cursorShape = Qt.PointingHandCursor
        }
    }

    Rectangle { // Bg
        width: parent.width
        height: parent.height
        color: theme.sand_light
        border.color: theme.gray_bold
        border.width: dp(2)
    }

    Text {  // Title
        id: title
        x: dp(12)
        y: dp(8)
        height: dp(20)
        width: parent.width - 2*x
        text: "..."
        font.family: theme.inter_bold.name
        font.pixelSize: dp(16)
        color: theme.gray_bold
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    Text {  // Subtitle
        id: subtitle
        x: dp(12)
        y: dp(32)
        height: dp(20)
        width: parent.width - 2*x
        text: "..."
        font.family: theme.inter_medium.name
        font.pixelSize: dp(16)
        color: theme.gray_light
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    MouseArea {
        id: mouse_area
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
        onClicked: {
            if (popup_root.message_action == "Dismiss") {
                // Do nothing
            } else if (popup_root.message_action == "OpenLog") {
                fourteen.open_logs_folder();
            } else {
                console.log("TODO: handle sync status popup click action "+popup_root.message_action);
            }

            popup_root.visible = false
        }
    }
}