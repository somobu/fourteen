import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import ".."
import "../reusable"

Item {
    id: media_popup_root

    // Internal props
    property var selectedItems: []
    property var feed: ""
    property var folders: {}

    Component.onCompleted: {
        var {feed, folders} = JSON.parse(fourteen.get_media_entries(fourteen.pagedata.feed_id));

        media_popup_root.feed = feed;
        media_popup_root.folders = folders;

        years_dropdown.model = Object.keys(folders).sort();
        years_dropdown.currentIndex = Object.keys(folders).length - 1;
    }

    function pickFolder(name) {
        gridRepeater.model = media_popup_root.folders[name]
    }

    Rectangle {
        id: lib_root
        anchors.centerIn: parent
        width: Math.min(parent.width * 0.95, dp(832))
        height: Math.min(parent.height * 0.95, dp(768))
        color: theme.sand_light

        MouseArea { // Block background dismiss area
            anchors.fill: parent
            cursorShape: Qt.ArrowCursor
            onClicked: { }
        }

        Text { // "Sry, no media found"
            anchors.centerIn: parent
            text: gettext("No media found. Please drag&drop some pics into editor")
            font.family: theme.inter_bold_extra.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
            visible: years_dropdown.model.length == 0
        }

        ComboBox {
            id: years_dropdown
            model: []
            x: dp(16)
            y: dp(16)
            width: dp(208)
            height: dp(32)
            visible: years_dropdown.model.length > 0

            onCurrentIndexChanged: media_popup_root.pickFolder(years_dropdown.model[currentIndex])

            background: Rectangle {
                implicitWidth: years_dropdown.implicitWidth
                implicitHeight: years_dropdown.implicitHeight
                color: theme.sand_light
                border.width: 0
            }

            indicator: Item { }

            contentItem: Item {
                width: years_dropdown.width
                height: years_dropdown.height

                Rectangle {
                    width: parent.width
                    height: parent.height
                    color: theme.gray_bold
                }

                Text {
                    x: dp(10)
                    height: dp(32)
                    text: years_dropdown.displayText
                    color: theme.sand_light
                    font.family: theme.inter_medium.name
                    font.pixelSize: dp(16)
                    verticalAlignment: Image.AlignVCenter
                    Layout.alignment: Qt.AlignVCenter
                    Layout.leftMargin: dp(10)
                    Layout.fillWidth: true
                }

                Image {
                    x: parent.width - dp(28)
                    y: dp(4)
                    width: dp(24)
                    height: dp(24)
                    source: "../imgs/ic_down.png"
                }
            
            }
        }

        Rectangle { // Top divider
            y: dp(63)
            width: parent.width
            height: dp(2)
            color: theme.gray_light
        }

        ScrollViewFix {
            y: dp(64)
            width: parent.width
            height: parent.height - y - dp(80)
            contentHeight: grid.height + dp(32)
            clip: true
            ScrollBar.vertical.policy: ScrollBar.AsNeeded

            Grid {
                id: grid
                x: dp(16)
                y: dp(16)
                width: lib_root.width - 2*x
                columns: Math.floor(width / dp(100)) - 1
                columnSpacing: dp(16)
                rowSpacing: columnSpacing

                Repeater {
                    id: gridRepeater
                    model: []

                    ImageEntry {
                        required property int index
                        required property string modelData

                        feed_id: media_popup_root.feed
                        source: modelData

                        onSelectionChanged: {
                            if (isSelected) {
                                media_popup_root.selectedItems.push(item);
                            } else {
                                media_popup_root.selectedItems = media_popup_root.selectedItems.filter((i) => i !== item)
                            }
                            pics_counter.count = media_popup_root.selectedItems.length;
                        }
                    }
                }

            }

        }

        Rectangle { // Bottom divider
            y: parent.height - dp(81)
            width: parent.width
            height: dp(2)
            color: theme.gray_light
        }

        Text {
            property int count: 0
            id: pics_counter
            x: dp(16)
            y: (parent.height - dp(80)) + dp(16)
            text: count + gettext(" pictures selected")
            color: theme.gray_bold
            font.family: theme.inter_medium.name
            font.pixelSize: dp(14)
            visible: years_dropdown.model.length > 0
        }


        BtnIcon {
            label: gettext("Select")
            x: parent.width - width - dp(16)
            y: (parent.height - dp(80)) + dp(24)
            visible: years_dropdown.model.length > 0
            onClick: {
                popup.onMediaPicked(media_popup_root.selectedItems);
                popup.hide();
            }
        }

    }

}
