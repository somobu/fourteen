import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property string feed_id: "ERROR_FEED_ID_UNSET"
    property string source: "head_img.png"
    property bool selected: false
    property bool hovered: false
    signal selectionChanged(isSelected: bool, item: string)

    width: dp(100)
    height: dp(100)

    MouseArea { // Block background dismiss area
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { parent.hovered = true }
        onExited:  { parent.hovered = false }
        onClicked: { 
            parent.selected = !parent.selected
            parent.selectionChanged(parent.selected, parent.source)
        }
    }

    Image {
        width: dp(100)
        height: dp(100)
        source:  "image://fourteen/" + parent.feed_id + "/" + parent.source
        fillMode: Image.PreserveAspectCrop
        asynchronous: true
    }

    Rectangle {
        width: parent.width
        height: parent.height
        color: "#30FFFFFF"
        visible: parent.hovered
    }

    Rectangle { // Selection border, inner
        x: dp(4)
        y: dp(4)
        width: parent.width - 2*x
        height: parent.height - 2*y
        color: "#00000000"
        visible: parent.selected

        border.color: theme.sand_bg
        border.width: dp(2)

    }

    Rectangle { // Selection border, outer
        width: parent.width
        height: parent.height
        color: "#30FFFFFF"
        visible: parent.selected

        border.color: theme.accent
        border.width: dp(4)
    }

}
