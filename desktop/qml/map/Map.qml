import QtQuick 2.15
import QtLocation 5.13
import QtPositioning 5.13

Item {
    id: editor_root
    y: dp(48)
    width: parent.width
    height: parent.height - y

    Map {
        anchors.fill: parent
        center: QtPositioning.coordinate(59.9758, 29.7686)
        zoomLevel: 15

        plugin: plugin_osm
    }

    Plugin {
        id: plugin_osm
        name: "osm"

        PluginParameter { name: "osm.useragent"; value: "Fourteen/4.2" }
        PluginParameter { name: "osm.mapping.host"; value: "https://tile.openstreetmap.org/" }
        PluginParameter { name: "osm.mapping.providersrepository.disabled"; value: true }
        PluginParameter { name: "osm.mapping.custom.datacopyright"; value: "All mine" }

    }
}
