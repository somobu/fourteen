import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import "../reusable"

Item {
    width: root.width
    height: dp(48)

    function recalc_nav() {
        header_item_repeater.model = fourteen.headdata;
    }

    Rectangle {
        width: parent.width
        height: parent.height
        color: theme.sand_light
    }

    Rectangle {
        width: parent.width
        height: dp(2)
        y: parent.height
        color: theme.sand_bg
    }

    RowLayout {
        width: parent.width - dp(16) - (fourteen.need_csd_borders() ? dp(8) : 0)
        height: parent.height
        anchors.left: parent.left
        anchors.leftMargin: dp(16)

        Repeater {
            id: header_item_repeater
            
            Text {
                required property var modelData
                required property int index
                height: parent.height
                text: getTitle() + (is_last() ? " " : "  › ")
                font.family: theme.inter_bold_extra.name
                font.pixelSize: dp(16)
                lineHeight: dp(22)
                lineHeightMode: Text.FixedHeight
                verticalAlignment: Text.AlignBottom
                color: is_last() ? theme.gray_bold : theme.gray_light

                function is_last() {
                    return index >= header_item_repeater.model.length - 1;
                }

                function getTitle() {
                    var v = modelData["name"]
                    return v === "" ? gettext("Untitled") : gettext(v)
                }

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onEntered: { parent.color = is_last() ? theme.gray_bold_hover : theme.gray_light_hover }
                    onExited:  { parent.color = is_last() ? theme.gray_bold : theme.gray_light }
                    onClicked: { fourteen.proceed_to_page(modelData.data) }
                }
            }

        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Text {
            height: parent.height - dp(2)
            rightPadding: dp(16)
            text: gettext("Add post")
            font.family: theme.inter_bold.name
            font.pixelSize: dp(16)
            lineHeight: dp(22)
            lineHeightMode: Text.FixedHeight
            verticalAlignment: Text.AlignBottom
            color: theme.gray_bold
            visible: !!fourteen.pagedata.feed_id

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { parent.color = theme.gray_bold_hover }
                onExited:  { parent.color = theme.gray_bold }
                onClicked: { fourteen.proceed_to_page({
                        "page": "editor",
                        "feed": fourteen.pagedata.feed_id
                    }) 
                }
            }
        }

        Text {
            height: parent.height - dp(2)
            text: gettext("Places & events")
            font.family: theme.inter_bold.name
            font.pixelSize: dp(16)
            lineHeight: dp(22)
            lineHeightMode: Text.FixedHeight
            verticalAlignment: Text.AlignBottom
            color: theme.gray_bold

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { parent.color = theme.gray_bold_hover }
                onExited:  { parent.color = theme.gray_bold }
                onClicked: { fourteen.proceed_to_page({"page":"map"}) }
            }
        }

        Item {  // Hamburger
            implicitWidth: dp(48)
            height: dp(48)

            Rectangle {
                id: btnMenuBg
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Image {
                width: dp(24)
                height: dp(24)
                anchors.centerIn: parent
                source: "../imgs/menu.png"
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnMenuBg.color = theme.sand_bg}
                onExited:  { btnMenuBg.color = theme.sand_light }
                onClicked: { headerMenu.visible = true }
            }
        }

        Item {  // Search button
            width: dp(48)
            height: dp(48)

            Rectangle {
                id: btnSearchBg
                width: parent.width
                height: parent.height
                color: theme.accent
            }

            Image {
                width: dp(16)
                height: dp(16)
                anchors.centerIn: parent
                source: "../imgs/search.png"
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnSearchBg.color = theme.accent_hover }
                onExited:  { btnSearchBg.color = theme.accent }
                onClicked: { 
                    searchPanel.visible = true 
                    searchField.forceActiveFocus()
                }
            }
        }

        Item {  // Collapse
            visible: fourteen.is_csd()
            implicitWidth: dp(40)
            height: dp(48)

            Rectangle {
                id: btnCollapseBg
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Image {
                width: parent.width
                height: parent.height
                source: "collapse.png"
                fillMode: Image.PreserveAspectCrop
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnCollapseBg.color = theme.sand_bg}
                onExited:  { btnCollapseBg.color = theme.sand_light }
                onClicked: { window.showMinimized() }
            }
        }

        Item {  // Fullscreen
            visible: fourteen.is_csd()
            implicitWidth: dp(40)
            height: dp(48)

            Rectangle {
                id: btnFullscreenBg
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Image {
                width: parent.width
                height: parent.height
                source: "fullscreen.png"
                fillMode: Image.PreserveAspectCrop
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnFullscreenBg.color = theme.sand_bg}
                onExited:  { btnFullscreenBg.color = theme.sand_light }
                onClicked: { 
                    if (window.visibility == Window.Windowed) {
                        window.showMaximized()
                    } else {
                        window.showNormal()
                    }
                }
            }
        }

        Item {  // Close
            visible: fourteen.is_csd()
            implicitWidth: dp(40)
            height: dp(48)

            Rectangle {
                id: btnCloseBg
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Image {
                width: parent.width
                height: parent.height
                source: "close.png"
                fillMode: Image.PreserveAspectCrop
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnCloseBg.color = theme.sand_bg }
                onExited:  { btnCloseBg.color = theme.sand_light }
                onClicked: { window.close() }
            }
        }
    }

    Item {  // Header menu
        id: headerMenu
        height: btns.height + dp(2)
        width: dp(256)
        x: window.width - width - dp(48) - dp(8)
        y: dp(8)
        visible: false

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
            border.color: theme.gray_light
            border.width: dp(1)
        }

        MouseArea {
            width: parent.width - 2*x
            height: parent.height - 2*y
            x: -dp(16)
            y: -dp(16)
            hoverEnabled: true
            preventStealing: true
            onExited: { headerMenu.visible = false }
        }

        ColumnLayout {
            id: btns
            width: parent.width - dp(2)
            x: dp(1)
            y: dp(1)
            spacing: 0

            BtnHeaderMenu {
                width: parent.width
                label: gettext("Sync")
                icon: "../imgs/sync.svg"
                onHoverEnter: { headerMenu.visible = true }
                onClick: {
                    fourteen.proceed_to_page({"page":"sync"})
                    headerMenu.visible = false
                }
            }

            BtnHeaderMenu {
                width: parent.width
                label: gettext("About")
                icon: "../imgs/info.svg"
                onHoverEnter: { headerMenu.visible = true }
                onClick: { 
                    fourteen.proceed_to_page({"page":"about"})
                    headerMenu.visible = false
                }
            }

        }

    }

    Item {  // Search panel
        id: searchPanel
        width: dp(540)
        height: dp(48)
        x: window.width - width - (fourteen.is_csd() ? dp(3*53) : 0)
        visible: false

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.accent
        }

        TextField {
            id: searchField
            height: dp(40)
            width: dp(440)
            x: dp(4)
            y: dp(4)
            background: Rectangle { color: theme.sand_light }
            onAccepted: { doSearch() }
        }

        Item {
            width: dp(48)
            height: dp(48)
            x: parent.width - 2*dp(48)

            Rectangle {
                id: btnSearchBarBg
                width: parent.width
                height: parent.height
                color: theme.accent
            }

            Image {
                width: dp(16)
                height: dp(16)
                anchors.centerIn: parent
                source: "../imgs/search.png"
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnSearchBarBg.color = theme.accent_hover}
                onExited:  { btnSearchBarBg.color = theme.accent }
                onClicked: { doSearch() }
            }
        }

        Item {
            width: dp(48)
            height: dp(48)
            x: parent.width - 1*dp(48)

            Rectangle {
                id: btnCloseBarBg
                width: parent.width
                height: parent.height
                color: theme.accent
            }

            Image {
                width: dp(16)
                height: dp(16)
                anchors.centerIn: parent
                source: "../imgs/close_sand.png"
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { btnCloseBarBg.color = theme.accent_hover}
                onExited:  { btnCloseBarBg.color = theme.accent }
                onClicked: { 
                    searchField.clear()
                    searchPanel.visible = false 
                }
            }
        }

    }

    DragHandler {
        enabled: fourteen.is_csd()
        grabPermissions: TapHandler.CanTakeOverFromAnything
        target: null
        onActiveChanged: if (active) { window.startSystemMove(); }
    }

    function doSearch() {
        fourteen.proceed_to_page({"page":"search"})
        searchField.clear()
        searchPanel.visible = false
    }
}