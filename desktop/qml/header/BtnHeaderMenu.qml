import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    property string label: "Header Menu Button"
    property string icon: "../imgs/bg_tile.png"
    property bool showBorder: false
    signal click
    signal hoverEnter
    signal hoverExit

    id: itemRoot
    height: dp(48)

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: theme.sand_light
    }

    Rectangle {
        visible: parent.showBorder
        width: dp(36)
        height: dp(36)
        x: dp(6)
        y: parent.height/2 - height/2
        color: theme.gray_light
    }

    Image {
        width: dp(32)
        height: dp(32)
        x: dp(8)
        y: parent.height/2 - height/2
        source: itemRoot.icon
        fillMode: Image.PreserveAspectCrop
        sourceSize: Qt.size(width, height)
    }

    Text {
        x: dp(50)
        width: parent.width - x
        height: parent.height
        text: itemRoot.label
        color: theme.gray_bold
        font.family: theme.inter_bold_semi.name
        font.pixelSize: dp(16)
        verticalAlignment: Text.AlignVCenter
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        propagateComposedEvents: true
        onClicked: { itemRoot.click() }
        onEntered: { 
            lbl_bg.color = theme.sand_bg 
            itemRoot.hoverEnter()
        }
        onExited:  { 
            lbl_bg.color = theme.sand_light 
            itemRoot.hoverExit()
        }
    }
}