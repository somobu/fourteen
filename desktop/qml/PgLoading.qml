import QtQuick 2.15
import QtQuick.Controls 2.15


Item {

    Text {
        anchors.centerIn: parent
        text: gettext("Loading")
        font.family: theme.inter_bold_extra.name
        font.pixelSize: dp(16)
        color: theme.gray_bold
    }

}