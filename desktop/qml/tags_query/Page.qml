import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import ".."
import "../feed"
import "../reusable"


ScrollViewFix {
    id: feed_root
    y: header.height
    width: parent.width
    height: parent.height - y
    contentHeight: feed_column.height

    Column {    // Main column
        id: feed_column
        x: dp(144)
        y: dp(16)
        width: feed_root.width - 2*x
        spacing: dp(16)

        Item { // Empty item (fixed-size spacer)
            width: parent.width
            height: 8*dp(16)
        }

        Text {
            height: dp(64)
            text: gettext("Tag query results: ") + fourteen.pagedata.tag.name
            font.family: theme.inter_bold_semi.name
            font.pixelSize: dp(24)
            verticalAlignment: Text.AlignVCenter
            color: theme.gray_bold
        }

        Repeater {
            model: fourteen.pagedata.posts
            visible: fourteen.pagedata.posts.length > 0

            Post {
                required property var modelData
                required property int index
                post_model: modelData
                width: parent.width
                scrollOffset: feed_root.contentItem.contentY
                onYChanged: {
                    // recalcOfs();
                    // feed_root.content_refs[index] = y;
                }
            }

        }

        Item { // Empty item (fixed-size spacer)
            width: parent.width
            height: 1*dp(16)
            visible: fourteen.pagedata.posts.length > 0
        }

        Text {
            height: dp(64)
            width: parent.width
            text: gettext("Thats all!")
            font.family: theme.inter_bold_semi.name
            font.pixelSize: dp(20)
            color: theme.gray_bold
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Item { // Empty item (fixed-size spacer)
            width: parent.width
            height: 8*dp(16)
            visible: fourteen.pagedata.posts.length > 0
        }

    }
}
