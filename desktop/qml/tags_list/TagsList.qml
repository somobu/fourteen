import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import ".."
import "../reusable"

Item {
    id: tags_popup_root

    Rectangle {
        id: lib_root
        anchors.centerIn: parent
        width: Math.min(parent.width * 0.95, dp(832))
        height: Math.min(parent.height * 0.95, dp(768))
        color: theme.sand_light

        MouseArea { // Block background dismiss area
            anchors.fill: parent
            cursorShape: Qt.ArrowCursor
            onClicked: { }
        }

        TagEntry {
            is_title: true
            height: dp(48)
            width: parent.width
        }

        BtnIcon {
            x: parent.width - width - dp(8)
            y: dp(8)
            label: gettext("Add new")
            backgroundColorNormal: theme.sand_light
            backgroundColorHover: theme.sand_light
            textColorNormal: theme.accent
            textColorHover: theme.accent_hover

            property var new_tag_count: 0

            onClick: {
                new_tag_count += 1
                var tag = {
                    "uuid":"new-tag-"+new_tag_count, // Actual value set by backend
                    "edited": true,
                    "active": true,
                    "edited_at": 0 , // Actual value set by backend
                    "feed_id": fourteen.pagedata.feed_id,
                    "edited_by": "", // Actual value set by backend
                    "name": "New Tag #"+new_tag_count,
                    "tag_type":"Regular",
                    "trigger":"regex",
                    "force_into_edit": true,
                };
                fourteen.pagedata.tags.unshift(tag);
                tags_repeater.model = fourteen.pagedata.tags;
                tags_repeater.modelChanged();
            }
        }

        Rectangle {
            y: dp(48)
            width: parent.width
            height: dp(2)
            color: theme.gray_bold
        }

        ScrollViewFix {
            y: dp(48)
            height: parent.height - y - dp(48)
            width: parent.width
            contentHeight: clmn.height
            clip: true

            Column {
                id: clmn
                width: parent.width

                Repeater {
                    id: tags_repeater
                    model: fourteen.pagedata.tags

                    TagEntry {
                        required property var modelData
                        required property int index
                        height: dp(48)
                        width: parent.width
                        tag_id: modelData["uuid"]
                        active: modelData["active"]
                        modified: modelData["edited"] || false
                        name: modelData["name"]
                        type: modelData["tag_type"]
                        trigger: modelData["trigger"]
                        force_into_edit_mode: modelData["force_into_edit"] || false
                    }

                }
            }

        }

        Rectangle {
            y: parent.height - dp(48)
            width: parent.width
            height: dp(2)
            color: theme.gray_bold
        }

        BtnIcon {
            x: parent.width - width - dp(8)
            y: parent.height - height/2 - dp(48)/2
            label: gettext("Apply")
            onClick: {
                for (var tag of fourteen.pagedata.tags) {
                    if (tag.edited || false) {
                        var new_tag = JSON.parse(fourteen.update_tag(JSON.stringify(tag)));
                        tag.uuid = new_tag.uuid;
                        tag.edited = false;
                        tag.force_into_edit = false;

                        tags_repeater.model = fourteen.pagedata.tags;
                        tags_repeater.modelChanged();
                    }
                }

                popup.hide();
            }
        }

    }


}