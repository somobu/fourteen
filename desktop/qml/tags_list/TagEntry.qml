import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import ".."
import "../reusable"

Item {

    property bool is_title: false
    property var tag_id: ""
    property var active: true
    property var modified: false
    property var type: "Type"
    property var name: "Name"
    property var trigger: "Trigger"
    property var force_into_edit_mode: false

    id: rt
    width: parent.width
    height: dp(48)

    property var edit_mode: force_into_edit_mode

    function onDataChanged() {
        var tags = fourteen.pagedata.tags;
        for (var tag of tags) {
            if (tag.uuid == tag_id) {
                tag.name = name;
                tag.trigger = trigger;
                tag.edited = true;
                tag.force_into_edit = false;
                return;
            }
        }
    }

    RowLayout { // Non-edit mode
        width: parent.width - dp(18)
        height: dp(48)
        spacing: dp(8)
        visible: !rt.edit_mode

        Item {
            width: dp(48)
            height: dp(48)
            implicitWidth: width
            Layout.preferredWidth: width
            Layout.fillWidth: false

            Rectangle {
                visible: !rt.is_title
                x: dp(12)
                y: dp(12)
                width: parent.width - 2*x
                height: parent.height - 2*y
                color: theme.gray_bold
            }
        }

        Text {
            width: dp(120)
            Layout.preferredWidth: width
            Layout.fillWidth: false
            text: rt.is_title ? gettext("Type") : rt.type
            font.family: (rt.is_title || rt.modified) ? theme.inter_bold_extra.name : theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
        }

        Text {
            width: dp(240)
            Layout.preferredWidth: width
            Layout.fillWidth: true
            text: rt.is_title ? gettext("Name") : rt.name
            font.family: (rt.is_title || rt.modified) ? theme.inter_bold_extra.name : theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
        }

        Text {
            width: dp(360)
            Layout.preferredWidth: width
            Layout.fillWidth: true
            text: rt.is_title ? gettext("Trigger") : rt.trigger
            font.family: (rt.is_title || rt.modified) ? theme.inter_bold_extra.name : theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
        }
    }

    BtnIconOnly {
        visible: !rt.is_title && !rt.edit_mode && dat_hover.hovered
        icon: "imgs/ic_edit.png"
        x: parent.width - 2 * width
        y: parent.height/2 - height/2
        onClick: { 
            rt.edit_mode = true
        }
    }

    RowLayout { // Edit mode
        width: parent.width - dp(18)
        height: dp(48)
        spacing: dp(8)
        visible: rt.edit_mode

        Item {
            width: dp(48)
            height: dp(48)
            implicitWidth: width
            Layout.preferredWidth: width
            Layout.fillWidth: false

            Rectangle {
                visible: !rt.is_title
                x: dp(12)
                y: dp(12)
                width: parent.width - 2*x
                height: parent.height - 2*y
                color: theme.gray_bold
            }
        }

        Text {
            width: dp(120)
            Layout.preferredWidth: width
            Layout.fillWidth: false
            text: rt.is_title ? gettext("Type") : rt.type
            font.family: theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
        }

        CustomTextField {
            id: edit_name
            width: dp(240)
            Layout.preferredWidth: width
            Layout.fillWidth: true
            text: rt.name
            fontSize: dp(16)
        }

        CustomTextField {
            id: edit_trigger
            width: dp(360)
            Layout.preferredWidth: width
            Layout.fillWidth: true
            text: rt.trigger
            fontSize: dp(16)
        }
    }

    BtnIconOnly { // Edit mode apply
        visible: rt.edit_mode && dat_hover.hovered
        icon: "imgs/accept_sand24.png"
        x: parent.width - 2*width - dp(32)
        y: parent.height/2 - height/2
        onClick: { 
            rt.modified = true
            rt.edit_mode = false
            rt.name = edit_name.getEditedText()
            rt.trigger = edit_trigger.getEditedText()
            rt.onDataChanged();
        }
    }

    BtnIconOnly { // Edit mode cancel
        visible: rt.edit_mode && dat_hover.hovered
        icon: "imgs/close_sand24.png"
        x: parent.width - 2 * width
        y: parent.height/2 - height/2
        onClick: { 
            rt.edit_mode = false
        }
    }

    HoverHandler {
        id: dat_hover
        acceptedButtons: Qt.NoButton
    }
}