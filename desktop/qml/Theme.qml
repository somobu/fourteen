import QtQuick 2.15

QtObject {
    id: theme

    property color transparent:         "#00FFFFFF"

    // Note: if you change this -- do not forget to replace colors in rust as well
    property color accent:              "#2AA198"
    property color accent_hover:        "#3bb08f"
    property color sand_light:          "#FDF6E3"
    property color sand_light_hover:    "#f2ecda"
    property color sand_bg:             "#EEE8D5"
    property color gray_bold:           "#586E75"
    property color gray_bold_hover:     "#6d8991"
    property color gray_light:          "#93A1A1"
    property color gray_light_hover:    "#788a8a"

    // Semi-transparent versions
    property color sand_light_semi:          "#80FDF6E3"
    property color sand_light_semi_hover:    "#80f2ecda"

    property var inter_thin:        FontLoader { source: "fonts/Inter-Thin.ttf" }
    property var inter_light:       FontLoader { source: "fonts/Inter-Light.ttf" }
    property var inter_medium:      FontLoader { source: "fonts/Inter-Medium.ttf" }
    property var inter_bold_semi:   FontLoader { source: "fonts/Inter-SemiBold.ttf" }
    property var inter_bold:        FontLoader { source: "fonts/Inter-Bold.ttf" }
    property var inter_bold_extra:  FontLoader { source: "fonts/Inter-ExtraBold.ttf" }
}