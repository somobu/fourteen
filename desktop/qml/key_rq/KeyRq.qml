import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import ".."
import "../reusable/"

Item {

    Item {
        id: publish
        width: 21 * dp(16)
        height: pub_col.height + 2*dp(16)
        anchors.centerIn: parent

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        ColumnLayout {
            id: pub_col
            width: parent.width - 2*dp(16)
            x: dp(16)
            y: dp(16)
            spacing: dp(8)

            Title {
                text: gettext("Enter the key")
                icon: "feed/ic_info.png"
                width: parent.width
            }

            Item { height: dp(8) }

            Item {  // Post name
                height: dp(32)
                width: parent.width
                clip: true

                Rectangle { // Frame
                    width: parent.width
                    height: parent.height
                    color: theme.sand_light
                    border.color: theme.gray_light
                    border.width: dp(1)
                }

                TextInput {
                    id: post_title
                    width: parent.width - dp(12)
                    anchors.centerIn: parent
                    font.family: theme.inter_bold_semi.name
                    font.pixelSize: dp(14)
                    color: theme.gray_bold
                    Keys.onReturnPressed: btn_accept.handleClick()
                    Keys.onEnterPressed: btn_accept.handleClick()
                }

            }

            Text {
                id: error_msg
                width: parent.width
                visible: false
                text: gettext("Invalid key")
                font.family: theme.inter_bold_extra.name
                font.pixelSize: dp(14)
                color: theme.gray_bold
                horizontalAlignment: Text.AlignRight
            }

            Item { height: dp(8) }

            Item {
                height: dp(32)
                width: parent.width

                BtnIcon {
                    id: btn_accept
                    x: parent.width - width
                    label: gettext("Confirm")
                    onClick: handleClick()

                    // Made as function cuz triggered by Enter key
                    function handleClick() {
                        let rz = fourteen.add_key(fourteen.pagedata.proceed_to.feed, post_title.text);
                        if (rz) {
                            fourteen.proceed_to_page(fourteen.pagedata.proceed_to);
                        } else {
                            error_msg.visible = true;
                        }
                    }
                }
            }

        }
    }

}