import QtQuick 2.15

Item {  // Header
    property string text: ""
    property string icon: ""
    id: header
    width: dp(240)
    height: dp(24)
    clip: true

    Behavior on height {
        PropertyAnimation { 
            duration: 75 
            easing.type: Easing.InOutQuad
        }
    }

    Image {
        width: dp(24)
        height: dp(24)
        x: dp(4)
        y: dp(4)
        source: header.icon
    }

    Text {
        x: dp(40)
        y: dp(4)
        width: parent.width - x
        height: dp(24)
        text: header.text
        font.family: theme.inter_medium.name
        font.pixelSize: dp(14)
        verticalAlignment: Text.AlignVCenter
        color: theme.gray_bold
        elide: Text.ElideRight
    }
}