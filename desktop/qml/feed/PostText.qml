import QtQuick 2.15

Text {
    property string body: "[ERR: NO POST TEXT]"

    id: post_text
    text: body
    width: post_body.width
    wrapMode: Text.Wrap
    color: theme.gray_bold
    textFormat: TextEdit.MarkdownText
    font.family: theme.inter_medium.name
    font.pixelSize: dp(16)
    lineHeight: 1.5
    onLinkActivated: (link)=> {
        if (link.startsWith("fourteen://")) {
            var url = link.replace("fourteen://", "").split("/");
            var feed_id = url[0];

            if (url.length == 1) {
                console.log("TODO: proceed to feed");
                return;
            }

            var mode = url[1];

            if (mode === "tag") {
                var tag_id = url[2];
                fourteen.proceed_to_page({
                    "page": "tag_query", 
                    "feed": feed_id,
                    "tag": tag_id,
                });
                return;
            }

            console.log("Unknown url " + url);
        } else {
            Qt.openUrlExternally(link)
        }
    }

    MouseArea {
        width: parent.width
        height: parent.height
        acceptedButtons: Qt.NoButton
        cursorShape: post_text.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
    }
}
