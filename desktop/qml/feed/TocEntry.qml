import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property var title: ""
    property var level: 1
    property var isCurrent: false

    property bool hovered: false

    property var default_color: (rt.level < 3 ? theme.gray_bold : theme.accent)
    property var hover_color: (rt.level < 3 ? theme.gray_bold_hover : theme.accent_hover)

    signal click

    id: rt
    height: dp(16)

    Rectangle { // Hover bg
        y: -2
        width: parent.width
        height: parent.height + dp(8)
        visible: rt.hovered
        color: theme.sand_light_hover
    }

    Rectangle { // Current post indicator
        width: dp(6)
        x: -dp(2)
        height: parent.height - 2*x
        visible: rt.isCurrent
        color: theme.gray_bold
    }

    Text {
        id: toc_body
        x: (rt.level - 1) * dp(16)
        width: parent.width - x
        text: rt.title
        color: rt.hovered ? rt.hover_color : rt.default_color
        font.family: theme.inter_bold_semi.name
        font.pixelSize: dp(16)
        elide: Text.ElideRight
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { rt.hovered = true }
        onExited:  { rt.hovered = false }
        onClicked: { rt.click() }
    }

}
