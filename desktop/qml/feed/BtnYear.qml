import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property string label
    property bool expandMonths: false
    property var months: [ 
        gettext("jan"), gettext("feb"), gettext("mar"), gettext("apr"), 
        gettext("may"), gettext("jun"), gettext("jul"), gettext("aug"), 
        gettext("sep"), gettext("oct"), gettext("nov"), gettext("dec") 
    ]
    property var month_names: [
        gettext("nul"),
        gettext("jan"), gettext("feb"), gettext("mar"), gettext("apr"), 
        gettext("may"), gettext("jun"), gettext("jul"), gettext("aug"), 
        gettext("sep"), gettext("oct"), gettext("nov"), gettext("dec")
    ]
    property var year: "1990"
    signal click(i: var)

    id: itemRoot
    height: dp(32)
    width: row.width

    function set_expanded(expanded) {
        expandMonths = expanded;
        lbl_bg.color = itemRoot.expandMonths ? theme.accent : theme.gray_bold;
    }

    Row {
        id: row

        Item {
            height: dp(32)
            width: lbl.width + 16

            Rectangle {
                id: lbl_bg
                width: parent.width
                height: parent.height
                color: expandMonths ? theme.accent : theme.gray_bold
            }

            Text {
                id: lbl
                x: dp(8)
                height: parent.height
                text: label
                color: theme.sand_light
                font.family: theme.inter_bold_extra.name
                font.pixelSize: dp(16)
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onEntered: { lbl_bg.color = itemRoot.expandMonths ? theme.accent_hover : theme.gray_bold_hover }
                onExited: { lbl_bg.color = itemRoot.expandMonths ? theme.accent : theme.gray_bold }
                onClicked: { itemRoot.click(itemRoot) }
            }
        }

        Repeater {
            model: itemRoot.expandMonths ? itemRoot.months : []

            BtnMonth {
                required property string modelData
                required property int index
                label: month_names[modelData]
                year: itemRoot.year
                month: modelData
                onClick: {
                    fourteen.proceed_to_page({
                        "page": "feed", 
                        "feed": fourteen.pagedata.feed_id, 
                        "year": (itemRoot.label-0),
                        "month": (modelData-0)
                    })
                }
            }
        }

    }
}
