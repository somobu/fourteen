import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "../reusable"

Item {
    id: head_root
    width: parent.width
    height: head_column.height + dp(32)

    property bool empty: false
    property bool show_months_ribbon: false

    signal editRequested()

    Rectangle { // Shadow
        x: dp(4)
        y: dp(4)
        width: parent.width
        height: parent.height
        color: theme.gray_light
    }

    Rectangle { // Background
        width: parent.width
        height: parent.height
        color: theme.sand_light
    }

    ColumnLayout {
        id: head_column
        x: dp(16)
        y: dp(16)
        width: parent.width - 2*x
        spacing: dp(16)

        Image { // Header image
            id: head_img
            visible: fourteen.pagedata.header_imgs.length > 0
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: parent.width * 0.192
            fillMode: Image.PreserveAspectCrop
            source: visible ? get_header_img() : ""

            function get_header_img() {
                var len = fourteen.pagedata.header_imgs.length;
                var idx = Math.floor(Math.random() * len);
                return "image://fourteen/" + fourteen.pagedata.header_imgs[idx];
            }
        }

        Text {
            text: getTitle()
            font.family: theme.inter_bold_extra.name
            font.pixelSize: dp(22)
            color: theme.gray_bold

            function getTitle() {
                if (fourteen.pagedata.title.trim() === "") {
                    return gettext("Untitled")
                } else {
                    return fourteen.pagedata.title
                }
            }
        }

        Text {
            text: fourteen.pagedata.descr
            font.family: theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_light
            textFormat: TextEdit.MarkdownText
            visible: text.trim() !== ""
        }

        Item {
            height: dp(32)
            width: parent.width
            visible: !head_root.empty && head_root.show_months_ribbon

            ScrollView {
                id: months_scroll
                width: feed_column.width - 2*dp(16)
                height: dp(32)
                contentHeight: dp(32)
                clip: true
                Layout.fillWidth: true
                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOff

                Row {
                    height: dp(32)
                    spacing: dp(12)
                    id: me_dat_row

                    function show_item(item)
                    {
                        var was_active = item.expandMonths;
                        for (var i = 0; i < dat_som_rep.count; i++) {
                            dat_som_rep.itemAt(i).set_expanded(false);
                        }
                        item.expandMonths = !was_active;
                    }

                    Repeater {
                        model: fourteen.pagedata.years
                        id: dat_som_rep

                        BtnYear {
                            required property int index
                            required property string modelData
                            label: modelData
                            months: fourteen.pagedata.months_ids[modelData]
                            expandMonths: fourteen.pagedata.year == modelData
                            year: modelData
                            onClick: (i) => { me_dat_row.show_item(i) }
                        }
                    }

                }
            }

            MouseArea {
                anchors.fill: months_scroll

                onWheel: {
                    // Inner layout of years scroller > scroll container
                    if (me_dat_row.width > months_scroll.contentItem.width) {
                        months_scroll.contentItem.contentX -= wheel.angleDelta.y;

                        if (months_scroll.contentItem.contentX + months_scroll.contentItem.width > me_dat_row.width) {
                            months_scroll.contentItem.contentX = me_dat_row.width -  months_scroll.contentItem.width;
                        }

                        if (months_scroll.contentItem.contentX < 0) {
                            months_scroll.contentItem.contentX = 0;
                        }
                    } else {
                        // Pass event to parent scrollview
                        feed_root.onWheel(wheel);
                    }
                }
                onClicked: mouse.accepted = false;
                onPressed: mouse.accepted = false;
                onReleased: mouse.accepted = false;
                onDoubleClicked: mouse.accepted = false;
                onPositionChanged: mouse.accepted = false;
                onPressAndHold: mouse.accepted = false;
            }

        }

    }

    BtnIconOnly {
        id: btn_edit
        width: dp(32)
        height: dp(32)
        x: parent.width - dp(48)
        y: parent.height - (head_root.empty ? 3*dp(16) : 3*dp(16))
        icon: "feed/ic_pencil.png"
        visible: root_hover.hovered
        onClick: { head_root.editRequested() }
    }

    HoverHandler {
        id: root_hover
        acceptedButtons: Qt.NoButton // Don't eat the mouse clicks
    }

}
