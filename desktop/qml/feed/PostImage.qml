import QtQuick 2.15

Item {
    property string src: ""
    property string descr: ""

    id: pbody
    height: descr === "" ? img.height : img.height + 32

    Image {
        id: img
        x: dp(16)
        width: Math.min(pbody.width - dp(32), sourceSize.width)
        height: sourceSize.height * (width / sourceSize.width)
        asynchronous: true
        source: (pbody.src == "" ? null : "image://fourteen/" + pbody.src)
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        y: img.height + 12
        text: pbody.descr
        width: pbody.width
        wrapMode: Text.Wrap
        color: theme.gray_light
        horizontalAlignment: Text.AlignHCenter
        font.family: theme.inter_medium.name
        font.pixelSize: dp(16)
        lineHeight: 1.5
    }

}
