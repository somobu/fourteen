import QtQuick 2.15

import "../reusable"

Item {
    property var items

    id: pbody
    height: img.height + dp(32)

    property var currentItemIdx: 0;

    function setItem(idx) {
        if (idx < 0) {
            idx = items.length + idx
        }

        currentItemIdx = idx % items.length;
        img.source = "image://fourteen/" + items[currentItemIdx].src;
        desc.text = "[" + (currentItemIdx+1)+"/" + items.length + "]";

        var descr_text = items[currentItemIdx].descr;
        if (descr_text !== "") {
            desc.text += " " + descr_text;
        }
    }

    Image {
        id: img
        x: dp(16)
        width: Math.min(pbody.width - dp(32), sourceSize.width)
        height: sourceSize.height * (width / sourceSize.width)
        asynchronous: false
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: desc
        y: img.height + dp(12)
        width: pbody.width
        wrapMode: Text.Wrap
        color: theme.gray_light
        horizontalAlignment: Text.AlignHCenter
        font.family: theme.inter_medium.name
        font.pixelSize: dp(16)
        lineHeight: 1.5
    }


    BtnIcon {
        width: dp(32)
        height: dp(32)
        x: dp(24)
        y: parent.height/2 - height
        visible: root_hover.hovered
        icon: "imgs/ic_arrow_left.png"
        backgroundColorNormal: theme.sand_light_semi
        backgroundColorHover: theme.sand_light_semi
        onClick: { 
            setItem(currentItemIdx - 1)
        }
    }

    BtnIcon {
        width: dp(32)
        height: dp(32)
        x: parent.width - dp(56)
        y: parent.height/2 - height
        visible: root_hover.hovered
        icon: "imgs/ic_arrow_right.png"
        backgroundColorNormal: theme.sand_light_semi
        backgroundColorHover: theme.sand_light_semi
        onClick: { 
            setItem(currentItemIdx + 1)
        }
    }


    HoverHandler {
        id: root_hover
        acceptedButtons: Qt.NoButton
    }
}
