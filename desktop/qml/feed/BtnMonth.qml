import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property string label
    property var year: "1990"
    property var month: "0"
    signal click

    id: itemRoot
    height: dp(32)
    width: lbl.width + dp(16)

    function is_selected() {
        return fourteen.pagedata.year == year && fourteen.pagedata.month == month;
    }

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: is_selected() ? theme.gray_bold : theme.gray_light
    }

    Text {
        id: lbl
        x: dp(8) 
        height: parent.height
        text: label
        color: theme.sand_light
        font.family: theme.inter_bold_extra.name
        font.pixelSize: dp(16)
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { lbl_bg.color = is_selected() ? theme.gray_bold_hover : theme.gray_light_hover }
        onExited:  { lbl_bg.color = is_selected() ? theme.gray_bold : theme.gray_light }
        onClicked: { itemRoot.click() }
    }
}
