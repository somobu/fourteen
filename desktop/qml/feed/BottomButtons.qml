import QtQuick 2.15
import QtQuick.Layouts 1.15

import ".."

Item {
    id: btns_root
    height: dp(48)
    property bool leftVisible: true
    property bool rightVisible: true
    signal clickLeft
    signal clickUp
    signal clickRight

    RowLayout {
        width: parent.width - dp(272)
        height: parent.height
        x: dp(272)

        Item {
            height: btns_root.height
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: btns_root.leftVisible

            Rectangle {
                id: bg_left
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Text {
                text: gettext("‹  Prev")
                width: parent.width
                height: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: theme.accent
                font.family: theme.inter_bold_semi.name
                font.pixelSize: dp(16)
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                propagateComposedEvents: true
                onClicked: { btns_root.clickLeft() }
                onEntered: { bg_left.color = theme.sand_light_hover }
                onExited:  { bg_left.color = theme.sand_light }
            }
        }

        Item {
            height: btns_root.height
            Layout.fillHeight: true
            Layout.fillWidth: true

            Rectangle {
                id: bg_center
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Text {
                text: gettext("Up")
                width: parent.width
                height: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: theme.accent
                font.family: theme.inter_bold_semi.name
                font.pixelSize: dp(16)
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                propagateComposedEvents: true
                onClicked: { btns_root.clickUp() }
                onEntered: { bg_center.color = theme.sand_light_hover }
                onExited:  { bg_center.color = theme.sand_light }
            }
        }

        Item {
            height: btns_root.height
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: btns_root.rightVisible

            Rectangle {
                id: bg_right
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Text {
                text: gettext("Next  ›")
                width: parent.width
                height: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: theme.accent
                font.family: theme.inter_bold_semi.name
                font.pixelSize: dp(16)
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                propagateComposedEvents: true
                onClicked: { btns_root.clickRight() }
                onEntered: { bg_right.color = theme.sand_light_hover }
                onExited:  { bg_right.color = theme.sand_light }
            }
        }

    }


}