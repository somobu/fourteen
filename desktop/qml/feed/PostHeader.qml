import QtQuick 2.15

Item {
    property var image: ""
    property var title

    id: pbody
    height: img.height

    Image {
        id: img
        x: -2*dp(16)
        width: pbody.width + 4*dp(16)
        height: width * 0.48
        source: pbody.image === "" ? "" : pbody.image
        asynchronous: true
        fillMode: Image.PreserveAspectCrop
    }

    Item {
        width: lbl.width + 2*dp(24)
        height: lbl.height + 2*dp(16)
        x: (pbody.width - width) / 2
        y: pbody.height - height - dp(24)

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        Text {
            id: lbl
            x: dp(24)
            y: dp(16)
            property string body: pbody.title
            text: body
            wrapMode: Text.Wrap
            color: theme.gray_bold
            font.family: theme.inter_light.name
            font.pixelSize: dp(24)
        }

    }

}
