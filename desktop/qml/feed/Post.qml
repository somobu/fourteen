import QtQuick 2.15

import ".."
import "../reusable"

Item {
    property var post_model

    id: post_root
    width: parent.width
    height: post_body.height + (post_root.post_model.featured ? 2*dp(16) : 3*dp(16))
    z: 0 + (root_hover.hovered ? 10 : 0)

    property double scrollOffset: 0.0

    // Internal props
    property bool hovered: root_hover.hovered

    onScrollOffsetChanged: { recalcOfs() }

    function ptitle(post) {
        if (post.title.trim() === "") {
            return bugs.getFormattedShortDate(post.date)
        } else {
            return post.title
        }
    }

    function recalcOfs() {
        var max_offset = height - post_info_bg.height

        var resulting_offset = scrollOffset - y;
        if (resulting_offset > max_offset) resulting_offset = max_offset;
        if (resulting_offset < 0) resulting_offset = 0; // Order matters

        post_info.offset = resulting_offset;
    }

    HoverHandler {
        id: root_hover
        acceptedButtons: Qt.NoButton
    }

    Rectangle { // Post info shadow
        width: post_info_bg.width
        height: post_info_bg.height
        color: theme.gray_light
        x: dp(4)
        y: dp(4) + post_info.offset
    }

    Rectangle { // Post text shadow
        id: post_text_shadow
        width: parent.width - dp(272)
        height: parent.height
        color: theme.gray_light
        x: dp(4) + dp(272)
        y: dp(4)
    }

    Rectangle {
        id: post_info_bg
        width: dp(272)
        y: post_info.offset
        height: post_info.height + 2*dp(16)
        color: theme.sand_light
    }

    Item {  // Post info
        property bool expanded: root_hover.hovered
        property var offset: 0

        id: post_info
        width: dp(272) - 2*x
        // height: is_partial() && !expanded ? parent.height - 2*dp(16) : post_info_clmn.height;
        height: post_info_clmn.height;
        x: dp(16)
        y: dp(16) + offset

        function is_partial() {
            return post_root.height < 10*dp(16);
        }

        Column {
            id: post_info_clmn
            width: parent.width
            spacing: dp(8)

            Title {
                text: ptitle(post_root.post_model)
                icon: post_info.expanded || !post_info.is_partial() ? "feed/ic_info.png" : "feed/ic_down.png"
            }

            PostInfoEntry { // Visible 2nd
                height: (post_info.expanded || post_root.height >= 8*dp(16)) ? dp(25) : 0;
                text: post_root.post_model.address_str
                icon: "ic_map.png"
            }

            PostInfoEntry { // Visible first
                height: (post_info.expanded || post_root.height >= 6*dp(16)) ? dp(25) : 0;
                text: bugs.getFormattedDate(post_root.post_model.date)
                icon: "ic_calendar.png"
            }

            PostInfoEntry { // Visible last
                height: (post_info.expanded || post_root.height >= 10*dp(16)) ? dp(25) : 0
                text: post_root.post_model.author_str
                icon: "ic_user.png"
            }

        }
    }

    Rectangle { // Post text bg
        width: parent.width - x
        height: parent.height
        x: dp(272)
        color: theme.sand_light
    }


    Column {  // Post text column
        property var is_featured: post_root.post_model.featured
        id: post_body
        width: post_root.width - dp(272) - 4*dp(16)
        x: dp(272) + 2*dp(16)
        y: (is_featured ? 0 : 1.5*dp(16))
        spacing: dp(16)

        PostHeader {
            width: post_body.width
            visible: post_body.is_featured
            image: post_body.is_featured ? "image://fourteen/" + post_root.post_model.featured_img : ""
            title: ptitle(post_root.post_model)
        }

        Repeater {
            model: post_root.post_model.para

            Loader {
                id: body_loader
                required property var modelData
                required property int index
                width: post_body.width
                source: getViewByType(modelData.t)
                asynchronous: true

                function getViewByType(t) {
                    if (t === "txt"){
                        return "PostText.qml";
                    } else if (t === "img") {
                        return "PostImage.qml";
                    } else if (t === "gal") {
                        return "PostGallery.qml";
                    } else {
                        return "PostText.qml";
                    }
                }

                onLoaded: {
                    var t = modelData.t;
                    if (t === "txt") {
                        body_loader.item.body = modelData.txt;
                    } else if (t === "img") {
                        body_loader.item.src = modelData.src;
                        body_loader.item.descr = modelData.descr;
                    } else if (t === "gal") {
                        body_loader.item.items = modelData.data;
                        body_loader.item.setItem(0)
                    } else {
                        body_loader.item.body = "```Unknown payload:\n"
                            + JSON.stringify(modelData, null, 2)
                            + "\n```";
                    }
                }

                
            }
        }

    }

    BtnIconOnly {
        id: btn_edit
        width: dp(32)
        height: dp(32)
        x: parent.width - dp(48)
        y: dp(16)
        visible: root_hover.hovered
        icon: "feed/ic_pencil.png"
        onClick: { 
            fourteen.proceed_to_page({
                "page": "editor", 
                "feed": fourteen.pagedata.feed_id,
                "post": post_root.post_model.id
            }) 
        }
    }
}