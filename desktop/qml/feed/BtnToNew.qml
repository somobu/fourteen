import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    signal clicked

    id: itemRoot
    height: dp(32)
    width: lbl.width + dp(16)

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: theme.sand_light
    }

    Row {
        id: lbl
        height: dp(32)
        x: dp(8)
        spacing: dp(8)

        Text {
            height: parent.height
            text: gettext("To new")
            color: theme.accent
            font.family: theme.inter_bold_extra.name
            font.pixelSize: dp(16)
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            width: dp(16)
            height: parent.height
            source: "arrow_downward.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { lbl_bg.color = theme.sand_bg; }
        onExited:  { lbl_bg.color = theme.sand_light; }
        onClicked: { itemRoot.clicked(); }
    }
}
