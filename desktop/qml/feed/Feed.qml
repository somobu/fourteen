import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import ".."
import "../reusable"

ScrollViewFix {
    id: feed_root
    y: header.height
    width: parent.width
    height: parent.height - y
    contentHeight: feed_column.height

    property var content_refs: []

    function ptitle(post) {
        if (post.title.trim() === "") {
            return bugs.getFormattedShortDate(post.date)
        } else {
            return post.title
        }
    }

    Binding {
        target: feed_root.contentItem
        property: "boundsBehavior"
        value: Flickable.StopAtBounds
    }

    Column {    // Main column
        id: feed_column
        x: dp(16)
        y: dp(16)
        width: feed_root.width - 2*dp(16)
        spacing: dp(16)

        function hasSidebar() {
            return feed_root.width > dp(1300)
        }

        FeedHeader {
            id: head_root
            width: parent.width
            empty: fourteen.pagedata.posts.length == 0
            show_months_ribbon: !feed_column.hasSidebar()

            onEditRequested: {
                popup.visible = true;
                feed_root.contentItem.contentY = 0;
            }
        }

        Item { // Empty item (fixed-size spacer)
            width: parent.width
            height: dp(16)
        }

        Item { // Month divider
            id: month_div
            width: (parent.width - (parent.hasSidebar() ? dp(288) : 0))
            height: 3*dp(16)
            visible: fourteen.pagedata.posts.length > 0

            Rectangle {
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            RowLayout {
                width: parent.width
                height: parent.height
                anchors.fill: parent

                Item {
                    Layout.fillWidth: true
                }

                Rectangle {
                    implicitWidth: month_div.width / 3
                    height: dp(2)
                    color: theme.gray_light
                    Layout.alignment: Qt.AlignVCenter
                }

                Item {
                    Layout.fillWidth: true
                }

                Text {
                    text: bugs.getFormattedMonth(fourteen.pagedata.date)
                    font.family: theme.inter_bold_extra.name
                    font.pixelSize: dp(18)
                    color: theme.gray_bold
                    Layout.alignment: Qt.AlignVCenter
                }

                Item {
                    Layout.fillWidth: true
                }

                Rectangle {
                    implicitWidth: month_div.width / 3
                    height: dp(2)
                    color: theme.gray_light
                    Layout.alignment: Qt.AlignVCenter
                }

                Item {
                    Layout.fillWidth: true
                }

            }
        }

        Repeater {
            model: fourteen.pagedata.posts
            visible: fourteen.pagedata.posts.length > 0

            Post {
                required property var modelData
                required property int index
                post_model: modelData
                width: (parent.width - (parent.hasSidebar() ? dp(288) : 0))
                scrollOffset: feed_root.contentItem.contentY
                onYChanged: { 
                    recalcOfs();
                    feed_root.content_refs[index] = y; 
                }
            }

        }

        Item { // Empty item (fixed-size spacer)
            width: parent.width
            height: dp(8)
            visible: fourteen.pagedata.posts.length > 0
        }

        BottomButtons {
            width: parent.width - (parent.hasSidebar() ? dp(288) : 0)
            height: dp(48)
            visible: fourteen.pagedata.posts.length > 0
            leftVisible: get_prev() != null
            rightVisible: get_next() != null
            onClickLeft: { fourteen.proceed_to_page(get_prev()) }
            onClickUp: { feed_root.ScrollBar.vertical.position = 0 }
            onClickRight: { fourteen.proceed_to_page(get_next()) }

            function get_prev() {
                var d = fourteen.pagedata;
                var feed_id = d.feed_id;

                if (!d.months_ids[d.year]) return null;

                var current_year_idx = d.years.indexOf(""+d.year);
                var current_month_idx = d.months_ids[d.year].indexOf(""+d.month);

                if (current_month_idx > 0) {
                    return {
                        "page": "feed", 
                        "feed": fourteen.pagedata.feed_id, 
                        "year": (d.year-0),
                        "month": (d.months_ids[d.year][current_month_idx-1]-0)
                    };
                } else {
                    if (current_year_idx > 0) {
                        var year = d.years[current_year_idx-1];
                        var month_list = d.months_ids[year];
                        var month = month_list[month_list.length - 1];

                        return {
                            "page": "feed", 
                            "feed": fourteen.pagedata.feed_id, 
                            "year": (year-0),
                            "month": (month-0)
                        };
                    } else {
                        return null;
                    }
                }
            }

            function get_next() {
                var d = fourteen.pagedata;
                var feed_id = d.feed_id;

                if(!d.months_ids[d.year]) return null;

                var current_year_idx = d.years.indexOf(""+d.year);
                var current_month_idx = d.months_ids[d.year].indexOf(""+d.month);

                if (current_month_idx + 1 < d.months_ids[d.year].length) {
                    var month = d.months_ids[d.year][current_month_idx+1];
                    return {
                        "page": "feed", 
                        "feed": fourteen.pagedata.feed_id, 
                        "year": (d.year-0),
                        "month": (month-0)
                    };
                } else {
                    if (current_year_idx + 1 < d.years.length) {
                        var year = d.years[current_year_idx+1];
                        var month = d.months_ids[year][0];
                        return {
                            "page": "feed", 
                            "feed": fourteen.pagedata.feed_id, 
                            "year": (year-0),
                            "month": (month-0)
                        };
                    } else {
                        return null;
                    }
                }
            }
        }

        Item {
            width: parent.width
            height: dp(320)
            visible: fourteen.pagedata.posts.length == 0

            Text {
                anchors.centerIn: parent
                text: gettext("This feed has no posts (yet)")
                font.family: theme.inter_bold.name
                font.pixelSize: dp(16)
                color: theme.gray_bold
            }
        }


        Item { // Empty item (fixed-size spacer)
            width: parent.width
            height: 2 * dp(48)
        }

    }

    Item {
        id: side_panel
        width: dp(272)
        height: scroll_view.height + 4*dp(16)
        x: feed_column.x + feed_column.width - width
        y: calcY()
        visible: feed_column.hasSidebar() && fourteen.pagedata.posts.length > 0
        
        property int activePostIndex: 0

        function calcY() {
            // Little side-effect
            activePostIndex = calcCurrentPost();
            app_background.bg_offset = feed_root.contentItem.contentY / 3;

            var minY = head_root.height + 4*dp(16);
            var targetY = feed_root.contentItem.contentY + 1*dp(16)
            return Math.max(minY, targetY)
        }

        function calcCurrentPost() {
            var headerBlockHeight = head_root.height + 4*dp(16);
            var currentScroll = feed_root.contentItem.contentY// + headerBlockHeight;

            var keys = Object.keys(feed_root.content_refs);
            keys.sort();

            for (var k in keys) {
                var itemStart = feed_root.content_refs[k];

                if (itemStart > currentScroll) {
                    // Return perious post index, but no less than zero
                    return k - 1 < 0 ? 0 : k - 1;
                }
            }
            
            // All posts starts above scroll, return last post id
            return Math.max(0, keys.length - 1);
        }

        Rectangle {
            x: dp(4)
            y: dp(4)
            width: parent.width
            height: parent.height
            color: theme.gray_light
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        Title {
            x: dp(16)
            y: dp(16)
            text: gettext("TOC")
            icon: "feed/ic_toc.png"
        }

        ScrollViewFix {    
            id: scroll_view
            y: 3 * dp(16)
            width: side_panel.width
            height: Math.min(side_column.height + dp(22), window.height - 9*dp(16))
            contentHeight: side_column.height + dp(22)
            clip: true
            ScrollBar.vertical.policy: ScrollBar.AlwaysOff

            Column {
                id: side_column
                x: dp(16)
                y: dp(16)
                width: parent.width - 2*x
                spacing: dp(8)

                Repeater {
                    model: buildTOC()

                    function buildTOC() {

                            var rz = [];

                            for (var year of fourteen.pagedata.years) {
                                rz.push({
                                    "title": year, 
                                    "level": 1,
                                    "page": {
                                        "page": "feed", 
                                        "feed": fourteen.pagedata.feed_id, 
                                        "year": (year-0),
                                        "month": fourteen.pagedata.months_ids[year][0] - 0
                                    }
                                })

                                if (year == fourteen.pagedata.year) {
                                    for (var month of fourteen.pagedata.months_ids[year]) {
                                        rz.push({
                                            "title": bugs.getMonthName(month - 1), 
                                            "level": 2,
                                            "page": {
                                                "page": "feed", 
                                                "feed": fourteen.pagedata.feed_id, 
                                                "year": (year-0),
                                                "month": (month-0)
                                            }
                                        })

                                        if (month == fourteen.pagedata.month) {
                                            var i = 0;
                                            for (var post of fourteen.pagedata.posts) {
                                                rz.push({
                                                    "title": ptitle(post),
                                                    "id": post.id,
                                                    "index": i,
                                                    "level": 3
                                                })

                                                i += 1
                                            }
                                        }
                                    }
                                }
                            }

                            return rz
                        }

                    TocEntry {
                        required property var modelData
                        required property int index

                        title: modelData["title"]
                        level: modelData["level"]
                        isCurrent: level == 3 && side_panel.activePostIndex == modelData["index"]

                        width: side_column.width

                        onClick: { 
                            if (level != 3) {
                                fourteen.proceed_to_page(modelData["page"]);
                            } else {
                                var ofsY = feed_root.content_refs[modelData["index"]];
                                feed_root.targetScrollY = ofsY;
                            }
                        }
                    }

                }

            }

        }


    }

    Item {  // Feed editor popup
        id: popup
        y: -dp(12)
        width: parent.width
        height: parent.height + y
        visible: false

        Background {
            with_solid_color: false
            bg_offset: app_background.bg_offset
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onClicked: { 
                popup.visible = false; 
            }
            onWheel: {  }
        }

        Item {
            x: (feed_root.width - width) / 2
            y: (feed_root.height - height) / 2
            width: dp(480)
            height: feed_edit_column.height + 2*dp(16)

            MouseArea { // Block background dismiss area
                anchors.fill: parent
                cursorShape: Qt.ArrowCursor
                onClicked: { }
            }

            Rectangle {
                x: dp(4)
                y: dp(4)
                width: parent.width
                height: parent.height
                color: theme.gray_light
            }

            Rectangle {
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Column {
                id: feed_edit_column
                x: dp(16)
                y: dp(16)
                width: parent.width - x*dp(16)
                spacing: dp(16)

                Text {
                    height: 28
                    text: gettext("Title")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(22)
                    color: theme.gray_bold
                }

                Item {  // Feed title
                    height: dp(32)
                    width: dp(480) - 2*dp(16)
                    clip: true

                    Rectangle { // Frame
                        width: parent.width
                        height: parent.height
                        color: theme.sand_light
                        border.color: theme.gray_light
                        border.width: dp(1)
                    }

                    TextInput {
                        id: edit_feed_title
                        width: parent.width - dp(12)
                        anchors.centerIn: parent
                        text: fourteen.pagedata.title
                        font.family: theme.inter_bold_semi.name
                        font.pixelSize: dp(14)
                        color: theme.gray_bold
                    }

                }

                Text {
                    height: dp(28)
                    text: gettext("Description")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(22)
                    color: theme.gray_bold
                }

                Item {  // Feed title
                    height: dp(32)
                    width: dp(480) - 2*dp(16)
                    clip: true

                    Rectangle { // Frame
                        width: parent.width
                        height: parent.height
                        color: theme.sand_light
                        border.color: theme.gray_light
                        border.width: dp(1)
                    }

                    TextInput {
                        id: edit_feed_descr
                        width: parent.width - dp(12)
                        anchors.centerIn: parent
                        text: fourteen.pagedata.descr
                        font.family: theme.inter_bold_semi.name
                        font.pixelSize: dp(14)
                        color: theme.gray_bold
                    }

                }

                Text {
                    height: dp(28)
                    text: gettext("Feed UUID")
                    font.family: theme.inter_bold.name
                    font.pixelSize: dp(22)
                    color: theme.gray_bold
                }

                TextInput {
                    height: dp(14)
                    width: dp(480) - 2*dp(16) - dp(12)
                    text: fourteen.pagedata.feed_id
                    font.family: theme.inter_bold_semi.name
                    font.pixelSize: dp(14)
                    color: theme.gray_bold
                    verticalAlignment: Text.AlignVCenter
                    readOnly: true
                    selectByMouse: true
                    selectionColor: theme.accent
                }

                Item {  // Confirm btn
                    height: dp(32)
                    width: dp(480) - 2*dp(16)

                    BtnIcon {
                        id: server_popup_btn_delete
                        label: gettext("Delete")
                        backgroundColorNormal: theme.sand_light
                        backgroundColorHover: theme.sand_light
                        textColorNormal: theme.accent
                        textColorHover: theme.accent_hover
                        onClick: {
                            fourteen.update_feed(
                                fourteen.pagedata.feed_id,
                                "Deleted feed",
                                "Deleted feed",
                                "Deleted"
                            )

                            fourteen.proceed_to_page({ "page": "greeter" })
                        }
                    }

                    BtnIcon {
                        x: parent.width - width
                        label: gettext("Confirm")
                        textStyle: theme.inter_bold_semi.name
                        onClick: {
                            fourteen.update_feed(
                                fourteen.pagedata.feed_id,
                                edit_feed_title.text,
                                edit_feed_descr.text,
                                "Visible"
                            )

                            fourteen.proceed_to_page({
                                "page": "feed", 
                                "feed": fourteen.pagedata.feed_id
                            })
                        }
                    }
                }
            }

        }

    }
}
