import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import ".."
import "../reusable"

Item {
    id: editor_root
    y: dp(48)
    width: parent.width
    height: parent.height - y

    property var currentTags: (fourteen.pagedata.post.tags || [])
    property var suggestedTags: []
    property var suggestedTagsCount: []

    function onPostTextChanged() {
        var text = (""+post_data.text).toLowerCase()
        var tags = fourteen.pagedata.tags;

        var tagsOcurrences = []
        var tagsCounts = []

        for (var tag of tags) {
            var regex = RegExp(triggerToRe(tag.trigger, true));

            var ocurrence_count = 0;
            
            while (regex.exec(text)) {
                ocurrence_count += 1;
            }

            if (ocurrence_count > 0) {
                tagsOcurrences.push(tag.uuid);
                tagsCounts.push(ocurrence_count);
            }
        }

        suggestedTags = tagsOcurrences;
        suggestedTagsCount = tagsCounts;
    }

    // is_new_trigger = true means "seach only triggers that doesnt start w/ ["
    // is_new_trigger = false means "search only triggers that wrapped between [ and ][n]"
    function triggerToRe(trigger, is_new_trigger) {
        if (is_new_trigger) {
            return new RegExp("((?!\\[).|^)(" + trigger + ")", "gm");
        } else {
            return new RegExp("\\[(" + trigger + ")\\]\\[\\d+\\]", "gm");
        }
    }

    function getTagById(tag_id) {
        for (var t of fourteen.pagedata.tags) {
            if (t.uuid === tag_id) {
                return t;
            }
        }

        return null
    }

    function getTagMatchBounds(tag_id, is_new_trigger) {
        var tag = getTagById(tag_id);

        var text = (""+post_data.text).toLowerCase()
        var regex = triggerToRe(tag.trigger, is_new_trigger);
        var match = regex.exec(text);

        var start = regex.lastIndex - match[0].length;
        var end = regex.lastIndex;

        if (is_new_trigger) {
            // Regex may capture one additional char at start
            // So we'll offset start index by 1 if capture is not at line start
            if (start != 0 && text.charAt(start-1) != '\n') {
                start += 1;
            }
        }

        return { 'start': start, 'end': end }
    }

    function getIndexedTagMatchBounds(index) {
        var text = (""+post_data.text).toLowerCase();

        // Tags are indexed from 1, hence the (index+1)
        var regex = new RegExp("\\[[^\\[]+\\]\\["+(index+1)+"\\]", "gm");
        var match = regex.exec(text);

        var start = regex.lastIndex - match[0].length;
        var end = regex.lastIndex;

        return { 'start': start, 'end': end }
    }

    function selectTag(tag_id, is_new_trigger) {
        var { start, end } = getTagMatchBounds(tag_id, is_new_trigger);
        post_data.select(start, end);
    }

    function pickTag(tag_id) {
        var { start, end } = getTagMatchBounds(tag_id, true);
        var src = post_data.text.substring(start, end);

        var tag_index = currentTags.indexOf(tag_id);
        if (tag_index == -1) {
            tag_index = currentTags.length;
            currentTags.push(tag_id);
            currentTagsChanged();
        } 

        var dst = "[" + src + "][" + (tag_index + 1) + "]";
        
        post_data.text = post_data.text.substring(0, start) + dst + post_data.text.substring(end, post_data.text.length);
    }

    function dropTag(tag_id) {
        var tag_index = currentTags.indexOf(tag_id);

        while (true) {
            try {
                var { start, end } = getIndexedTagMatchBounds(tag_index);
                
                var from = post_data.text.substring(start, end);
                var to = from.substring(1, from.indexOf(']'));

                post_data.text = post_data.text.replace(from, to);
            } catch (e) {
                break;
            }
        }

        for (var i = tag_index; (i + 1) < currentTags.length; i++) {
            // Tags are indexed from 1, hence the i+2
            var from = "][" + (i + 2) + "]";
            var to = "][" + (i + 1) + "]";
            post_data.text = post_data.text.replace(from, to);
        }

        currentTags.splice(tag_index, 1);
        currentTagsChanged();
    }

    Row {   // Editor itself
        x: dp(16)
        y: dp(16)
        width: parent.width - x
        height: parent.height - 2*y
        spacing: dp(16)

        Column {    // Left column
            id: block_post
            width: dp(272)
            spacing: dp(16)

            Item {
                id: publish
                z: 10 // There are some overlays in 'publish' block
                width: parent.width
                height: pub_col.height + 2*dp(16)

                Rectangle {
                    width: parent.width
                    height: parent.height
                    color: theme.sand_light
                }

                ColumnLayout {
                    id: pub_col
                    width: parent.width - 2*dp(16)
                    x: dp(16)
                    y: dp(16)
                    spacing: 0

                    Title {
                        text: gettext("Publish")
                        icon: "feed/ic_info.png"
                    }

                    Item {
                        height: dp(16)
                    }

                    CustomTextField {
                        id: post_title
                        text: fourteen.pagedata.post.title || ""
                    }

                    Item {
                        height: dp(16)
                    }

                    PropEntry { // Date picker
                        id: postdate_prop
                        z: 10
                        height: dp(32)
                        title: getTitle()
                        icon: "ic_calendar.png"
                        Layout.fillWidth: true

                        property bool showCalendar: false

                        function getTitle() {
                            return gettext("Date: ") + "<b>" + getDate() + "</b>"
                        }

                        function getDate() {
                            if (fourteen.pagedata.post.created_at > 0) {
                                return bugs.getFormattedDate(fourteen.pagedata.post.created_at);
                            } else {
                                return gettext("now");
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            enabled: !postdate_prop.showCalendar

                            onClicked: {
                                postdate_prop.showCalendar = !postdate_prop.showCalendar

                                if (postdate_prop.showCalendar) {
                                    datetime_input.forceActiveFocus()

                                    if (fourteen.pagedata.post.created_at > 0) {
                                        datetime_input.text = bugs.getFormattedDate(fourteen.pagedata.post.created_at);
                                    } else {
                                        datetime_input.text = bugs.getFormattedDate(Date.now() - 1);
                                    }
                                    
                                }
                            }
                        }

                        Item {
                            visible: postdate_prop.showCalendar
                            x: dp(36)
                            width: parent.width - x
                            height: parent.height

                            Rectangle {
                                anchors.fill: parent
                                color: theme.sand_light
                            }

                            TextInput {
                                id: datetime_input
                                y: (parent.height - height) / 2
                                width: parent.width - (2*dp(24) + dp(4))
                                text: "date!!"
                            }

                            BtnIconOnly {
                                property var locale: Qt.locale()
                                icon: "imgs/accept_sand24.png"
                                x: parent.width - 2 * width - dp(4)
                                y: (parent.height - height) / 2
                                onClick: { 
                                    postdate_prop.showCalendar = false;

                                    var millis = Date.fromLocaleString(Qt.locale(), ""+datetime_input.text, "dd.MM.yyyy hh:mm") - 0;

                                    if (!isNaN(millis)) {
                                        fourteen.pagedata.post.created_at = millis;
                                    }

                                    postdate_prop.title = postdate_prop.getTitle()
                                }
                            }

                            BtnIconOnly {
                                icon: "imgs/close_sand24.png"
                                x: parent.width - width
                                y: (parent.height - height) / 2
                                onClick: { 
                                    postdate_prop.showCalendar = false;
                                }
                            }

                        }

                    }

                    ComboBox { // Post type selector
                        id: post_type_dropdown
                        height: dp(32)
                        Layout.fillWidth: true

                        model: [ gettext("default"), gettext("featured") ]
                        currentIndex: getSelectionIndex()

                        onCurrentIndexChanged: {}

                        function getSelectionIndex() {
                            var fmt = fourteen.pagedata.post.format;

                            if (fmt == "Featured") return 1;
                            return 0; // Default
                        }

                        function getPostFormat() { // Those are NOT a user-friendly post format names!
                            if (currentIndex == 0) return "Default";
                            if (currentIndex == 1) return "Featured";
                            console.log("Bad things happend at getPostFormat");
                        }

                        background: Rectangle {
                            implicitWidth: post_type_dropdown.implicitWidth
                            implicitHeight: post_type_dropdown.implicitHeight
                            color: theme.sand_light
                            border.width: 0
                        }

                        indicator: Item { }

                        contentItem: PropEntry {
                            width: post_type_dropdown.width
                            height: post_type_dropdown.height
                            title: gettext("Format: ") + "<b>" + post_type_dropdown.displayText + "</b>"
                            icon: "ic_shelf.png"
                        }
                    }


                    Item {
                        height: dp(16)
                        visible: post_type_dropdown.currentIndex == 1
                    }

                    Item {  // Featured image picker
                        height: dp(112)
                        Layout.fillWidth: true
                        visible: post_type_dropdown.currentIndex == 1

                        Rectangle {
                            height: dp(112)
                            width: dp(232)
                            color: headpic_hover.hovered ? theme.gray_bold_hover : theme.gray_bold
                            anchors.centerIn: parent
                        }

                        Image {
                            id: postpic_preview
                            height: dp(108)
                            width: dp(228)
                            anchors.centerIn: parent
                            source: getPicFullSource()
                            asynchronous: true
                            fillMode: Image.PreserveAspectCrop

                            function getPicFullSource() {
                                if (fourteen.pagedata.post.f_img == undefined) return "";
                                if (fourteen.pagedata.post.f_img == "") return "";
                                return "image://fourteen/" + fourteen.pagedata.feed_id + "/" + fourteen.pagedata.post.f_img;
                            }
                        }

                        Text {
                            visible: postpic_preview.source == ""
                            anchors.centerIn: parent
                            text: gettext("Click here to pick image")
                            color: theme.sand_light
                            font.family: theme.inter_bold_semi.name
                            font.pixelSize: dp(14)
                        }

                        HoverHandler {
                            id: headpic_hover
                            acceptedButtons: Qt.NoButton // Don't eat the mouse clicks
                            cursorShape: Qt.PointingHandCursor
                        }

                        MouseArea {
                            height: dp(108)
                            width: dp(228)
                            anchors.centerIn: parent
                            cursorShape: Qt.PointingHandCursor

                            onClicked: {
                                drag_area.enabled = false;

                                var onMediaPicked = (paths) => {
                                    if (paths.length > 0) {
                                        var path = paths[0];
                                        fourteen.pagedata.post.f_img = path;
                                        postpic_preview.source = "image://fourteen/" + fourteen.pagedata.feed_id + "/" + path;
                                    }
                                };

                                popup.show_media(onMediaPicked);
                            }
                        }
                    }

                    Item {
                        height: dp(16)
                    }

                    Item {
                        height: dp(32)
                        width: parent.width

                        BtnIcon {
                            label: gettext("Delete")
                            visible: !fourteen.pagedata.is_new
                            backgroundColorNormal: theme.sand_light
                            backgroundColorHover: theme.sand_light
                            textColorNormal: theme.accent
                            textColorHover: theme.accent_hover
                            onClick: {
                                popup_del_confirm.visible = true
                            }
                        }

                        BtnIcon {
                            x: parent.width - width
                            label: fourteen.pagedata.is_new ? gettext("Post") : gettext("Update")
                            onClick: {
                                fourteen.pagedata.post.title = post_title.getEditedText();
                                fourteen.pagedata.post.text = post_data.text;
                                fourteen.pagedata.post.format = post_type_dropdown.getPostFormat();
                                fourteen.pagedata.post.tags = editor_root.currentTags;

                                fourteen.update_post(
                                    fourteen.pagedata.feed_id,
                                    JSON.stringify(fourteen.pagedata.post)
                                );
                            }
                        }
                    }
                }

                Item {
                    id: popup_del_confirm
                    width: dp(246)
                    height: dp(56)
                    x: dp(8)
                    y: parent.height - height - dp(8)
                    visible: false

                    Rectangle { // Frame bg
                        width: parent.width
                        height: parent.height
                        color: theme.gray_light
                        x: dp(4)
                        y: dp(4)
                    }

                    Rectangle { // Frame
                        width: parent.width
                        height: parent.height
                        color: theme.sand_light
                        border.color: theme.gray_bold
                        border.width: dp(1)
                    }

                    Text {
                        height: parent.height - dp(2)
                        x: dp(16)
                        text: gettext("You sure?")
                        verticalAlignment: Text.AlignVCenter
                        font.family: theme.inter_bold_semi.name
                        font.pixelSize: dp(14)
                        color: theme.gray_bold
                    }

                    BtnIconOnly {
                        icon: "imgs/close_sand24.png"
                        x: parent.width - 2 * width - dp(20)
                        y: parent.height - height - dp(16)
                        onClick: { 
                            popup_del_confirm.visible = false;
                        }
                    }

                    BtnIconOnly {
                        icon: "imgs/accept_sand24.png"
                        x: parent.width - width - dp(16)
                        y: parent.height - height - dp(16)
                        onClick: { 
                            popup_del_confirm.visible = false;
                            fourteen.delete_post(fourteen.pagedata.id);
                        }
                    }

                }
            }

            BtnBorder {
                label: gettext("Manage tags")
                width: parent.width
                onClick: { popup.show_tags() }
            }
        }

        Item { // Editor
            id: editor
            height: window.height - 5*dp(16) // parent.height doesnt work well here
            width: parent.width - 2*dp(272) - 3*dp(16)

            function onMediaPicked(paths) {
                drag_area.enabled = true;

                if(paths.length == 0) return;

                var text = "";
                for (var path of paths) {
                    text += "\n\n![]("+path+")";
                }
                text += "\n\n";

                post_data.insert(post_data.cursorPosition, text);
            }

            Rectangle { // Bg
                width: parent.width
                height: parent.height
                color: theme.sand_light
            }

            Row { // Top buttons
                x: dp(8)
                y: dp(8)
                height: dp(32)
                width: parent.width - dp(16)

                BtnIcon {
                    label: gettext("Media")
                    icon: "editor/ic_medialib.png"
                    backgroundColorNormal: "transparent"
                    backgroundColorHover: "transparent"
                    textColorNormal: theme.gray_bold
                    textColorHover: theme.gray_bold_hover
                    onClick: {
                        drag_area.enabled = false;
                        popup.show_media(editor.onMediaPicked);
                    }
                }
            }

            Rectangle { // Frame
                width: parent.width - dp(16)
                height: parent.height - y - dp(32)
                x: dp(8)
                y: dp(8) + dp(40)
                color: theme.sand_light
                border.color: theme.gray_light
                border.width: dp(1)
            }

            ScrollView {
                id: view
                width: parent.width - 2*dp(16)
                height: parent.height - y - dp(32)
                x: dp(8)
                y: dp(8) + dp(40)

                TextArea {
                    id: post_data
                    placeholderText: gettext("Quick markdown tips:\n\n- Paragraphs are divided by two new lines;\n- Text styles: **bold**, *italic*, ~~strikethrough~~;\n- Lists can be unordered (starts with - or *, like this one), or ordered (starts with 1. 2. 3.);\n  - Lists can be nested;\n- Headings starts with #;\n- Blockquote starts with >;\n- Use media library to insert pictures;\n\nExample:\n\n# Top-level heading\n\nA **wise man** once said:\n>Nothing\n\n## Second-level heading\n\n![](2024/picture.png)")
                    placeholderTextColor: theme.gray_light
                    text: fourteen.pagedata.post.text || ""
                    font.family: theme.inter_bold_semi.name
                    font.pixelSize: dp(16)
                    font.letterSpacing: 1.0
                    color: theme.gray_bold
                    selectionColor: theme.accent
                    selectedTextColor: theme.sand_bg
                    wrapMode: TextEdit.Wrap
                    selectByMouse: true

                    onTextChanged: { 
                        statsCounterInfo.onTextEdited()
                        editor_root.onPostTextChanged()
                    }

                    property Text placeholder: children[0]

                    Binding {
                        target: post_data.placeholder
                        property: "wrapMode"
                        value: Text.Wrap
                    }
                }
            }

            Text {
                id: statsCounterInfo
                height: dp(32)
                x: dp(10)
                y: parent.height - height
                text: gettext("Awaiting input")
                font.family: theme.inter_medium.name
                font.pixelSize: dp(14)
                color: theme.gray_light
                verticalAlignment: Text.AlignVCenter
                function onTextEdited() {
                    var word_count;
                    if ((post_data.text || "").trim().length == 0) word_count = 0;
                    else word_count = (post_data.text.trim().match(/[\s\.\!\?\:\;]+/g) || []).length + 1;

                    var symb_count = post_data.text.length;

                    text = word_count + gettext(" words, ") + symb_count + gettext(" symbols")
                }
            }
        }

        ScrollViewFix {    // Right column
            width: dp(272)
            height: parent.height
            contentHeight: right_column.height
            ScrollBar.vertical.policy: ScrollBar.AsNeeded

            Column {
                id: right_column
                width: parent.width
                spacing: dp(16)

                Item {  // Applied tags list
                    id: selected_tags
                    width: parent.width
                    height: selected_tags_body.height + 2*dp(16)

                    Rectangle { // Bg
                        width: parent.width
                        height: parent.height
                        color: theme.sand_light
                    }

                    ColumnLayout {
                        id: selected_tags_body
                        width: parent.width - 2*dp(16)
                        x: dp(16)
                        y: dp(16)
                        spacing: 0

                        Title {
                            text: gettext("Tags")
                            icon: "feed/ic_info.png"
                        }

                        Item {
                            height: dp(16)
                        }

                        Item {
                            height: dp(32)
                            width: parent.width
                            visible: editor_root.currentTags.length == 0

                            Text {
                                anchors.fill: parent
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                text: gettext("No tags picked")
                                color: theme.gray_light
                                font.family: theme.inter_medium.name
                                font.pixelSize: dp(16)
                            }
                        }

                        Repeater {
                            model: editor_root.currentTags

                            TagEntry {
                                required property var modelData
                                required property int index

                                tag_id: modelData
                                tag_no: currentTags.indexOf(modelData) + 1
                                is_tick: false
                                onHoverEnter: { selectTag(modelData, false); }
                                onHoverExit: { post_data.deselect(); }
                                onPicked: { dropTag(modelData); }
                            }
                        }

                    }
                    
                }

                Item {  // Suggested tags list
                    id: suggested_tags
                    width: parent.width
                    height: suggested_tags_body.height + 2*dp(16)

                    Rectangle {
                        width: parent.width
                        height: parent.height
                        color: theme.sand_light
                    }

                    ColumnLayout {
                        id: suggested_tags_body
                        width: parent.width - 2*dp(16)
                        x: dp(16)
                        y: dp(16)
                        spacing: 0

                        Title {
                            text: gettext("Suggested tags")
                            icon: "feed/ic_info.png"
                        }

                        Rectangle {
                            height: dp(16)
                        }

                        Item {
                            height: dp(32)
                            width: parent.width
                            visible: editor_root.suggestedTags.length == 0

                            Text {
                                anchors.fill: parent
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                text: gettext("No suggested tags available")
                                color: theme.gray_light
                                font.family: theme.inter_medium.name
                                font.pixelSize: dp(16)
                                wrapMode: Text.WordWrap
                            }
                        }

                        Repeater {
                            model: editor_root.suggestedTags

                            TagEntry {
                                required property var modelData
                                required property int index

                                tag_id: modelData
                                onHoverEnter: { selectTag(modelData, true); }
                                onHoverExit: { post_data.deselect(); }
                                onPicked: { pickTag(modelData); }
                            }
                        }

                    }
                    
                }

                Item {  // Emojis list
                    id: emoji_picker
                    width: parent.width
                    height: emoji_picker_body.height + 2*dp(16)

                    Rectangle {
                        width: parent.width
                        height: parent.height
                        color: theme.sand_light
                    }

                    ColumnLayout {
                        id: emoji_picker_body
                        width: parent.width - 2*dp(16)
                        x: dp(16)
                        y: dp(16)
                        spacing: 0

                        Title {
                            text: gettext("Emoticons")
                            icon: "feed/ic_info.png"
                        }

                        Rectangle {
                            height: dp(16)
                        }

                        Item {
                            height: dp(32)
                            width: parent.width
                            visible: fourteen.pagedata.emoji_names.length == 0

                            Text {
                                anchors.fill: parent
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                text: gettext("No emoticons available")
                                color: theme.gray_light
                                font.family: theme.inter_medium.name
                                font.pixelSize: dp(16)
                                wrapMode: Text.WordWrap
                            }
                        }

                        GridLayout {
                            columns: 6
                            columnSpacing: 6
                            rowSpacing: 6
                            visible: fourteen.pagedata.emoji_names.length != 0

                            Repeater {
                                model: fourteen.pagedata.emoji_names

                                BtnEmoji {
                                    required property var modelData
                                    required property int index

                                    icon: fourteen.pagedata.emoji_root + "/" + modelData + ".png"

                                    onClick: {
                                        post_data.insert(post_data.cursorPosition, ":"+modelData+":");
                                    }
                                }
                            }

                        }
                    }
                }
            }

        }

    }

    Item {  // Drag'n'drop overlay
        id: drag_overlay
        width: parent.width
        height: parent.height
        visible: false
        
        Background {
            with_solid_color: false
            bg_offset: app_background.bg_offset
        }

        Text {
            anchors.centerIn: parent
            text: gettext("Drop here to add file to post")
            font.family: theme.inter_bold_extra.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
        }
    }

    DropArea {
        id: drag_area
        width: parent.width
        height: parent.height
        enabled: true

        onEntered: (drag) => {
            drag.accept (Qt.LinkAction);
            drag_overlay.visible = true;
        }
        onExited: {
            drag_overlay.visible = false;
        }
        onDropped: (drag) => {
            drag_overlay.visible = false;
            var files = fourteen.add_media_entries(
                fourteen.pagedata.feed_id, 
                JSON.stringify(drag.urls)
            );
            editor.onMediaPicked(files);
        }
    }
}