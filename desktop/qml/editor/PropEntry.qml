import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15


Item {
    property string title: ""
    property string icon: ""

    id: pnt
    height: dp(32)

    Rectangle {
        width: parent.width
        height: parent.height
        color: ptype_hover.hovered ? theme.sand_light_hover : theme.sand_light
        border.width: 0
    }

    Rectangle {
        x: dp(4)
        y: dp(4)
        width: dp(24)
        height: dp(24)
        color: theme.gray_light

        Image {
            anchors.centerIn: parent
            source: pnt.icon
        }
    }

    Text {
        x: dp(36)
        height: dp(32)
        text: parent.title
        color: theme.gray_bold
        font.family: theme.inter_medium.name
        font.pixelSize: dp(14)
        verticalAlignment: Image.AlignVCenter
        Layout.alignment: Qt.AlignVCenter
        Layout.leftMargin: dp(10)
        Layout.fillWidth: true
    }

    HoverHandler {
        id: ptype_hover
        acceptedButtons: Qt.NoButton // Don't eat the mouse clicks
        cursorShape: Qt.PointingHandCursor
    }

}