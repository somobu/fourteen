import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property string icon
    signal click

    id: itemRoot
    height: dp(35)
    width: dp(35)

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: theme.transparent
    }

    Image {
        source: itemRoot.icon
        x: dp(2)
        y: dp(2)
        width: parent.width - 2*x
        height: parent.height - 2*y
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { lbl_bg.color = "#80586E75" }
        onExited:  { lbl_bg.color = theme.transparent }
        onClicked: { itemRoot.click() }
    }
}
