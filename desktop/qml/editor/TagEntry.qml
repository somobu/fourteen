import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import "../reusable"

Item {
    property string tag_id
    property string tag_no: ""

    /// Switch between "tick" and "cross" icon on pick button
    property bool is_tick: true
    
    signal picked
    signal hoverEnter
    signal hoverExit

    id: pnt
    height: dp(32)
    width: parent.width

    property var tag: lookupTag()

    function lookupTag() {
        var tags = fourteen.pagedata.tags

        for (var tag of tags) {
            if (tag.uuid === tag_id) return tag; 
        }

        return null
    }

    Item {
        width: parent.width
        height: dp(24)
        y: parent.height / 2 - height/2

        Rectangle {
            width: parent.width
            height: parent.height
            color: ptype_hover.hovered ? theme.sand_light_hover : theme.sand_light
            border.width: 0
        }

        Text {
            height: parent.height
            width: dp(24)
            text: ""+pnt.tag_no
            color: theme.gray_light
            font.family: theme.inter_medium.name
            font.pixelSize: dp(16)
            verticalAlignment: Image.AlignVCenter
            horizontalAlignment: Text.AlignRight
        }

        Text {
            height: parent.height
            x: dp(32)
            text: pnt.tag.name
            color: theme.gray_bold
            font.family: theme.inter_medium.name
            font.pixelSize: dp(16)
            verticalAlignment: Image.AlignVCenter
        }

        HoverHandler {
            id: ptype_hover
            acceptedButtons: Qt.NoButton

            onHoveredChanged: {
                hovered ? pnt.hoverEnter() : pnt.hoverExit()
            }
        }

        BtnIconOnly {
            visible: ptype_hover.hovered
            icon: pnt.is_tick ? "imgs/accept_sand24.png" : "imgs/close_sand24.png"
            x: parent.width - width
            onClick: { pnt.picked() }
        }
    }

}