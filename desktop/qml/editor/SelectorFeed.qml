import QtQuick 2.15
import QtQuick.Layouts 1.15

Item {
    property string selected_feed_id: "test"
    property bool show_selector: false
    property bool enabled: true

    id: itemRoot
    height: dp(24)
    width: parent.width
    z: show_selector ? 10 : 0

    function onSelectionChange() {
        label.updFmtTxt()
    }

    Item {  // Icon
        height: dp(24)
        width: dp(24)

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.gray_light
        }

        Image {
            anchors.centerIn: parent
            source: "ic_shelf.png"
        }
    }

    Text {  // Text
        id: label
        x: dp(32)
        width: parent.width - 2*x
        height: parent.height
        text: getFormattedText()
        color: theme.gray_bold
        font.family: theme.inter_bold_semi.name
        font.pixelSize: dp(14)
        verticalAlignment: Text.AlignVCenter

        function updFmtTxt() {
            text = getFormattedText()
        }

        function getFormattedText() {
            var name = "UNKNOWN";

            for (var o of fourteen.pagedata.feeds) {
                if (o.id === itemRoot.selected_feed_id) {
                    name = o.name;
                    break;
                }
            }

            return "Feed: <b>"+name+"</b>"
        }
    }

    Item { // Edit btn
        id: btn_edit
        height: dp(24)
        width: dp(24)
        x: parent.width - width
        enabled: itemRoot.enabled
        visible: itemRoot.enabled
        signal click()

        Rectangle {
            id: btn_edit_bg
            width: parent.width
            height: parent.height
            color: theme.gray_bold
        }

        Image {
            anchors.centerIn: parent
            source: itemRoot.show_selector ? "ic_close24.png" : "../imgs/ic_edit.png"
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: { itemRoot.show_selector = !itemRoot.show_selector }
            onEntered: { btn_edit_bg.color = theme.gray_bold_hover }
            onExited:  { btn_edit_bg.color = theme.gray_bold }
        }
    }

    Item {
        id: select_dropdown
        x: dp(30)
        y: parent.height + dp(6)
        width: parent.width - x
        height: dropdown_column.height + dp(18)
        visible: itemRoot.show_selector

        Rectangle { // Frame
            width: parent.width
            height: parent.height
            color: theme.sand_light
            border.color: theme.gray_light
            border.width: dp(1)
        }

        ColumnLayout {
            id: dropdown_column
            x: dp(10)
            y: dp(8)
            spacing: dp(8)

            Repeater {
                model: fourteen.pagedata.feeds

                Item {
                    required property int index
                    height: dp(16)
                    width: select_dropdown.width - dp(10)

                    function is_selected() {
                        return fourteen.pagedata.feeds[index].id === itemRoot.selected_feed_id
                    }

                    Rectangle { // Frame
                        width: dp(12)
                        height: dp(12)
                        y: dp(2)
                        color: parent.is_selected() ? theme.gray_bold : theme.sand_light
                        border.color: theme.gray_bold
                        border.width: dp(2)
                    }

                    Text {
                        x: dp(16)
                        color: theme.gray_bold
                        font.family: theme.inter_bold_semi.name
                        font.pixelSize: dp(14)
                        verticalAlignment: Text.AlignVCenter
                        text: fourteen.pagedata.feeds[parent.index].name
                    }

                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        hoverEnabled: true
                        propagateComposedEvents: true
                        onClicked: { 
                            itemRoot.selected_feed_id = fourteen.pagedata.feeds[parent.index].id 
                            itemRoot.onSelectionChange()
                            itemRoot.show_selector = false
                        }
                        onEntered: { /*btn_edit_bg.color = theme.gray_bold_hover*/ }
                        onExited:  { /*btn_edit_bg.color = theme.gray_bold*/ }
                    }

                }


            }

        }
    }
}