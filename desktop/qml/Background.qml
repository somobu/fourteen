import QtQuick 2.15

Item {
    width: window.width
    height: window.height
    property bool with_solid_color: true
    property double bg_offset: 0

    // Internal props
    property int side_px: dp(18)

    Rectangle {
        width: parent.width
        height: parent.height
        color: parent.with_solid_color ? theme.sand_bg : "#93A1A1"
        opacity: parent.with_solid_color ? 1.0 : 0.5
    }

    Image {
        y: -parent.side_px - ((parent.bg_offset|0) % parent.side_px)
        width: parent.width
        height: parent.height + parent.side_px
        fillMode: Image.Tile
        source: "imgs/bg_tile.png"
    }

    Image {
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectCrop
        source: "imgs/bg_vignette.png"
        visible: parent.with_solid_color
    }
}