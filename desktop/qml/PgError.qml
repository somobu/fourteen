import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


ScrollViewFix {
    id: feed_root
    y: header.height
    width: parent.width
    height: parent.height - y
    contentHeight: feed_column.height

    Text {
        id: feed_column
        width: parent.width
        height: parent.height
        text: gettext("Page ") + fourteen.pagedata.url
        + "\n" + gettext("Produced an error ") +"\n" + fourteen.pagedata.error
        + "\n\n\n" + gettext("Backtrace:") + "\n" + fourteen.pagedata.backtrace
        font.family: theme.inter_bold_extra.name
        font.pixelSize: dp(16)
        color: theme.gray_bold
        wrapMode: Text.WordWrap
    }


}