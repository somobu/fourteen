import QtQuick 2.15

import ".."

Item {

    Item {
        width: dp(480)
        height: longtext.height + 2*longtext.y
        anchors.centerIn: parent

        Rectangle {
            x: dp(4) 
            y: dp(4)
            width: parent.width
            height: parent.height
            color: theme.gray_light
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        Text {
            id: longtext
            x: dp(16)
            y: dp(16)
            width: parent.width - 2*x
            text: gettext("Sorry, this feature is not unimplemented yet")
            font.family: theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
        }
    }
}