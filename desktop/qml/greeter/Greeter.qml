import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import ".."
import "../reusable"

Item {

    Item {
        width: dp(736)
        height: dp(128)
        anchors.centerIn: parent

        Rectangle {
            x: dp(4)
            y: dp(4)
            width: parent.width
            height: parent.height
            color: theme.gray_light
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        ColumnLayout {
            id: content
            width: parent.width - 2*dp(16)
            height: parent.height - dp(24)
            y: dp(16)
            x: dp(16)

            Text {
                text: gettext("Welcome!")
                font.family: theme.inter_bold_extra.name
                font.pixelSize: dp(16)
                color: theme.gray_bold
            }

            Text {
                text: gettext("This minimalistic diary/blog provides the following feeds:")
                font.family: theme.inter_medium.name
                font.pixelSize: dp(16)
                color: theme.gray_light
            }

            ScrollView {
                width: parent.width
                height: dp(32)
                contentWidth: feedsRow.width
                contentHeight: dp(32)
                clip: true
                Layout.fillWidth: true
                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOff

                RowLayout {
                    id: feedsRow
                    width: Math.max(content.width, implicitWidth)
                    height: parent.height
                    spacing: dp(16)

                    Repeater {
                        model: fourteen.pagedata.feed_ids

                        BtnIcon {
                            required property string modelData
                            required property int index
                            label: getTitle()
                            icon: fourteen.pagedata.feed_privs[index] ? "imgs/ic_key.png" : ""
                            textSize: dp(16)
                            textStyle: theme.inter_bold_extra.name
                            onClick: { fourteen.proceed_to_page({"page": "feed", "feed": modelData}) }

                            function getTitle() {
                                var v = fourteen.pagedata.feed_names[index];
                                return v === "" ? gettext("Untitled") : v
                            }
                        }
                    }


                    Item {  // Spacer
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }


                    BtnIcon {
                        label: gettext("Create +")
                        backgroundColorNormal: theme.sand_light
                        backgroundColorHover: theme.sand_light
                        textColorNormal: theme.accent
                        textColorHover: theme.accent_hover
                        textSize: dp(16)
                        textStyle: theme.inter_bold_extra.name
                        onClick: { access_popup.visible = true }
                    }
                }
            }
        }

        Item { // Access popup
            id: access_popup
            width: dp(272)
            height: dp(136)
            x: parent.width - width - dp(8)
            y: dp(8)
            visible: false

            function confirm_new_feed() {
                access_popup.visible = false;
                
                var new_feed_id = fourteen.add_feed(access_field.text);
                fourteen.proceed_to_page({"page": "feed", "feed": new_feed_id});

                access_field.text = "";
            }

            Rectangle {
                width: parent.width
                height: parent.height
                color: theme.sand_light
                border.color: theme.gray_light
                border.width: dp(1)
            }

            MouseArea { // Just to prevent underlying buttons from clicking
                width: parent.width
                height: parent.height
                hoverEnabled: true
            }

            Title {
                x: dp(16)
                y: dp(16)
                text: gettext("Create new feed")
                icon: "imgs/ic_key.png"
            }

            TextField {
                id: access_field
                width: dp(240)
                height: dp(32)
                x: dp(16)
                y: dp(64) - dp(8)
                horizontalAlignment: TextInput.AlignHCenter
                placeholderText: gettext("Feed name")
                color: theme.gray_bold
                font.family: theme.inter_medium.name
                font.pixelSize: dp(14)
                background: Rectangle {
                     color: theme.sand_light

                     Rectangle {
                        width: parent.width
                        height: dp(1)
                        y: parent.height - dp(4)
                        color: theme.gray_light
                     }
                }
                onAccepted: {
                    access_popup.confirm_new_feed()
                }
            }

            Text {  // This hint is meaningless in current state
                visible: false
                x: dp(16)
                y: parent.height - height - dp(16)
                height: dp(24)
                text: gettext("Drag or pick a file")
                color: theme.gray_light
                font.family: theme.inter_medium.name
                font.pixelSize: dp(14)
                lineHeightMode: Text.FixedHeight
                verticalAlignment: Text.AlignVCenter
            }

            BtnIconOnly {
                icon: "imgs/close_sand24.png"
                x: parent.width - 2 * width - dp(20)
                y: parent.height - height - dp(16)
                onClick: { 
                    access_popup.visible = false;
                    access_field.text = "";
                }
            }

            BtnIconOnly {
                icon: "imgs/accept_sand24.png"
                x: parent.width - width - dp(16)
                y: parent.height - height - dp(16)
                onClick: {
                    access_popup.confirm_new_feed()
                }
            }
        }
    }
}