import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import ".."

Item {

    Item {
        width: dp(480)
        height: longtext.height + 2*longtext.y
        anchors.centerIn: parent

        Rectangle {
            x: dp(4) 
            y: dp(4)
            width: parent.width
            height: parent.height
            color: theme.gray_light
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: theme.sand_light
        }

        Text {
            id: longtext
            x: dp(24)
            y: dp(24)
            width: parent.width - 2*x
            text: getFormattedText()
            font.family: theme.inter_medium.name
            font.pixelSize: dp(16)
            color: theme.gray_bold
            textFormat: TextEdit.MarkdownText
            onLinkActivated: Qt.openUrlExternally(link)

            function getFormattedText() {
                var baseText = gettext("\
**Fourteen v4**\n\n\
Author: Ilya \"Iria.Somobu\" Somov\n\n\
Source code: <a LINK_STYLE href=\"https://gitlab.com/somobu/fourteen\">on GitLab</a>\n\n\
Release: <a LINK_STYLE href=\"https://gitlab.com/somobu/fourteen/-/releases\">on GitLab</a>\n\n\
License: <a LINK_STYLE href=\"https://gitlab.com/somobu/fourteen/-/releases\">GNU GPL v3</a>\n\n\
");
                var linkStyleReplacer = "style=\"color: #2AA198; font-weight: bold; text-decoration: none;\""

                for (var i = 0; i < 30; i++) {
                    baseText = baseText.replace("LINK_STYLE", linkStyleReplacer);
                }

                return baseText
            }

                MouseArea {
                    width: parent.width
                    height: parent.height
                    acceptedButtons: Qt.NoButton
                    cursorShape: longtext.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                }

        }

    }

}