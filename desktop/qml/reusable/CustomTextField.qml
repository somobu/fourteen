import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15


Item {  // Post name
    property var text: ""
    property var fontSize: dp(14)

    height: dp(32)
    width: parent.width
    clip: true

    function getEditedText() {
        return post_title.text
    }

    Rectangle { // Frame
        width: parent.width
        height: parent.height
        color: theme.sand_light
        border.color: theme.gray_light
        border.width: 1
    }

    TextInput {
        id: post_title
        width: parent.width - dp(12)
        anchors.centerIn: parent
        text: parent.text
        font.family: theme.inter_bold_semi.name
        font.pixelSize: parent.fontSize
        color: theme.gray_bold
        selectionColor: theme.accent
        selectedTextColor: theme.sand_bg
        selectByMouse: true

        onTextEdited: {
            parent.text = text
        }
    }

    HoverHandler {
        id: headpic_hover
        acceptedButtons: Qt.NoButton
        cursorShape: Qt.IBeamCursor
    }


}

