import QtQuick 2.15
import QtQuick.Controls 2.15

// Button with text and optional icon

Item {
    property string label

    property var backgroundColorNormal: theme.sand_light
    property var backgroundColorHover: theme.sand_light
    property var borderColor: theme.gray_bold

    property var textColorNormal: theme.gray_bold
    property var textColorHover: theme.gray_bold_hover
    property var textSize: dp(14)
    property var textStyle: theme.inter_bold_semi.name

    signal click

    id: itemRoot
    height: dp(40)

    property bool hovered: false

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: itemRoot.hovered ? itemRoot.backgroundColorHover : itemRoot.backgroundColorNormal
        border.color: itemRoot.borderColor
        border.width: dp(2)
    }

    Text {
        id: lbl
        width: parent.width
        height: parent.height
        text: label
        color: itemRoot.hovered ? itemRoot.textColorHover : itemRoot.textColorNormal
        font.family: itemRoot.textStyle
        font.pixelSize: itemRoot.textSize
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { itemRoot.hovered = true }
        onExited:  { itemRoot.hovered = false }
        onClicked: { itemRoot.click() }
    }
}
