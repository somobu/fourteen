import QtQuick 2.15
import QtQuick.Controls 2.15

// Button with text and optional icon

Item {
    property string label
    property string icon: ""

    property var backgroundColorNormal: theme.accent
    property var backgroundColorHover: theme.accent_hover

    property var textColorNormal: theme.sand_light
    property var textColorHover: theme.sand_light
    property var textSize: dp(14)
    property var textStyle: theme.inter_bold_semi.name

    property var cursor: Qt.PointingHandCursor

    signal click

    id: itemRoot
    height: dp(32)
    width: lbl.width + dp(16) + (icon == "" ? 0 : dp(24))

    property bool hovered: false

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: itemRoot.hovered ? itemRoot.backgroundColorHover : itemRoot.backgroundColorNormal
    }

    Image {
        width: dp(16)
        height: dp(16)
        x: dp(8)
        y: dp(8)
        visible: itemRoot.icon !== ""
        source: itemRoot.icon == "" ? "" : "../"+itemRoot.icon
    }

    Text {
        id: lbl
        x: (itemRoot.icon == "" ? dp(8) : dp(30))
        height: parent.height
        text: label
        color: itemRoot.hovered ? itemRoot.textColorHover : itemRoot.textColorNormal
        font.family: itemRoot.textStyle
        font.pixelSize: itemRoot.textSize
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: itemRoot.cursor
        hoverEnabled: true
        onEntered: { itemRoot.hovered = true }
        onExited:  { itemRoot.hovered = false }
        onClicked: { itemRoot.click() }
    }
}
