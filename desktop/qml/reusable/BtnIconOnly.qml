import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property string icon
    signal click

    id: itemRoot
    height: dp(24)
    width: dp(24)

    Rectangle {
        id: lbl_bg
        width: parent.width
        height: parent.height
        color: theme.gray_bold
    }

    Image {
        source: itemRoot.icon == "" ? "" : "../" + itemRoot.icon
        x: parent.width * 0.16
        y: parent.width * 0.16
        width: parent.width - 2*x
        height: parent.height - 2*y
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onEntered: { lbl_bg.color = theme.gray_bold_hover }
        onExited:  { lbl_bg.color = theme.gray_bold }
        onClicked: { itemRoot.click() }
    }
}
