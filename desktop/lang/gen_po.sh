#!/bin/bash
set -euxo pipefail

# Extract strings
find ./qml/ -name '*.qml' | \
xgettext -f - \
  -p ./lang/ \
  -C \
  --default-domain="fourteen" \
  --sort-by-file \
  --from-code="UTF-8"

# Update translations
msgmerge --previous --update lang/po/ru.po lang/fourteen.po

# Overwrite en translation
mv lang/fourteen.po lang/po/en.po


