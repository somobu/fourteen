#!/bin/bash
set -euxo pipefail

# Generate .mo
mkdir -p lang/mo/en/LC_MESSAGES
msgfmt lang/po/en.po -o lang/mo/en/LC_MESSAGES/fourteen.mo

mkdir -p lang/mo/ru/LC_MESSAGES
msgfmt lang/po/ru.po -o lang/mo/ru/LC_MESSAGES/fourteen.mo

