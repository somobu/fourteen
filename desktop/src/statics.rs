use std::{cmp::min, collections::HashMap};

use once_cell::sync::Lazy;
use qmetaobject::QmlEngine;

use fourteen_lib::{
    database::Database,
    error::{self, OptionToError},
    raw::Feed,
};

pub static mut ENGINE: Lazy<QmlEngine> = Lazy::new(QmlEngine::new);
pub static mut PRIVKEYS: Lazy<HashMap<String, String>> = Lazy::new(HashMap::new);
pub static mut BACKEND: Option<Database> = None;

/// User option: enable client-side decorations
pub static mut CSD: bool = false;

/// User option: initial set of encryption keys
pub static mut INIT_PRIV_KEYS: Option<HashMap<String, String>> = None;

pub fn back() -> &'static mut Database {
    return unsafe { BACKEND.as_mut().expect("Wtf?! no backend present") };
}

pub fn get_priv_key(feed_id: &String) -> Option<&String> {
    return unsafe { PRIVKEYS.get(feed_id) };
}

pub fn has_priv_key(feed_id: &String) -> bool {
    unsafe { PRIVKEYS.contains_key(feed_id) }
}

pub trait NameByStatics {
    fn get_name(&self) -> error::Result<String>;
}

impl NameByStatics for Feed {
    fn get_name(&self) -> error::Result<String> {
        if self.private {
            if has_priv_key(&self.uuid) {
                let key = get_priv_key(&self.uuid).to_result()?;
                let inner = self.inner.clone().decrypt(key)?;
                Ok(inner.de()?.title.clone())
            } else {
                Ok(self.uuid[0..(min(8, self.uuid.len()))].to_string())
            }
        } else {
            Ok(self.inner.de()?.title.clone())
        }
    }
}
