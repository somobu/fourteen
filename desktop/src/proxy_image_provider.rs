use fourteen_lib::{
    container::{self, ContentFormat},
    crypto,
};

use crate::files;

use super::statics;

pub fn register() {
    unsafe {
        statics::ENGINE.add_proxy_image_provider("fourteen".into(), |id| {
            let elements: Vec<&str> = id.split('/').collect();

            if elements.len() < 2 {
                eprintln!("ImageProxy: malformed path {id}");
                return malformed_image();
            }

            let feed_id = elements[0].to_string();

            let path = files::get_userdata_folder() + id;
            let data = std::fs::read(&path);

            if data.is_err() {
                eprintln!("ImageProxy for {path}: error {}", data.unwrap_err());
                return malformed_image();
            }

            let mut data = data.unwrap();
            let header = container::Header::from_bytes(&data);

            let key = statics::get_priv_key(&feed_id);
            if key.is_some() {
                if header.is_some() {
                    let header = header.unwrap();
                    if header.content_format != ContentFormat::ChaChaPoly1305 {
                        eprintln!("ImageProxy for {path}: content format mismatch!");
                        return malformed_image();
                    }

                    let dec = crypto::decrypt(key.unwrap(), &data);
                    if dec.is_err() {
                        eprintln!("ImageProxy for {path}: unable to decrypt w/ valid header, giving up");
                        return malformed_image();
                    }

                    data = dec.unwrap();
                    return data;
                } else {
                    eprintln!("ImageProxy for {path}: unable to decrypt, returning data as-is (our last hope: file is not encrypted)");
                    return data;
                }
            }

            data
        })
    };
}

fn malformed_image() -> Vec<u8> {
    std::fs::read(files::get_appimage_prefix() + "/qml/imgs/malformed_image.png").unwrap()
}
