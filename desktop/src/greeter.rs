use serde::Serialize;

use fourteen_lib::error::{self};

use crate::statics::NameByStatics;

use super::statics::back;

#[derive(Debug, Default, Serialize)]
pub struct GreeterData {
    feed_names: Vec<String>,
    feed_ids: Vec<String>,
    feed_privs: Vec<bool>,
}

pub fn get() -> error::Result<serde_json::Value> {
    let data = back().list_feeds()?;

    let feed_names = data
        .iter()
        .map(|e| e.get_name())
        .collect::<Result<Vec<_>, error::Error>>()?;

    let feed_ids = data.iter().map(|e| e.uuid.clone()).collect::<Vec<_>>();
    let feed_privs = data.iter().map(|e| e.private).collect::<Vec<_>>();

    Ok(serde_json::to_value(GreeterData {
        feed_names,
        feed_ids,
        feed_privs,
    })?)
}
