//! Does the sync (open connection, get/push data)
//!
//! Knows nothing about threads, user sync info, qml conversion, etc.

use std::process::Command;

use fourteen_lib::{
    database::Database,
    delta::{Delta, DeltaSummary},
};

use serde::{Deserialize, Serialize};

use crate::files;

#[derive(Debug)]
pub enum SyncTarget {
    Local,
    LocalCargo { path: String },
    Ssh { user_server: String },
}

impl SyncTarget {
    pub fn base_command(&self) -> String {
        match self {
            SyncTarget::Local => format!("fourteen-cli"),
            SyncTarget::LocalCargo { path } => {
                format!("cargo run -p fourteen-cli -q -- -d {path}")
            }
            SyncTarget::Ssh { user_server } => {
                format!("ssh {user_server} fourteen-cli")
            }
        }
    }

    pub fn rsync_path(&self) -> String {
        match self {
            SyncTarget::Local => files::get_userdata_folder(),
            SyncTarget::LocalCargo { path } => path.clone() + "/",
            SyncTarget::Ssh { user_server } => format!("{user_server}:~/.local/share/fourteen/"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SyncSummary {
    pub server: String,
    pub feed: String,
    pub remote_summary: DeltaSummary,
    pub local_summary: DeltaSummary,
}

#[derive(Debug)]
pub enum SyncError {
    BashIoError {
        cmd: String,
        error: String,
    },
    BashConversionError {
        cmd: String,
    },
    BashError {
        cmd: String,
        status: String,
        stdout: String,
        stderr: String,
    },
    SerdeError {
        input: String,
        error: String,
    },
    DbError {
        input: String,
        error: String,
    },
}

pub fn perform_sync(
    db: &mut Database,
    feed_id: &String,
    since: u64,
    remote: &SyncTarget,
    pull: bool,
    push: bool,
) -> Result<SyncSummary, SyncError> {

    // Sync posts

    let local = SyncTarget::LocalCargo { path: "~/.local/share/fourteen/".to_string() };
    let local_base = local.base_command();
    let remote_base = remote.base_command();

    // Step 1: fetch updates from remote

    let cmdstr = format!("{remote_base} export --feed {feed_id} --since {since}");
    let remote_delta = bash(&cmdstr)?;
    let remote_delta: Delta =
        serde_json::from_str(&remote_delta).map_err(|e| SyncError::SerdeError {
            input: remote_delta,
            error: e.to_string(),
        })?;

    // Step 2: push update to remote

    let remote_summary: DeltaSummary;
    if push {
        let cmdstr = format!("{local_base} export --feed {feed_id} --since {since} | {remote_base} import");

        let s = bash(&cmdstr)?;
        remote_summary = serde_json::from_str(&s).map_err(|e| SyncError::SerdeError {
            input: remote_base,
            error: e.to_string(),
        })?;
    } else {
        remote_summary = DeltaSummary::default();
    }

    // Step 3: pull update to local

    let local_summary;
    if pull {
        local_summary = remote_delta.into_db(&db).map_err(|e| SyncError::DbError {
            input: format!("remote_delta:#?"),
            error: e.to_string(),
        })?;
    } else {
        local_summary = DeltaSummary::default();
    }


    // Sync files

    let local = files::get_userdata_folder() + feed_id;
    let dst = remote.rsync_path() + feed_id;

    std::fs::create_dir_all(&local).unwrap();

    // Step 4: push files to remote
    if push {
        let cmdstr = format!("rsync -ru {local}/. {dst}");
        let rz = bash(&cmdstr)?;
        println!("Rsync push result: {rz}");
    }

    // Step 5: pull files from remote
    if pull {
        let cmdstr = format!("rsync -ru {dst}/. {local}");
        let rz = bash(&cmdstr)?;
        println!("Rsync pull result: {rz}");
    }

    Ok(SyncSummary {
        server: format!("{remote:?}"),
        feed: feed_id.clone(),
        remote_summary,
        local_summary,
    })
}

fn bash(cmd: &str) -> Result<String, SyncError> {
    let out = Command::new("bash")
        .arg("-c")
        .arg(cmd)
        .output()
        .map_err(|e| SyncError::BashIoError {
            cmd: cmd.to_string(),
            error: e.to_string(),
        })?;

    if out.status.success() {
        return Ok(
            String::from_utf8(out.stdout).map_err(|_| SyncError::BashConversionError {
                cmd: cmd.to_string(),
            })?,
        );
    } else {
        let stdout = String::from_utf8(out.stdout).map_err(|_| SyncError::BashConversionError {
            cmd: cmd.to_string(),
        })?;

        let stderr = String::from_utf8(out.stderr).map_err(|_| SyncError::BashConversionError {
            cmd: cmd.to_string(),
        })?;

        return Err(SyncError::BashError {
            cmd: cmd.to_string(),
            status: out.status.to_string(),
            stdout,
            stderr,
        });
    }
}
