use std::{collections::HashMap, usize};

use gregorian::Date;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::Serialize;

use super::statics::back;
use fourteen_lib::{
    error::{self},
    raw::{self, PostFormat, TagId},
    util::millis,
};

#[derive(Debug, Default, Serialize)]
pub struct FeedData {
    feed_id: String,
    year: i32,
    month: i32,

    title: String,
    descr: String,
    header_imgs: Vec<String>,

    years: Vec<String>,
    /// Map year -> months[]
    months_ids: HashMap<String, Vec<String>>,

    /// Time on date divider
    date: u64,
    posts: Vec<Post>,

    tag: Option<raw::Tag>,
}

#[derive(Debug, Default, Serialize)]
pub struct Post {
    id: String,
    title: String,
    address_str: String,
    date: u64,
    author_str: String,
    para: Vec<Para>,
    featured: bool,
    featured_img: String,
}

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "t")]
pub enum Para {
    #[serde(rename = "txt")]
    Text { txt: String },

    #[serde(rename = "img")]
    Image { src: String, descr: String },

    #[serde(rename = "gal")]
    Gallery { data: Vec<GalleryItem> },
}

#[derive(Debug, Clone, Serialize)]
pub struct GalleryItem {
    src: String,
    descr: String,
}

pub fn get(
    feed_id: String,
    year: i16,
    month: u8,
    key: Option<String>,
) -> error::Result<serde_json::Value> {
    let mut feed = back().get_feed(&feed_id)?;
    if key.is_some() {
        feed.inner = feed.inner.decrypt(key.as_ref().unwrap())?;
    }

    let date = (Date::new(year, month, 1)?.to_unix_timestamp() as u64) * 1000;

    let mut output = FeedData {
        feed_id: feed_id.clone(),
        year: year as i32,
        month: month as i32,

        title: feed.inner.de()?.title.clone(),
        descr: feed.inner.de()?.descr.clone(),
        header_imgs: feed
            .inner
            .de()?
            .header_imgs
            .iter()
            .map(|e| format!("{feed_id}/{e}"))
            .collect(),
        years: Vec::new(),
        months_ids: HashMap::new(),
        date,
        posts: Vec::new(),
        tag: None,
    };

    back().years_and_months(&feed_id)?.iter().for_each(|e| {
        output.years.push(e.0.to_string());
        output.months_ids.insert(
            e.0.to_string(),
            e.1.iter().map(|j| j.to_string()).collect::<Vec<_>>(),
        );
    });

    let a1 = millis();

    let posts = back().get_posts_by_month(&feed_id, year, month)?;

    let mut cached_usernames: HashMap<String, String> = HashMap::new();

    for e in posts {
        let post_inner = if key.is_some() {
            e.inner.decrypt(key.as_ref().unwrap())?
        } else {
            e.inner
        };
        let post_inner = post_inner.de()?;

        let l = cached_usernames.get(&post_inner.created_by);

        let name = if l.is_some() {
            l.unwrap().clone()
        } else {
            let user = back().get_user(&feed_id, &post_inner.created_by)?;

            let user_inner = if key.is_some() {
                user.inner.decrypt(key.as_ref().unwrap())?
            } else {
                user.inner
            };

            let user_inner = user_inner.de()?;

            cached_usernames.insert(post_inner.created_by.clone(), user_inner.name.clone());

            user_inner.name.clone()
        };

        let tags = post_inner.tags.clone();

        let para = split_paras(feed_id.clone(), post_inner.text.clone(), &tags);

        output.posts.push(Post {
            id: e.uuid.clone(),
            title: post_inner.title.clone(),
            address_str: "N/A".into(),
            date: e.created_at,
            author_str: name,
            para,
            featured: post_inner.format == PostFormat::Featured,
            featured_img: {
                if post_inner.f_img.trim().is_empty() {
                    "".to_string()
                } else {
                    format!("{feed_id}/{}", post_inner.f_img.clone())
                }
            },
        });
    }

    let a2 = millis();
    println!("Posts (1): {} ms", (a2 - a1));

    output.posts.sort_by_key(|e| e.date);

    Ok(serde_json::to_value(&output)?)
}

pub fn query_tag(
    feed_id: &String,
    tag_id: String,
    key: Option<String>,
) -> error::Result<serde_json::Value> {
    let mut feed = back().get_feed(feed_id)?;
    if key.is_some() {
        feed.inner = feed.inner.decrypt(key.as_ref().unwrap())?;
    }

    let tags = back().get_tags(&vec![tag_id.clone()])?;
    let mut tag = tags[0].clone();
    if key.is_some() {
        tag.inner = tag.inner.decrypt(key.as_ref().unwrap())?;
    }

    let mut output = FeedData {
        feed_id: feed_id.clone(),
        tag: Some(tag),

        title: feed.inner.de()?.title.clone(),
        descr: feed.inner.de()?.descr.clone(),
        header_imgs: feed
            .inner
            .de()?
            .header_imgs
            .iter()
            .map(|e| format!("{feed_id}/{e}"))
            .collect(),
        posts: Vec::new(),
        date: 0,
        year: 0,
        month: 0,
        years: Vec::new(),
        months_ids: HashMap::new(),
    };

    let a1 = millis();

    let posts = back().get_posts_by_tag(feed_id, &tag_id)?;

    for e in posts {
        let post_inner = if key.is_some() {
            e.inner.decrypt(key.as_ref().unwrap())?
        } else {
            e.inner
        };
        let post_inner = post_inner.de()?;

        let user = back().get_user(feed_id, &post_inner.created_by)?;
        let user_inner = if key.is_some() {
            user.inner.decrypt(key.as_ref().unwrap())?
        } else {
            user.inner
        };
        let user_inner = user_inner.de()?;

        let tags = post_inner.tags.clone();

        output.posts.push(Post {
            id: e.uuid.clone(),
            title: post_inner.title.clone(),
            address_str: "N/A".into(),
            date: e.created_at,
            author_str: user_inner.name.clone(),
            para: split_paras(feed_id.clone(), post_inner.text.clone(), &tags),
            featured: post_inner.format == PostFormat::Featured,
            featured_img: {
                if post_inner.f_img.trim().is_empty() {
                    "".to_string()
                } else {
                    format!("{feed_id}/{}", post_inner.f_img.clone())
                }
            },
        });
    }

    let a2 = millis();
    println!("Posts (2): {} ms", (a2 - a1));

    output.posts.sort_by_key(|e| e.date);

    Ok(serde_json::to_value(&output)?)
}

const MD_IMG_REGEX: Lazy<Regex> = Lazy::new(|| Regex::new(r"(?m)^\s*!\[.*\](.*)\s*$").unwrap());

fn split_paras(feed_id: String, text: String, tags: &Vec<TagId>) -> Vec<Para> {
    let mut out: Vec<Para> = vec![];

    let mut search_index = 0;

    loop {
        let found = MD_IMG_REGEX.find_at(&text, search_index);

        if found.is_some() {
            let found = found.unwrap();

            let img_line_s = found.start();
            let img_line_e = found.end();

            let prev_text = text[search_index..img_line_s].trim();
            if !prev_text.is_empty() {
                let prev_lines = process_text(prev_text.into(), &feed_id, tags);

                out.push(Para::Text { txt: prev_lines });
            }

            // Looks like `![Dat awesome desciption](path/to/image.png)`
            let img_line = found.as_str().trim();

            // Find description block bounds
            let descr_start = img_line.find('[').unwrap();
            let descr_end = img_line.find(']').unwrap();
            let this_pic_descr = img_line[(descr_start + 1)..descr_end].trim().to_string();

            // Find img url, which is after description block
            let ref_start = img_line[descr_end..].find('(').unwrap() + descr_end;
            let ref_end = img_line[descr_end..].find(')').unwrap() + descr_end;
            let src = &img_line[(ref_start + 1)..ref_end];

            let this_pic_src = format!("{feed_id}/{src}");

            if out.last().is_none() {
                out.push(Para::Image {
                    src: this_pic_src,
                    descr: this_pic_descr,
                });
            } else {
                let last = out.last().cloned().unwrap();
                match last {
                    Para::Text { txt: _ } => out.push(Para::Image {
                        src: this_pic_src,
                        descr: this_pic_descr,
                    }),
                    Para::Image { src, descr } => {
                        let _ = out.pop().unwrap();
                        out.push(Para::Gallery {
                            data: vec![
                                GalleryItem { src, descr },
                                GalleryItem {
                                    src: this_pic_src,
                                    descr: this_pic_descr,
                                },
                            ],
                        });
                    }
                    Para::Gallery { data: _ } => {
                        let l = match out.last_mut().unwrap() {
                            Para::Text { txt: _ } => panic!(),
                            Para::Image { src: _, descr: _ } => panic!(),
                            Para::Gallery { data } => data,
                        };

                        l.push(GalleryItem {
                            src: this_pic_src,
                            descr: this_pic_descr,
                        });
                    }
                }
            }

            search_index = img_line_e;
        } else {
            let remainder = text[search_index..].trim();
            if !remainder.is_empty() {
                let prev_lines = process_text(remainder.into(), &feed_id, tags);

                out.push(Para::Text { txt: prev_lines });
            }

            break;
        }
    }

    if out.is_empty() {
        out.push(Para::Text {
            txt: "*No post body*".into(),
        });
    }

    out
}

const LINK_STYLE: &str = "
color: #2AA198;
font-weight: bold;
text-decoration: none;
";

const TAG_STYLE: &str = "
color: #2AA198;
font-weight: normal;
text-decoration: none;
";

const EMOJI_STYLE: &str = "width=\"24\" height=\"24\" align=\"top\"";

const LINKS_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"\[([^\[]*?)\]\(.*?\)").unwrap());
const TAGS_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"\[([^(]*?)\]\[.*?\]").unwrap());
const EMOJI_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r":\w+:").unwrap());

fn process_text(src: String, feed_id: &String, tags: &[TagId]) -> String {
    let mut dst = src.clone();
    LINKS_RE.find_iter(&src).for_each(|f| {
        let group = f.as_str();

        let split = group.find("](").unwrap();

        let descr = &group[1..split];
        let url = &group[(split + 2)..(group.len() - 1)];

        let s = format!("<a style=\"{LINK_STYLE}\" href=\"{url}\">{descr}</a>");
        dst = dst.replace(group, &s);
    });

    let dst = dst;

    let mut dst_tags = dst.clone();
    TAGS_RE.find_iter(&dst).for_each(|f| {
        let group = f.as_str();

        let split = group.find("][").unwrap();

        let descr = &group[1..split];
        let raw_index = &group[(split + 2)..(group.len() - 1)];

        let index = raw_index.parse::<usize>();
        if index.is_err() {
            return;
        }

        let index = index.unwrap();
        if index == 0 {
            return;
        }

        let index = index - 1;
        if index >= tags.len() {
            return;
        }

        let url = format!("fourteen://{}/tag/{}", feed_id, &tags[index]);

        let s = format!("<a style=\"{TAG_STYLE}\" href=\"{url}\">{descr}</a>");
        dst_tags = dst_tags.replace(group, &s);
    });

    let mut dst_emoji = dst_tags.clone();
    EMOJI_RE.find_iter(&dst_tags).for_each(|f| {
        let group = f.as_str();
        let name = &group[1..group.len() - 1];

        let s = format!("<img src=\"image://fourteen/{feed_id}/emoji/{name}.png\" {EMOJI_STYLE}/>");

        dst_emoji = dst_emoji.replace(group, &s);
    });

    dst_emoji
}
