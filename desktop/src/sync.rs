use fourteen_lib::{
    error,
    raw::{self, SyncFeed, SyncServer},
};
use serde::Serialize;
use uuid::Uuid;

use crate::statics::{self, back};

#[derive(Debug, Default, Serialize)]
pub struct SyncData {
    pub servers: Vec<raw::SyncServer>,
    pub sync_feeds: Vec<raw::SyncFeed>,
    pub raw_feeds: Vec<raw::Feed>,
}

pub fn get() -> error::Result<serde_json::Value> {
    let servers = back().list_sync_servers().unwrap();
    let sync_feeds = back()
        .list_sync_feeds()
        .unwrap()
        .iter()
        .map(|e| {
            let mut e = e.clone();

            // Remove nonexistent servers from filter lists
            e.pull_servers
                .retain(|s| servers.iter().find(|v| v.uuid == *s).is_some());
            e.push_servers
                .retain(|s| servers.iter().find(|v| v.uuid == *s).is_some());
            
            e
        })
        .collect();

    let rz = SyncData {
        servers,
        sync_feeds,
        raw_feeds: back()
            .list_feeds()?
            .into_iter()
            .map(|f| {
                let mut f = f;
                if statics::has_priv_key(&f.uuid) {
                    f.inner = f
                        .inner
                        .decrypt(statics::get_priv_key(&f.uuid).unwrap())
                        .unwrap();
                }
                return f;
            })
            .collect(),
    };

    Ok(serde_json::to_value(&rz)?)
}

pub fn update_server(data: String) {
    let mut server: SyncServer = serde_json::from_str(&data).unwrap();
    server.uuid = server.uuid.trim().to_string();

    if server.uuid == "" {
        server.uuid = Uuid::new_v4().to_string();
    }

    back().insert_sync_server(&server).unwrap();
}

pub fn update_feeds(data: String) {
    let feeds: Vec<SyncFeed> = serde_json::from_str(&data).unwrap();
    for feed in feeds {
        back().insert_sync_feed(&feed).unwrap();
    }
}

pub fn delete_server(id: String) {
    back().delete_sync_server(&id).unwrap()
}
