use std::{
    collections::HashMap,
    fs,
    path::{Path, PathBuf},
    str::FromStr,
};

use serde::Serialize;

use fourteen_lib::{crypto, error, util::millis};

use crate::files;

use super::statics;

#[derive(Debug, Default, Serialize)]
pub struct MediaLibData {
    pub feed: String,
    pub folders: HashMap<String, Vec<String>>,
}

pub fn get(feed_id: String) -> error::Result<serde_json::Value> {
    let mut data = MediaLibData {
        feed: feed_id.clone(),
        ..Default::default()
    };

    let feed_datadir = files::get_userdata_folder() + &feed_id;

    // Ensure path exists
    fs::create_dir_all(&feed_datadir)?;

    let feed_datadir = PathBuf::from_str(&feed_datadir)?
        .canonicalize()?
        .to_str()
        .unwrap()
        .to_string();

    let base_ofs = feed_datadir.len() + 1;

    // List files in root dir
    let mut files: Vec<String> = files::list_files(&feed_datadir, false)
        .iter()
        .map(|e| e[base_ofs..].into())
        .collect();
    files.sort();

    if !files.is_empty() {
        data.folders.insert("...".to_string(), files);
    }

    // List files in subfolders
    for dir in files::list_dirs(&feed_datadir) {
        let dirname = dir[base_ofs..].to_string();

        if dirname.starts_with("emoji") {
            continue;
        }

        let mut files: Vec<String> = files::list_files(&dir, true)
            .iter()
            .map(|e| e[base_ofs..].into())
            .collect();
        files.sort();

        data.folders.insert(dirname, files);
    }

    let value = serde_json::to_value(data)?;
    Ok(value)
}

pub fn copy_to_library(feed_id: String, uris: Vec<String>) -> Vec<String> {
    let basedir = files::get_userdata_folder() + &feed_id + "/";

    let date = gregorian::Date::today();
    let subdir = date.year().to_string() + "/" + &format!("{:02}", date.month().to_number()) + "/";

    let dir = basedir + &subdir + "/";
    std::fs::create_dir_all(dir.clone()).unwrap();

    let key = statics::get_priv_key(&feed_id);

    let mut rz: Vec<String> = Vec::new();

    for uri in uris {
        let uri = uri.replace("file://", "");
        let original_path = PathBuf::from(uri.clone());
        let original_path = original_path.canonicalize().unwrap();
        println!("Original path is {:?}", original_path);

        let ext = &uri[uri.rfind('.').unwrap()..];

        let date_millis = millis() - ((date.to_unix_timestamp() * 1000) as u64);
        let (h, m, mut s) = get_hms(date_millis);

        let mut name: String;
        let mut full_path: String;

        loop {
            name = date.year().to_string()
                + &format!("-{:02}-{:02}", date.month().to_number(), date.day())
                + &format!("T{:02}:{:02}:{:02}", h, m, s)
                + ext;

            full_path = dir.clone() + &name;

            if !Path::new(&full_path).exists() {
                break;
            } else {
                s += 1;

                if s >= 60 {
                    // I don't care
                }
            }
        }

        println!("Filename is {}", name);
        std::fs::copy(original_path, full_path.clone()).unwrap();

        let idx_name = subdir.to_string() + &name;

        if key.is_some() {
            let key = key.unwrap();

            let data = std::fs::read(full_path.clone()).unwrap();
            let data = crypto::encrypt(key, data).unwrap();
            std::fs::write(full_path.clone(), data).unwrap();
        }

        rz.push(idx_name);
    }

    rz
}

fn get_hms(mut millis: u64) -> (u64, u64, u64) {
    let h = millis / (60 * 60 * 1000);
    millis %= 60 * 60 * 1000;

    let m = millis / (60 * 1000);
    millis %= 60 * 1000;

    let s = millis / 1000;

    (h, m, s)
}
