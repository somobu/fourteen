use std::{
    fs::File,
    io::Write,
    sync::mpsc::{Receiver, Sender},
    thread,
};

use fourteen_lib::{
    error::{Error, ErrorKind, OptionToError},
    raw::{SyncMode, SyncServersFilter},
    util::millis,
};
use qmetaobject::{QString, QVariant};
use serde::{Deserialize, Serialize};

use crate::{
    files,
    statics::{self, back},
    sync_tool,
};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct SyncMessage {
    pub text: MessageText,
    pub action: MessageAction,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub enum MessageText {
    #[default]
    None,
    Working,
    Success,
    Failed,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub enum MessageAction {
    #[default]
    None,
    Dismiss,
    OpenLog,
}

static mut SYNC_THREAD_TX: Option<Sender<String>> = None;

pub fn start_sync_thread() {
    let (tx, rx) = std::sync::mpsc::channel::<String>();
    unsafe { SYNC_THREAD_TX = Some(tx) };
    thread::spawn(|| sync_thread(rx));
}

pub fn request_sync(server_id: String) {
    unsafe {
        SYNC_THREAD_TX
            .as_ref()
            .inspect(|e| e.send(server_id).unwrap());
    }
}

pub fn notify_qt(sync_status: SyncMessage) {
    let sync_status = serde_json::to_string(&sync_status).unwrap();
    let args: Vec<QVariant> = vec![QVariant::from(QString::from(sync_status))];

    unsafe {
        statics::ENGINE.invoke_method_noreturn("sync_status".into(), &args);
    }
}

fn sync_thread(rx: Receiver<String>) {
    for server_id in rx.iter() {
        let rz = handle_server(server_id);
        if rz.is_ok() {
            notify_qt(SyncMessage {
                text: MessageText::Success,
                action: MessageAction::Dismiss,
            })
        } else {
            propagate_error(rz.unwrap_err().to_string());
        }
    }
}

fn handle_server(server_id: String) -> Result<(), Error> {
    let mut server = back()
        .list_sync_servers()?
        .iter()
        .find(|e| e.uuid == server_id)
        .to_result()?
        .clone();

    notify_qt(SyncMessage {
        text: MessageText::Working,
        action: MessageAction::Dismiss,
    });

    std::fs::create_dir_all(files::get_logs_dir())?;

    let remote = sync_tool::SyncTarget::Ssh {
        user_server: server.server_data.clone(),
    };

    let feeds = back().list_sync_feeds()?;
    let pull = server.mode == SyncMode::Both || server.mode == SyncMode::Pull;
    let push = server.mode == SyncMode::Both || server.mode == SyncMode::Push;

    for feed in feeds {

        let should_be_synced = back().get_feed(&feed.feed_id).map_or(false, |e| match e.status {
            fourteen_lib::raw::FeedStatus::Visible => true,
            fourteen_lib::raw::FeedStatus::Deleted => false,
        });

        if !should_be_synced {
            eprintln!("Feed {} marked as deleted, skipping", feed.feed_id);
            continue;
        }

        let to_pull = pull
            && (feed.pull_mode == SyncServersFilter::All
                || (feed.pull_mode == SyncServersFilter::WhiteList
                    && feed.pull_servers.contains(&server_id)));

        let to_push = push
            && (feed.push_mode == SyncServersFilter::All
                || (feed.push_mode == SyncServersFilter::WhiteList
                    && feed.push_servers.contains(&server_id)));

        let feed_never_was_synced = back().get_feed(&feed.feed_id)?.edited_at == 0;
        let since = if feed_never_was_synced {
            0
        } else {
            server.last_sync
        };

        if to_pull || to_push {
            let sync_rz =
                sync_tool::perform_sync(&mut back(), &feed.feed_id, since, &remote, pull, push)
                    .map_err(|e| Error::new(ErrorKind::ConversionError(format!("{e:#?}"))))?;

            let summary_name = format!("{}-summary.json", millis());
            let mut f = File::create_new(files::get_logs_dir() + &summary_name)?;
            f.write(serde_json::to_string_pretty(&sync_rz)?.as_bytes())?;
        }
    }

    // Update last sync time
    server.last_sync = millis();
    back().insert_sync_server(&server)?;

    Ok(())
}

fn propagate_error(error: String) {
    let summary_name = format!("{}-errorlog.txt", millis());
    let mut f = File::create_new(files::get_logs_dir() + &summary_name).unwrap();
    f.write(error.as_bytes()).unwrap();

    notify_qt(SyncMessage {
        text: MessageText::Failed,
        action: MessageAction::OpenLog,
    })
}
