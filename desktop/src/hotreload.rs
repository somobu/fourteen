#[cfg(debug_assertions)]
pub fn watch(path: String) {
    use notify::{RecommendedWatcher, RecursiveMode, Watcher};
    use qmetaobject::QVariant;
    use std::{
        thread,
        time::{Duration, Instant},
    };

    use super::statics::ENGINE;

    thread::spawn(move || {
        let (tx, rx) = std::sync::mpsc::channel();

        let mut watcher = RecommendedWatcher::new(tx, notify::Config::default()).unwrap();
        watcher
            .watch(path.as_ref(), RecursiveMode::Recursive)
            .unwrap();

        println!("Hot reload is up and running!");

        let mut last_reload = Instant::now();

        loop {
            let rz = rx.recv().unwrap();

            if rz.is_ok() {
                let _ = rz.unwrap().paths;

                if last_reload.elapsed().as_millis() < 500 {
                    continue;
                }

                unsafe {
                    let args: Vec<QVariant> = vec![];
                    ENGINE.invoke_method_noreturn("hot_reload_pre".into(), &args);
                    thread::sleep(Duration::from_millis(1));
                    ENGINE.trim_component_cache();
                    ENGINE.clear_component_cache();
                    thread::sleep(Duration::from_millis(1));
                    ENGINE.invoke_method_noreturn("hot_reload_post".into(), &args);
                }

                last_reload = Instant::now();
            }
        }
    });
}

#[cfg(not(debug_assertions))]
pub fn watch(_: String) {
    // No-op
}
