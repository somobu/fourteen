use std::process::Command;

use gregorian::Date;
use qmetaobject::{qt_base_class, qt_method, qt_signal, QObject, QString, QStringList};
use regex::Regex;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use fourteen_lib::{
    error::{self, ErrorKind, OptionToError},
    raw::{self},
    util::millis,
};

use crate::{editor, feed, files, medialib, sync, sync_thread};

use super::statics::back;
use super::{greeter, statics};

#[allow(non_snake_case)]
#[derive(QObject, Default)]
pub struct Fourteen {
    base: qt_base_class!(trait QObject),

    proceed_to: qt_method!(fn(&mut self, data: QString)),
    pageSwitch: qt_signal!(data: QString),

    add_key: qt_method!(fn(&mut self, feed_id: QString, key: QString) -> bool),
    add_feed: qt_method!(fn(&mut self, feed_name: QString) -> QString),

    update_feed: qt_method!(fn(&mut self, feed_id: QString, title: QString, descr: QString, status: QString)),

    update_post: qt_method!(fn(&mut self, feed_id: QString, post: QString)),
    delete_post: qt_method!(fn(&mut self, id: QString)),

    update_tag: qt_method!(fn(&mut self, tag_json: QString) -> QString),

    get_media_entries: qt_method!(fn(&mut self, feed_id: QString) -> QString),
    add_media_entries: qt_method!(fn(&mut self, feed_id: QString, entries: QString) -> QStringList),

    gettext: qt_method!(fn(&mut self, src: QString) -> QString),

    is_csd: qt_method!(fn(&mut self) -> bool),

    trigger_sync: qt_method!(fn(&mut self, server: QString)),

    update_sync_server: qt_method!(
        fn update_sync_server(&mut self, server: QString) {
            sync::update_server(server.into());
            self.proceed_to("{\"page\":\"sync\"}".into());
        }
    ),

    update_sync_feeds: qt_method!(
        fn update_sync_feeds(&mut self, server: QString) {
            sync::update_feeds(server.into());
            self.proceed_to("{\"page\":\"sync\"}".into());
        }
    ),

    delete_sync_server: qt_method!(
        fn delete_sync_server(&mut self, server_id: QString) {
            sync::delete_server(server_id.into());
            self.proceed_to("{\"page\":\"sync\"}".into());
        }
    ),

    open_logs_folder: qt_method!(fn(&mut self)),
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
struct ProceedArgs {
    pub page: String,
    pub feed: Option<String>,
    pub post: Option<String>,
    pub tag: Option<String>,
    pub year: Option<i32>,
    pub month: Option<i32>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct PageData {
    pub page: String,
    pub head: Vec<HeadEntry>,
    pub data: serde_json::Value,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct HeadEntry {
    pub name: String,
    pub data: ProceedArgs,
}

impl Fourteen {
    fn switch_rq(&mut self, r: ProceedArgs) {
        let d = resp(r).unwrap_or_else(|e| get_error("Error ocurred".to_string(), e));
        self.switch_data(d);
    }

    fn switch_data(&mut self, r: PageData) {
        self.pageSwitch(serde_json::to_string(&r).unwrap().into());
    }

    fn proceed_to(&mut self, data: QString) {
        unsafe {
            if statics::INIT_PRIV_KEYS.is_some() {
                let keys = statics::INIT_PRIV_KEYS.take().unwrap();
                println!("Got {} initial keys", keys.len());

                for (k, v) in keys {
                    statics::PRIVKEYS.insert(k, v);
                }
            }
        }

        let data: String = data.into();
        let data: ProceedArgs = serde_json::from_str(&data).unwrap();

        let r = resp(data).unwrap_or_else(|e| get_error("Error ocurred".to_string(), e));
        self.switch_data(r);
    }

    fn add_key(&mut self, feed_id: QString, key: QString) -> bool {
        let regex = Regex::new(r"[0-9a-fA-F]{32}").unwrap();

        let feed_id: String = feed_id.into();
        let key: String = key.into();
        if !regex.is_match(&key) {
            return false;
        }

        let feed = back().get_feed(&feed_id).unwrap();
        let l = feed.inner.decrypt(&key);
        if l.is_err() {
            return false;
        }

        let _ = l.unwrap().de().unwrap();

        unsafe {
            statics::PRIVKEYS.insert(feed_id, key);
        }

        true
    }

    fn add_feed(&mut self, feed_name: QString) -> QString {
        let feed_id = Uuid::new_v4().to_string();
        let user_id = Uuid::new_v4().to_string();

        let feed = raw::Feed {
            uuid: feed_id.clone(),
            edited_at: millis(),
            private: false,
            status: raw::FeedStatus::Visible,
            inner: raw::Inner::Decrypted(raw::FeedData {
                owned_by: user_id.clone(),
                edited_by: user_id.clone(),
                title: feed_name.into(),
                descr: "".to_string(),
                header_imgs: vec![],
            }),
        };
        back().insert_feed(&feed).unwrap();

        let user = raw::User {
            uuid: user_id.clone(),
            feed_id: feed_id.clone(),
            edited_at: millis() + 1,
            inner: raw::Inner::Decrypted(raw::UserData {
                created_by: user_id.clone(),
                created_at: millis(),
                edited_by: user_id.clone(),
                name: "Author".to_string(),
                icon: "".to_string(),
            }),
        };
        back().insert_user(&user).unwrap();

        back().set_current_user(&feed_id, &user_id).unwrap();

        feed_id.into()
    }

    fn update_feed(&mut self, feed_id: QString, title: QString, descr: QString, status: QString) {
        let feed_id = feed_id.to_string();

        let mut feed = back().get_feed(&feed_id).unwrap();

        let key = statics::get_priv_key(&feed_id);
        if key.is_some() {
            feed.inner = feed.inner.decrypt(key.as_ref().unwrap()).unwrap();
        }

        let status = format!("\"{}\"", status.to_string());

        feed.edited_at = millis();
        feed.status = serde_json::from_str(&status).unwrap();
        feed.inner.de_mut().unwrap().title = title.to_string();
        feed.inner.de_mut().unwrap().descr = descr.to_string();
        // TODO: set 'edited-by'

        if key.is_some() {
            feed.inner = feed.inner.encrypt(key.unwrap()).unwrap();
        }

        back().insert_feed(&feed).unwrap();
    }

    fn update_post(&mut self, feed_id: QString, post: QString) {
        let feed_id: String = feed_id.into();
        let post_str: String = post.into();

        let mut post: raw::Post = serde_json::from_str(&post_str).unwrap();
        let current_user = back().get_current_user(&feed_id).unwrap();
        let key = statics::get_priv_key(&feed_id);

        if post.uuid.is_empty() {
            post.uuid = uuid::Uuid::new_v4().to_string();
            post.inner.de_mut().unwrap().created_by = current_user.clone();
        }

        if post.created_at == 0 {
            post.created_at = millis();
        }

        post.feed_id = feed_id.clone();
        post.edited_at = millis();
        post.inner.de_mut().unwrap().edited_by = current_user;

        let tags = post.inner.de().unwrap().tags.clone();

        if key.is_some() {
            post = post.encrypted(key).unwrap();
        }

        back().insert_post(&post).unwrap();
        back().insert_post_tags(&post.uuid, &tags).unwrap();

        let date = Date::from_unix_timestamp((post.created_at / 1000) as i64);
        let year = date.year().to_number() as i32;
        let month = date.month().to_number() as i32;

        let rz = resp(ProceedArgs {
            page: "feed".to_string(),
            feed: Some(feed_id),
            year: Some(year),
            month: Some(month),
            ..Default::default()
        })
        .unwrap();

        self.switch_data(rz);
    }

    fn delete_post(&mut self, id: QString) {
        let original_post = back().get_post(&id.clone().into());

        back().delete_post(&id.clone().into()).unwrap();
        back()
            .insert_post_tags(&id.clone().into(), &vec![])
            .unwrap();

        if original_post.is_ok() {
            self.switch_rq(ProceedArgs {
                page: "feed".to_string(),
                feed: Some(original_post.unwrap().feed_id),
                ..Default::default()
            });
        } else {
            self.switch_rq(ProceedArgs {
                page: "greeter".to_string(),
                ..Default::default()
            });
        }
    }

    fn update_tag(&mut self, tag_json: QString) -> QString {
        let mut tag: raw::Tag = serde_json::from_str(&tag_json.to_string()).unwrap();

        let current_user = back().get_current_user(&tag.feed_id).unwrap();
        tag.inner.de_mut().unwrap().edited_by = current_user;

        tag.edited_at = millis();

        if tag.uuid.starts_with("new-tag") {
            tag.uuid = uuid::Uuid::new_v4().to_string();
        }

        let key = statics::get_priv_key(&tag.feed_id);
        let mut crypt_tag = tag.clone();
        if key.is_some() {
            crypt_tag.inner = tag.inner.clone().encrypt(key.unwrap()).unwrap();
        }

        back().insert_tag(&crypt_tag).unwrap();

        serde_json::to_string(&tag).unwrap().into()
    }

    fn get_media_entries(&mut self, feed_id: QString) -> QString {
        let v = medialib::get(feed_id.to_string()).unwrap();
        serde_json::to_string(&v).unwrap().into()
    }

    fn add_media_entries(&mut self, feed_id: QString, entries: QString) -> QStringList {
        let entries: Vec<String> = serde_json::from_str(&entries.to_string()).unwrap();
        medialib::copy_to_library(feed_id.to_string(), entries).into()
    }

    #[cfg(feature = "gettext")]
    fn gettext(&mut self, src: QString) -> QString {
        gettextrs::gettext(src.to_string()).into()
    }

    #[cfg(not(feature = "gettext"))]
    fn gettext(&mut self, src: QString) -> QString {
        src
    }

    fn is_csd(&mut self) -> bool {
        unsafe { statics::CSD }
    }

    fn trigger_sync(&mut self, server: QString) {
        sync_thread::request_sync(server.into())
    }

    fn open_logs_folder(&mut self) {
        let _ = Command::new("bash")
            .arg("-c")
            .arg(format!("gio open {}", files::get_logs_dir()))
            .spawn()
            .unwrap();
    }
}

fn get404(url: String) -> PageData {
    #[derive(Debug, Serialize)]
    struct Inplace404Json {
        url: String,
    }

    let i4j = Inplace404Json { url };

    PageData {
        page: "404".to_string(),
        head: vec![HeadEntry {
            name: "Fourteen".to_string(),
            data: ProceedArgs {
                page: "greeter".to_string(),
                ..Default::default()
            },
        }],
        data: serde_json::to_value(i4j).unwrap(),
    }
}

fn get_error(title: String, error: error::Error) -> PageData {
    #[derive(Debug, Serialize)]
    struct InplaceErrorJson {
        url: String,
        error: String,
        backtrace: String,
    }

    let message = format!("{:?}", error.kind).replace("\\n", "\n");

    let iej = InplaceErrorJson {
        url: title,
        error: message,
        backtrace: error.bt.to_string().replace("}, {", "\n"),
    };

    PageData {
        page: "error".to_string(),
        head: vec![HeadEntry {
            name: "Fourteen".to_string(),
            data: ProceedArgs {
                page: "greeter".to_string(),
                ..Default::default()
            },
        }],
        data: serde_json::to_value(iej).unwrap(),
    }
}

fn resp(data: ProceedArgs) -> error::Result<PageData> {
    let mut head: Vec<HeadEntry> = vec![HeadEntry {
        name: "Fourteen".to_string(),
        data: ProceedArgs {
            page: "greeter".to_string(),
            ..Default::default()
        },
    }];

    let feed: Option<raw::Feed>;
    if data.feed.is_some() {
        let feed_id: String = data.feed.clone().unwrap();
        let mut raw_feed = back().get_feed(&feed_id)?;

        let l = statics::get_priv_key(&feed_id);
        if l.is_some() {
            raw_feed.inner = raw_feed.inner.decrypt(l.unwrap())?;
        }

        feed = Some(raw_feed);
    } else {
        feed = None;
    }

    let get_feed_entry = || {
        Ok::<HeadEntry, error::Error>(HeadEntry {
            name: feed.to_result()?.inner.de()?.title.clone(),
            data: ProceedArgs {
                page: "feed".to_string(),
                feed: data.feed.clone(),
                ..Default::default()
            },
        })
    };

    match data.page.as_str() {
        "about" => {
            head.push(HeadEntry {
                name: "About".to_string(),
                data: ProceedArgs {
                    page: "about".to_string(),
                    ..Default::default()
                },
            });

            Ok(PageData {
                page: "about".to_string(),
                head,
                data: serde_json::Value::Object(serde_json::Map::new()),
            })
        }
        "editor" => {
            let post_id = data
                .post
                .clone()
                .unwrap_or_else(|| Uuid::new_v4().to_string());

            head.push(get_feed_entry()?);
            head.push(HeadEntry {
                name: "Editor".to_string(),
                data: ProceedArgs {
                    page: "editor".to_string(),
                    feed: data.feed.clone(),
                    post: data.post.clone(),
                    ..Default::default()
                },
            });

            let feed_id: String = data.feed.clone().unwrap();

            Ok(PageData {
                page: "editor".to_string(),
                head,
                data: editor::get(&feed_id, post_id)?,
            })
        }
        "feed" => {
            let feed_id: String = data.feed.clone().unwrap();

            let key: Option<String>;
            if statics::has_priv_key(&feed_id) {
                let l = statics::get_priv_key(&feed_id);
                if l.is_none() {
                    return Err(error::new(ErrorKind::NotFound(format!(
                        "Key not found for feed {}",
                        feed_id
                    ))));
                } else {
                    key = Some(l.unwrap().to_owned());
                }
            } else {
                let feed = back().get_feed(&feed_id)?;
                if feed.private {
                    #[derive(Debug, Serialize)]
                    struct InplaceKeyRequest {
                        proceed_to: ProceedArgs,
                    }

                    let ikr = InplaceKeyRequest { proceed_to: data };

                    return Ok(PageData {
                        page: "key_rq".to_string(),
                        head,
                        data: serde_json::to_value(ikr).unwrap(),
                    });
                } else {
                    key = None;
                }
            }

            let last_post_time = back().get_last_post_time(&feed_id);
            if last_post_time.is_err() {
                Err(error::new(ErrorKind::BadString(format!(
                    "Unknown last post time {}",
                    last_post_time.unwrap_err()
                ))))
            } else {
                let last_post_date =
                    Date::from_unix_timestamp((last_post_time.unwrap() / 1000) as i64);

                let year = data
                    .year
                    .unwrap_or_else(|| last_post_date.year().to_number() as i32);

                let month = data
                    .month
                    .unwrap_or_else(|| last_post_date.month().to_number() as i32);

                head.push(get_feed_entry()?);

                Ok(PageData {
                    page: "feed".to_string(),
                    head,
                    data: feed::get(feed_id, year as i16, month as u8, key)?,
                })
            }
        }
        "greeter" => Ok(PageData {
            page: "greeter".to_string(),
            head,
            data: greeter::get()?,
        }),
        "map" => {
            head.push(HeadEntry {
                name: "Places & events".to_string(),
                data: ProceedArgs {
                    page: "map".to_string(),
                    ..Default::default()
                },
            });

            Ok(PageData {
                page: "map".to_string(),
                head,
                data: serde_json::Value::Object(serde_json::Map::new()),
            })
        }
        "search" => {
            head.push(HeadEntry {
                name: "Search".to_string(),
                data: ProceedArgs {
                    page: "search".to_string(),
                    ..Default::default()
                },
            });

            Ok(PageData {
                page: "search".to_string(),
                head,
                data: serde_json::Value::Object(serde_json::Map::new()),
            })
        }
        "sync" => {
            head.push(HeadEntry {
                name: "Sync".to_string(),
                data: ProceedArgs {
                    page: "sync".to_string(),
                    ..Default::default()
                },
            });

            Ok(PageData {
                page: "sync".to_string(),
                head,
                data: sync::get()?,
            })
        }
        "tag_query" => {
            head.push(get_feed_entry()?);
            head.push(HeadEntry {
                name: "Tag Query".to_string(),
                data: ProceedArgs {
                    page: "tag_query".to_string(),
                    feed: data.feed.clone(),
                    tag: data.tag.clone(),
                    ..Default::default()
                },
            });

            let feed_id: String = data.feed.clone().unwrap();
            let key: Option<String>;
            if statics::has_priv_key(&feed_id) {
                let l = statics::get_priv_key(&feed_id);
                if l.is_none() {
                    return Err(error::new(ErrorKind::NotFound(format!(
                        "Key not found for feed {}",
                        feed_id
                    ))));
                } else {
                    key = Some(l.unwrap().to_owned());
                }
            } else {
                let feed = back().get_feed(&feed_id)?;
                if feed.private {
                    #[derive(Debug, Serialize)]
                    struct InplaceKeyRequest {
                        proceed_to: ProceedArgs,
                    }

                    let ikr = InplaceKeyRequest { proceed_to: data };

                    return Ok(PageData {
                        page: "key_rq".to_string(),
                        head,
                        data: serde_json::to_value(ikr).unwrap(),
                    });
                } else {
                    key = None;
                }
            }

            Ok(PageData {
                page: "tag_query".to_string(),
                head,
                data: feed::query_tag(&feed_id, data.tag.clone().unwrap(), key)?,
            })
        }
        _ => Ok(get404(format!("{:?}", data))),
    }
}
