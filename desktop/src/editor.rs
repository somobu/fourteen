use fourteen_lib::{
    self,
    error::{self, OptionToError},
    raw::{self, FeedId},
};

use crate::files;

use super::statics::{self, back};
use serde::Serialize;

#[derive(Debug, Default, Serialize)]
pub struct EditorModel {
    pub id: String,
    pub feed_id: String,
    pub is_new: bool,

    pub post: raw::Post,
    pub tags: Vec<raw::Tag>,

    pub emoji_root: Option<String>,
    pub emoji_names: Vec<String>,
}

#[derive(Debug, Default, Serialize)]
pub struct Feed {
    pub id: String,
    pub name: String,
    pub is_priv: bool,
}

pub fn get(feed_id: &FeedId, post_id: String) -> error::Result<serde_json::Value> {
    let mut output = EditorModel {
        id: post_id.clone(),
        feed_id: feed_id.clone(),
        ..Default::default()
    };

    match back().get_post(&post_id) {
        Ok(mut post) => {
            output.is_new = false;

            if post.inner.is_encrypted() {
                post.inner = post
                    .inner
                    .decrypt(statics::get_priv_key(&post.feed_id).to_result()?)?;
            }

            output.post = post;
        }
        Err(_) => {
            output.is_new = true;
        }
    }

    output.tags = back()
        .get_active_tags(feed_id)?
        .iter()
        .map(|t| {
            let mut t = t.clone();
            if t.inner.is_encrypted() {
                t.inner = t
                    .inner
                    .decrypt(statics::get_priv_key(&t.feed_id).to_result().unwrap())
                    .unwrap();
            }

            t
        })
        .collect();

    let emoji_dir = files::get_userdata_folder() + "/" + feed_id + "/emoji/";

    let emoji_entries = std::fs::read_dir(&emoji_dir);
    if emoji_entries.is_ok() {
        output.emoji_root = Some(emoji_dir);

        for entry in emoji_entries.unwrap() {
            if entry.is_err() {
                continue;
            }

            let entry = entry.unwrap();
            let m = entry.metadata().unwrap();
            if m.is_file() {
                let fname = entry.file_name();
                let fname = fname.to_str().unwrap();
                if fname.ends_with(".png") {
                    output
                        .emoji_names
                        .push(fname[..fname.len() - 4].to_string());
                }
            }
        }
    }

    Ok(serde_json::to_value(&output)?)
}
