use std::{collections::HashMap, ffi::CStr, path::Path};

use clap::Parser;
use files::{get_database_subpath, get_default_database_path, get_userdata_folder};
use qmetaobject::*;

use fourteen_lib::{database::Database, delta::Delta, error, raw, util::millis};

pub mod editor;
pub mod feed;
pub mod files;
pub mod fourteen;
pub mod greeter;
pub mod hotreload;
pub mod medialib;
pub mod proxy_image_provider;
pub mod statics;
pub mod sync_thread;
pub mod sync_tool;
pub mod sync;


#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct CliCmds {
    /// Path to 'userdata' dir, defaults to '$XDG_DATA_HOME/fourteen' in release
    #[arg(short, long)]
    dir: Option<String>,

    /// UI translation language (locale), in format like "en_EN.UTF-8"
    #[arg(long)]
    ui_lang: Option<String>,

    /// UI scale factor, (currently) alias of QT_SCALE_FACTOR, defaults to 1.0
    #[arg(long)]
    ui_scale: Option<f64>,

    /// Enable client-side decorations (CSD)
    #[arg(long)]
    csd: bool,

    /// Path to json dict of "feed uuid to key"
    #[arg(long)]
    priv_keys: Option<String>,
}

fn reg<T>(bytes: &[u8])
where
    T: QObject + Default + Sized,
{
    let name = CStr::from_bytes_with_nul(bytes).unwrap();
    qml_register_type::<T>(name, 1, 0, name);
}

fn main() {
    let cli = CliCmds::parse();

    error::set_colored_backtrace(true);

    unsafe { statics::CSD = cli.csd };

    cli.ui_lang.map(|v| {
        set_lang(&v);
        v
    });

    std::env::set_var("QT_SCALE_FACTOR", cli.ui_scale.unwrap_or(1.0).to_string());

    setup_gettext();

    // Setup database
    unsafe {
        let workdir = cli.dir.clone();

        let db = if workdir.is_some() {
            let file = workdir.unwrap() + &get_database_subpath();
            Database::open_file(&file).unwrap()
        } else if cfg!(debug_assertions) {
            Database::open_in_memory().unwrap()
        } else {
            let file = get_default_database_path();
            let p = Path::new(&file).parent().unwrap();
            std::fs::create_dir_all(p).unwrap();

            Database::open_file(&file).unwrap()
        };

        statics::BACKEND = Some(db);
    }

    // Load priv keys from json (if specified)
    if cli.priv_keys.is_some() {
        let data = std::fs::read_to_string(cli.priv_keys.unwrap()).unwrap();
        let keys: HashMap<String, String> = serde_json::from_str(&data).unwrap();

        unsafe {
            statics::INIT_PRIV_KEYS = Some(keys);
        }
    }

    // Initialize db w/ debug data (in debug mode only)
    if cfg!(debug_assertions) && cli.dir.is_none() {
        let delta = get_userdata_folder() + "/delta.json";
        let delta = std::fs::read_to_string(delta);

        if delta.is_err() {
            println!("Unable to read debug data, skipping");
        } else {
            let delta = delta.unwrap();
            let delta: Delta = serde_json::from_str(&delta).unwrap();

            let b = statics::back();
            delta.feeds.iter().for_each(|f| {
                b.insert_feed(f).unwrap();

                if f.inner.is_encrypted() {
                    b.set_current_user(&f.uuid, &"enc_user".to_string())
                        .unwrap();
                } else {
                    b.set_current_user(&f.uuid, &f.inner.de().unwrap().owned_by)
                        .unwrap();
                }
            });
            delta.users.iter().for_each(|f| b.insert_user(f).unwrap());
            delta.tags.iter().for_each(|f| b.insert_tag(f).unwrap());
            delta.posts.iter().for_each(|f| {
                b.insert_post(f).unwrap();
                if !f.inner.is_encrypted() {
                    let tags = f.inner.de().unwrap().tags.clone();
                    b.insert_post_tags(&f.uuid, &tags).unwrap();
                }
            });

            b.insert_sync_server(&raw::SyncServer {
                uuid: "sync-server-1".to_string(),
                server_name: "Kitsuki".to_string(),
                server_type: raw::SyncServerType::Ssh,
                server_data: "kitsuki".to_string(),
                mode: raw::SyncMode::Both,
                last_sync: millis() - 60 * 60 * 1000,
            })
            .unwrap();
        }

        unsafe {
            statics::PRIVKEYS.insert(
                "encrypted".to_string(),
                "00000000000000000000000000000000".to_string(),
            )
        };
    }

    // Spawn aux sync thread
    sync_thread::start_sync_thread();

    // UI stuff
    hotreload::watch(files::get_appimage_prefix() + "/qml/");

    reg::<fourteen::Fourteen>(b"Fourteen\0");

    unsafe {
        proxy_image_provider::register();
        statics::ENGINE.load_file((files::get_appimage_prefix() + "/qml/Application.qml").into());
        statics::ENGINE.exec();
    }
}

fn set_lang(lang: &str) {
    println!("Set locale to {lang}");
    std::env::set_var("LC_ALL", lang);
    std::env::set_var("LANG", lang);
    std::env::set_var("LANGUAGE", lang);

    #[cfg(feature = "gettext")]
    {
        let _ = gettextrs::setlocale(gettextrs::LocaleCategory::LcMessages, "");
    }
}

fn setup_gettext() {
    #[cfg(feature = "gettext")]
    {
        gettextrs::bindtextdomain("fourteen", files::get_appimage_prefix() + "/lang/mo/").unwrap();
        gettextrs::textdomain("fourteen").unwrap();
    }
}
