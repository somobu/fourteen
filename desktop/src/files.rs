use std::{fs, path::PathBuf, str::FromStr, time::SystemTime};

pub fn get_appimage_prefix() -> String {
    std::env::var("APPDIR").unwrap_or(".".to_string())
}

pub fn get_xdg_data_prefix() -> String {
    std::env::var("XDG_DATA_HOME").unwrap_or_else(|_| get_home() + "/.local/share")
}

pub fn get_workdir() -> String {
    std::env::var("PWD").unwrap_or(".".to_string())
}

pub fn get_home() -> String {
    std::env::var("HOME").unwrap_or(".".to_string())
}

pub fn get_logs_dir() -> String {
    get_userdata_folder() + "/logs/"
}

pub fn get_userdata_folder() -> String {
    if cfg!(debug_assertions) {
        get_workdir() + "/../testdata/"
    } else {
        get_xdg_data_prefix() + "/fourteen/"
    }
}

pub fn get_default_database_path() -> String {
    get_userdata_folder() + &get_database_subpath()
}

pub fn get_database_subpath() -> String {
    "/database.sqlite".to_owned()
}

pub fn list_dirs(path: &String) -> Vec<String> {
    fs::read_dir(path)
        .unwrap()
        .filter_map(|e| {
            let e = e.unwrap().path();

            if e.is_dir() {
                Some(e.to_str().unwrap().to_string())
            } else {
                None
            }
        })
        .collect()
}

pub fn list_files(path: &String, include_subdirs: bool) -> Vec<String> {
    let dir = fs::read_dir(path).unwrap();
    let paths: Vec<PathBuf> = dir.map(|e| e.unwrap().path()).collect();

    let mut result = Vec::<String>::new();

    for path in paths {
        if path.is_file() {
            let l = path.canonicalize().unwrap().to_str().unwrap().to_string();
            result.push(l);
        }

        if include_subdirs && path.is_dir() {
            let m = path.to_str().unwrap().to_string();
            let mut l = list_files(&m, include_subdirs);
            result.append(&mut l);
        }
    }

    result
}

pub fn normalize_path(path: String) -> String {
    PathBuf::from_str(&path)
        .unwrap()
        .canonicalize()
        .unwrap()
        .to_str()
        .unwrap()
        .to_string()
}

pub fn get_file_mod_date(f: &str) -> u128 {
    fs::metadata(f)
        .unwrap()
        .modified()
        .unwrap()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_millis()
}
