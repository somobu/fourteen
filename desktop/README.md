# Fourteen Desktop

## Build on Linux

Overall procedure:
1. Get [rust](https://www.rust-lang.org/tools/install);
2. Get clang or gcc (install via your package manager);
3. Install dependencies;
4. Build&run via `cargo run`


### Debian and derivatives
Build dependencies:
```
qtbase5-dev qtdeclarative5-dev qtquickcontrols2-5-dev
```

Runtime dependencies:
```
qml-module-qtquick2 qml-module-qtquick-controls2 qml-module-qtquick-layouts qml-module-qtgraphicaleffects 
qml-module-qtlocation qml-module-qtpositioning
```

> **Note**: `qml-module-qtlocation` has to be `>=5.15.11`, otherwise "Places & locations" page hungs.
>
> As of 01.11.2024, stable Debian repos contains outdated (buggy) version of this package.


### Archlinux
Build dependencies:
```
qt5-base-dev qt5-declarative
```

Runtime dependencies:
```
qt5-quickcontrols2 qt5-graphicaleffects qt5-location
```


## Build on Windows

- Install [rustup](https://www.rust-lang.org/tools/install?platform_override=win);
- Install QT5 (mfs forces you to get an account -- but you can find offline installer somwhere in the internet);
- Install [gettext](https://mlocati.github.io/articles/gettext-iconv-windows.html) -- but it is missing .lib binaries;
- Set `GETTEXT_DIR` env var: `set GETTEXT_DIR=C:\Program Files\gettext_iconv\`;
- Somehow install [Visual C++ Build Tools](https://stackoverflow.com/a/63505627);
- Get `LNK1181: cannot open input file 'intl.lib` error ([32bit MSVC needed?](https://github.com/ZenGo-X/kms-secp256k1/issues/25#issuecomment-555946633));
- ~~There is [php's x64 build of libintl](https://windows.php.net/downloads/php-sdk/deps/) though~~ -- it is useless in our case;
- Fuck it, lets skip gettext on windows (rm `gettext-rs` dep from `desktop/Cargo.toml` and fix errors);


## Translations

You have to update `.po` and build `.mo` using smol script:
```bash
$ ./lang/update.sh
```


## Commandline arguments
```
Usage: fourteen-qt [OPTIONS]

Options:
  -d, --dir <DIR>              Path to 'userdata' dir, defaults to '$XDG_DATA_HOME/fourteen' in release
      --ui-lang <UI_LANG>      UI translation language (locale), in format like "en_EN.UTF-8"
      --ui-scale <UI_SCALE>    UI scale factor, (currently) alias of QT_SCALE_FACTOR, defaults to 1.0
      --csd                    Enable client-side decorations (CSD)
      --priv-keys <PRIV_KEYS>  Path to json dict of "feed uuid to key"
  -h, --help                   Print help
  -V, --version                Print version
```

## Packaging

It is possible to package program using [AppImage (guide)](pkg/README.md).

